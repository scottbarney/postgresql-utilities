﻿drop table if exists sniped_processes;

create table sniped_processes
( pid integer
, datname name
, usesysid oid
, usename name
, application_name text
, client_addr inet
, client_hostname text
, client_port integer
, backend_start timestamp with time zone
, xact_start timestamp with time zone
, query_start timestamp with time zone
, state_change timestamp with time zone
, wait_event_type text
, wait_event text
, state text
, backend_xid xid
, backend_xmin xid
, query text
, sniped timestamp with time zone
);

create or replace function snipe_process(state_in character varying, duration integer, live boolean default false)
returns integer
as
$body$
declare 
      st alias for $1;
      du alias for $2;
      li alias for $3;
      itvl character varying;
      start_before timestamp with time zone;

      psCur cursor( st_before timestamp with time zone )
      for
      select pid
           , datname
           , usesysid
           , usename
           , application_name
           , client_addr
           , client_hostname
           , client_port
           , backend_start::timestamp(0) as backend_start
           , xact_start::timestamp(0) as xact_start
           , query_start::timestamp(0) as query_start
           , state_change::timestamp(0) as state_change
           , wait_event_type
           , wait_event
           , state
           , backend_xid
           , backend_xmin
           , query
        from pg_stat_activity
       where state = st
         and query_start <= st_before;

        psRec record;
begin
    itvl := du || ' seconds';
    itvl := quote_literal(itvl);
    start_before := clock_timestamp() - itvl::interval;

    open psCur(start_before);
    loop
       fetch psCur into psRec;
       exit when not found;
       if li
       then
          insert into sniped_processes
               ( pid
               , datname
               , usesysid 
               , usename
               , application_name
               , client_addr
               , client_hostname
               , client_port
               , backend_start
               , xact_start
               , query_start
               , state_change
               , wait_event_type
               , wait_event
               , state
               , backend_xid
               , backend_xmin
               , query
               , sniped
               )
          values
               ( psRec.pid
               , psRec.datname
               , psRec.usesysid 
               , psRec.usename
               , psRec.application_name
               , psRec.client_addr
               , psRec.client_hostname
               , psRec.client_port
               , psRec.backend_start
               , psRec.xact_start
               , psRec.query_start
               , psRec.state_change
               , psRec.wait_event_type
               , psRec.wait_event
               , psRec.state
               , psRec.backend_xid
               , psRec.backend_xmin
               , psRec.query
               , clock_timestamp()
               );

               perform pg_terminate_backend(psRec.pid);
       else
          raise notice 'pid would have been killed: %', psRec.pid;
       end if;            

    end loop;
    close psCur;

    return 0;
end;
$body$
language plpgsql;

    