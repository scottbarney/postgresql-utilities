create or replace function snipe_process(state_in character varying, duration integer, live boolean default false)
returns integer
as
$body$
declare 
      st alias for $1;
      du alias for $2;
      li alias for $3;
      itvl character varying;
      start_before timestamp with time zone;

      psCur cursor( st_before timestamp with time zone )
      for
      select procpid           
        from pg_stat_activity
       where state = st
         and query_start <= st_before;

        psRec record;
begin
    itvl := du || ' seconds';
    itvl := quote_literal(itvl);
    start_before := clock_timestamp() - itvl::interval;

    open psCur(start_before);
    loop
       fetch psCur into psRec;
       exit when not found;
       if li
       then
          perform pg_terminate_backend(psRec.procpid);
       else
          raise notice 'pid would have been killed: %', psRec.procpid;
       end if;            

    end loop;
    close psCur;

    return 0;
end;
$body$
language plpgsql;

    
