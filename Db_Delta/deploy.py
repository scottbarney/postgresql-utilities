#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2
import sys
import os
import ConfigParser
import getopt

def install(dpath,cnx):
   try:

     con = None 
     print cnx
     cs = cnx
     con = psycopg2.connect(cs) 
     con.set_session(autocommit=True)
     cur = con.cursor()

     for subdir, dirs, files in sorted(os.walk(dpath)):
        for file in files:
            
            filepath = subdir + os.sep + file           
            if filepath.endswith(".sql"):    
               print "file: " + filepath
               cur.execute(open(filepath,"r").read())

   except psycopg2.DatabaseError, e:
       print 'Error %s' % e    
    
    
   finally:
       if con:
           con.close()

def main(argv):

   configFile = ""
   try:
     opts, args = getopt.getopt(argv,"hc:",["cfile="])

   except getopt.GetoptError:
     print 'deploy.py -c <config file>'
     sys.exit(2)

   for opt,arg in opts:
      if opt == '-c':
         configFile = arg
   print "using configuration: " + configFile

   config = ConfigParser.ConfigParser()
   config.read(configFile)

   # get config
   db = config.get('db','name')
   host = config.get('db','host')
   port = config.get('db','port')
   user = config.get('db','user')
   cpath = config.get('dir','path')

   cs = "dbname=" + db + " host=" + host + " port=" + port + " user=" + user
   
   print cs
   print cpath
   install(cpath,cs)


if __name__ == "__main__":
   main(sys.argv[1:])
