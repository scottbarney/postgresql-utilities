# DbDelta to Compare Databases

    Database/Schema comparison utility with optional optimistic syncrhonization
    Copyright (C) 2019 Scott Barney

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


	* to install:
		* setup deploy.conf to point to the source/destination db (run at least twice) 
			- dbdelta schema objects are required to be installed on each database being compared
		* python deploy.py -c deploy.conf	

	* usage: python dbdelta.py -c dbdelta.conf
	* must use dbdelta.conf template or a copy of
		- can be copied and passed in to support multiple database deltas
	* dbOne - is the master "to be compared to" database
	* dbTwo - is the destination db ( the one being compared against the master (dbOne)
	* filePath - where the deltas are to be written
	* sync - a toggle to turn on/off synchronization. 
		If an object is missing, it will attempt to optimistically add if conditions are satisfactory. Capability is limited.
		
		