CREATE OR REPLACE FUNCTION dbdelta.get_inherits(
    schema_in character varying,
    table_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      t_in alias for $2;

      out_parent text;
      
begin
    select bns.nspname || '.' || bt.relname as parent 
      into out_parent
      from pg_class ct 
      join pg_namespace cns on ct.relnamespace = cns.oid and cns.nspname = s_in 
      join pg_inherits i on i.inhrelid = ct.oid and ct.relname = t_in
      join pg_class bt on i.inhparent = bt.oid 
      join pg_namespace bns on bt.relnamespace = bns.oid;

    if out_parent is not null
    then
       out_parent := chr(10) || 'inherits (' || out_parent || ')';
    end if;

    return out_parent;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.get_inherits(character varying, character varying)
  OWNER TO postgres;
