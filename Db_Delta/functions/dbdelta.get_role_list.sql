CREATE OR REPLACE FUNCTION dbdelta.get_role_list()
  RETURNS TABLE(roles character varying) AS
$BODY$
begin
    return query select rolname::character varying
                  from pg_authid
                 order by rolname;

end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION dbdelta.get_role_list()
  OWNER TO postgres;
