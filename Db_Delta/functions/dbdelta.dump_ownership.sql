create or replace function dbdelta.dump_ownership(owner_in character varying) returns text
as
$body$
declare
      o_in alias for $1;

      oCur cursor
      for
      select n.nspname as schema_name
           , c.relname as object_name
           , case c.relkind when 'r' then 'table'
                            when 'v' then 'view'
                            when 'm' then 'materialized view'
                            when 'i' then 'index'
                            when 'S' then 'sequence'
                            when 's' then 'special'
                            when 'f' then 'foreign table' end as object_type
           , pg_catalog.pg_get_userbyid(c.relowner) as object_owner
        from pg_catalog.pg_class c
        left join pg_catalog.pg_namespace n on n.oid = c.relnamespace
       where c.relkind in ( 'r','v','m','S','f','')
         and n.nspname not in ( 'pg_catalog','information_schema')
         and n.nspname !~ '^pg_toast'
         and pg_catalog.pg_get_userbyid(c.relowner) = o_in
       order by 1,2;

      oRec record;
      o_ddl text;
begin
    open oCur;
    loop
       fetch oCur into oRec;
       exit when not found;

       if o_ddl is null
       then
          o_ddl := 'alter ' || oRec.object_type || ' ' || oRec.schema_name || '.' || oRec.object_name || ' owner to ' || oRec.object_owner || ';' || chr(10);
       else
          o_ddl := o_ddl ||
                   'alter ' || oRec.object_type || ' ' || oRec.schema_name || '.' || oRec.object_name || ' owner to ' || oRec.object_owner || ';' || chr(10);
       end if;

    end loop;
    close oCur;

    if o_ddl is null
    then
       o_ddl := '-- user does not own any objects';
    end if;

    return o_ddl;
end;
$body$
language plpgsql;
