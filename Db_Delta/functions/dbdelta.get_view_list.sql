create or replace function dbdelta.get_view_list(schema_in character varying) returns table ( views character varying )
as
$body$
declare
      in_s alias for $1;
begin 
    return query select viewname::character varying
                   from dbdelta.delta_views
                  where schemaname = in_s
                  order by viewname;
end;
$body$
language plpgsql;