CREATE OR REPLACE FUNCTION dbdelta.dump_role(role_in character varying)
  RETURNS text AS
$BODY$
declare
      in_role alias for $1;

      rCur cursor
      for
      select oid
           , rolname
           , rolsuper
           , rolinherit
           , rolcreaterole
           , rolcreatedb
           , rolcanlogin
           , rolconnlimit
           , rolpassword
           , rolvaliduntil
           , rolreplication
           , rolbypassrls
           , pg_catalog.shobj_description(oid,'pg_authid') as rolcomment
           , rolname = current_user as is_current_user
        from pg_authid
       where rolname = in_role
       order by 2;

      rmCur cursor(role_in text) 
      for
      select ur.rolname AS roleid
           , um.rolname AS member
           , a.admin_option
           , ug.rolname AS grantor 
        from pg_auth_members a 
        left join pg_authid ur on ur.oid = a.roleid
        left join pg_authid um on um.oid = a.member 
        left join pg_authid ug on ug.oid = a.grantor
       where not (ur.rolname ~ '^pg_' AND um.rolname ~ '^pg_')
         and um.rolname = role_in
       order by 1,2,3;
       
      rRec record;   
      rmRec record;
          
      s_out text;
begin

    open rCur;
    loop
       fetch rCur into rRec;
       exit when not found;

       if s_out is null
       then
          s_out := chr(10) || 'create role ' || rRec.rolname || ';' || chr(10);
       else
          s_out := s_out || chr(10) || 'create role ' || rRec.rolname || ';' || chr(10);
       end if;
              
       if rRec.rolsuper
       then 
          s_out := s_out || 'alter role ' || rRec.rolname || ' superuser;' || chr(10);
       else
          s_out := s_out || 'alter role ' || rRec.rolname || ' nosuperuser;' || chr(10);            
       end if;
       if rRec.rolinherit
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' inherit;' || chr(10);
       else
          s_out := s_out || 'alter role ' || rRec.rolname || ' noinherit;' || chr(10);
       end if;
       if rRec.rolcreaterole
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' createrole;' || chr(10);
       else
          s_out := s_out || 'alter role ' || rRec.rolname || ' nocreaterole;' || chr(10);              
       end if;
       if rRec.rolcreatedb
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' createdb;' || chr(10);
       else
          s_out := s_out || 'alter role ' || rRec.rolname || ' nocreatedb;' || chr(10);              
       end if;   
       if rRec.rolcanlogin
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' login;' || chr(10);
       else
          s_out := s_out || 'alter role ' || rRec.rolname || ' nologin;' || chr(10);              
       end if;
       if rRec.rolreplication
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' replication;' || chr(10);
       else
          s_out := s_out || 'alter role ' || rRec.rolname || ' noreplication;' || chr(10);              
       end if;      
       if rRec.rolbypassrls
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' bypassrls;' || chr(10);
       else
          s_out := s_out || 'alter role ' || rRec.rolname || ' nobypassrls;' || chr(10);              
       end if;       
       if rRec.rolconnlimit != -1
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' connection limit ' || rRec.rolconnlimit || ';' || chr(10);             
       end if;    
       if rRec.rolpassword is not null
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' password ' || rRec.rolpassword || ';' || chr(10);
       end if;
       if rRec.rolvaliduntil is not null
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' valid until ' || rRec.rolvaliduntil || ';' || chr(10);
       end if;
       if rRec.rolcomment is not null
       then
          s_out := s_out || 'alter role ' || rRec.rolname || ' comment on role is ' || rRec.rolcomment || ';' || chr(10);
       end if;

       open rmCur(rRec.rolname);
       loop
          fetch rmCur into rmRec;
          exit when not found;

          s_out := s_out || 'grant ' || rmRec.roleid || ' to ' || rmRec.member;

          if rmRec.admin_option
          then
             s_out := s_out || ' with admin option; ' || chr(10);
          else
             s_out := s_out || ';' || chr(10);
          end if;
          
       end loop;
       close rmCur;
                   
    end loop;
    close rCur;

    return s_out;
    
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_role(character varying)
  OWNER TO postgres;
