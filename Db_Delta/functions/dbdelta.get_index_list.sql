CREATE OR REPLACE FUNCTION dbdelta.get_index_list(IN schema_in character varying)
  RETURNS TABLE(idx character varying) AS
$BODY$
declare
      s_in alias for $1;

begin
    return query select relname::character varying
                   from pg_index i
                      , pg_class c
                   join pg_namespace n on n.oid = c.oid
                  where i.indexrelid = c.oid
                    and c.relname not like 'pg%'
                    and n.nspname = s_in
                    and indrelid not in ( select conrelid
                                            from pg_constraint );
 
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION dbdelta.get_index_list(character varying)
  OWNER TO postgres;
