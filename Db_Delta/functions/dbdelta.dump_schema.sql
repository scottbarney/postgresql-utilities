CREATE OR REPLACE FUNCTION dbdelta.dump_schema(schema_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      
      sCur cursor
      for
      select n.nspname::character varying, a.rolname
        from pg_catalog.pg_namespace n
           , pg_catalog.pg_authid a
       where n.nspowner = a.oid
         and n.nspname = s_in;      

      sRec record;
      s_out text;
begin
    open sCur;
    loop
       fetch sCur into sRec;
       exit when not found;

       s_out := 'create schema ' || sRec.nspname || ' authorization ' || sRec.rolname || ';';

    end loop;
    close sCur;

    return s_out;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_schema(character varying)
  OWNER TO postgres;
