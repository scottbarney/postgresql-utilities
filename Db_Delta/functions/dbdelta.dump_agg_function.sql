CREATE OR REPLACE FUNCTION dbdelta.dump_agg_function(
    schema_in character varying,
    agg_function_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      af_in alias for $2;

      v_ddl text;

      vCur cursor
      for
        SELECT 'create aggregate '||n.nspname||'.'||p.proname||'('||format_type(a.aggtranstype, null)||') (sfunc = '||a.aggtransfn
          ||', stype = '||format_type(a.aggtranstype, null)
          ||case when op.oprname is null then '' else ', sortop = '||op.oprname end
          ||case when a.agginitval is null then '' else ', initcond = '||a.agginitval end
          ||');' as source
        FROM pg_proc p
          JOIN pg_namespace n ON p.pronamespace = n.oid
          JOIN pg_aggregate a ON a.aggfnoid = p.oid
          LEFT JOIN pg_operator op ON op.oid = a.aggsortop
        where p.proname = af_in
          and n.nspname = s_in;

     vRec record;
begin
    open vCur;
    loop
       fetch vCur into vRec;
       exit when not found;

       v_ddl := vRec.source;
    end loop;
    close vCur;

    return v_ddl;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_agg_function(character varying, character varying)
  OWNER TO postgres;