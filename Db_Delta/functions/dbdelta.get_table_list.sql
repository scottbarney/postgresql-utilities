create or replace function dbdelta.get_table_list( schema_in character varying ) returns table ( tab_names character varying )
as
$body$
declare
      s_in alias for $1;
begin
    return query select c.relname::character varying
                  from pg_catalog.pg_class c
                  left join pg_catalog.pg_namespace n ON n.oid = c.relnamespace
                 where c.relkind = 'r'
                   and n.nspname = s_in
                 order by 1;
end;
$body$
language plpgsql;