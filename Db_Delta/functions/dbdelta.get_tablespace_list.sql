CREATE OR REPLACE FUNCTION dbdelta.get_tablespace_list()
  RETURNS table ( tbs character varying ) AS
$BODY$
declare
 
begin  
    return query select spcname::character varying 
      from pg_catalog.pg_tablespace      
     where spcname !~ '^pg_'      
     order by 1;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.get_tablespace_list()
  OWNER TO postgres;
