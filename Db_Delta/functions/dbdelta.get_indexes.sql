CREATE OR REPLACE FUNCTION dbdelta.get_indexes(
    schema_in character varying,
    table_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      t_in alias for $2;

      tab_oid oid;

      iCur cursor( t_oid oid )
      for
      select indexrelid
        from pg_index
       where indrelid = t_oid
         and indrelid not in ( select conrelid
                                 from pg_constraint );
       
      iRec record;

      i_ddl text;
begin
    select c.oid
      into tab_oid
      from pg_catalog.pg_class c
      left join pg_catalog.pg_namespace n ON n.oid = c.relnamespace
     where n.nspname = s_in
       and c.relname = t_in;

   open iCur(tab_oid);
   loop
      fetch iCur into iRec;
      exit when not found;

      if i_ddl is null
      then
         i_ddl := pg_get_indexdef(iRec.indexrelid) || ';';
      else
         i_ddl := chr(10) || pg_get_indexdef(iRec.indexrelid) || ';';
      end if;
   end loop;
   close iCur;

   return i_ddl;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.get_indexes(character varying, character varying)
  OWNER TO postgres;
