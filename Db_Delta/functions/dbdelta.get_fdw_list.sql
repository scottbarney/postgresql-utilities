CREATE OR REPLACE FUNCTION dbdelta.get_fdw_list(IN schema_in character varying)
  RETURNS TABLE(fdw character varying) AS
$BODY$
declare
      s_in alias for $1;
begin
    return query select c.relname::character varying
                   from pg_catalog.pg_class c
                   join pg_catalog.pg_foreign_table ft on c.oid = ft.ftrelid
                   left join pg_catalog.pg_namespace n on n.oid = c.relnamespace
                  where n.nspname = s_in
                  order by c.relname;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION dbdelta.get_fdw_list(character varying)
  OWNER TO postgres;
