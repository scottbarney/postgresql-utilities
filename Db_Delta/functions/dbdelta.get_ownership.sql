create or replace function dbdelta.get_ownership() returns table ( owns character varying )
as
$body$
begin
    return query select distinct pg_catalog.pg_get_userbyid(relowner)::character varying as obj_owner
                   from pg_class
                  order by pg_catalog.pg_get_userbyid(relowner)::character varying;
end;
$body$
language plpgsql;