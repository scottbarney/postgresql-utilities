create or replace function dbdelta.dump_grants(grantee_in character varying) returns text
as
$body$
declare
      g_in alias for $1;
      g_ddl text;
      
      tCur cursor
      for
      select table_schema
           , table_name
           , string_agg(privilege_type,', ') as privs
        from information_schema.role_table_grants
       where grantee = g_in
       group by table_schema
              , table_name;

      rCur cursor
      for
      select routine_schema
           , routine_name
           , string_agg(privilege_type,', ') as privs
        from information_schema.routine_privileges
       where grantee = g_in
       group by routine_schema
              , routine_name;

     tRec record;
     rRec record;
begin
    open tCur;
    loop
       fetch tCur into tRec;
       exit when not found;

       if g_ddl is null
       then
          g_ddl := 'grant ' || tRec.privs || ' on table ' || tRec.table_schema || '.' || tRec.table_name || ' to ' || g_in || ';' || chr(10);
       else
          g_ddl := g_ddl || 
                   'grant ' || tRec.privs || ' on table ' || tRec.table_schema || '.' || tRec.table_name || ' to ' || g_in || ';' || chr(10);
       end if;
    end loop;
    close tCur;

 return g_ddl;
 end;
 $body$
 language plpgsql;