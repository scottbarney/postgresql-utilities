create or replace function dbdelta.get_grantees() returns table ( grnt character varying )
as
$body$
begin
    return query select distinct grantee::character varying
                   from information_schema.role_table_grants
                  where grantee not in ( 'postgres','PUBLIC' )
                  order by grantee::character varying;
end;
$body$
language plpgsql;
