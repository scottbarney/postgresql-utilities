CREATE OR REPLACE FUNCTION dbdelta.dump_materialized_view(
    schema_in character varying,
    view_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      v_in alias for $2;

      v_ddl text;
      i_ddl text;

      vCur cursor 
      for
      select schemaname
           , viewname
           , definition
        from dbdelta.materialized_views
       where schemaname = s_in
         and viewname = v_in;

     vRec record;
begin
    open vCur;
    loop
       fetch vCur into vRec;
       exit when not found;

       v_ddl := 'create materialized view ' || vRec.schemaname || '.' || vRec.viewname || chr(10) ||
                'as' || chr(10) ||
                vRec.definition;
    end loop;
    close vCur;

    i_ddl := dbdelta.get_indexes(s_in,v_in);

    if i_ddl is not null
    then
       v_ddl :=  v_ddl || chr(10) || chr(10) || i_ddl;
    end if;
    
    return v_ddl;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_materialized_view(character varying, character varying)
  OWNER TO postgres;
