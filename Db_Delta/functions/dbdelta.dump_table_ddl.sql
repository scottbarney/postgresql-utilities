CREATE OR REPLACE FUNCTION dbdelta.dump_table_ddl(
    schema_in character varying,
    table_in character varying)
  RETURNS text AS
$BODY$
declare
    s_in alias for $1;
    t_in alias for $2;

    hasoids boolean;
    ddl  text;
    pk text;
    inh text;
    rec record;
begin
    for rec 
    in 
        select b.nspname as schema_name
             , b.relname as table_name
             , a.attname as column_name
             , pg_catalog.format_type(a.atttypid, a.atttypmod) as column_type,
            case when 
                (select substring(pg_catalog.pg_get_expr(d.adbin, d.adrelid) for 128)
                   from pg_catalog.pg_attrdef d
                  where d.adrelid = a.attrelid and d.adnum = a.attnum and a.atthasdef) is not null 
                then
                'default '|| (select substring(pg_catalog.pg_get_expr(d.adbin, d.adrelid) for 128)
                                from pg_catalog.pg_attrdef d
                               where d.adrelid = a.attrelid and d.adnum = a.attnum and a.atthasdef)
            else
                ''
            end as column_default_value,
            case when a.attnotnull = true 
            then 
                'not null'
            else
                ''
            end as column_not_null,
            a.attnum as attnum,
            e.max_attnum as max_attnum
        from pg_catalog.pg_attribute a
        inner join ( select c.oid
                          , n.nspname
                          , c.relname
                       from pg_catalog.pg_class c
                       left join pg_catalog.pg_namespace n ON n.oid = c.relnamespace
                      where n.nspname = s_in
                        and c.relname = t_in
                      order by 2, 3 ) b on a.attrelid = b.oid
        inner join ( select a.attrelid
                          , max(a.attnum) as max_attnum
                       from pg_catalog.pg_attribute a
                      where a.attnum > 0 
                        and not a.attisdropped
                      group by a.attrelid ) e on a.attrelid=e.attrelid
        where a.attnum > 0 
          and NOT a.attisdropped
         order by a.attnum
    loop
        if rec.attnum = 1 
        then
            ddl := 'create table '|| rec.schema_name || '.' || rec.table_name || ' (';
        else
            ddl := ddl || ',';
        end if;

        if rec.attnum <= rec.max_attnum 
        then
            ddl:= ddl || chr(10) ||
                           '    ' || rec.column_name ||
                           ' ' || rec.column_type || 
                           ' ' || rec.column_default_value ||
                           ' ' || rec.column_not_null;
        end if;
    end loop;

    ddl := ddl || chr(10) || ')';

    inh := dbdelta.get_inherits(s_in, t_in);

    if inh is not null
    then
       ddl := ddl || inh;
    end if;
   
    select relhasoids 
      into hasoids 
      from pg_catalog.pg_class c
      join pg_catalog.pg_namespace n on c.relnamespace = n.oid
     where c.relname = t_in::name
       and n.nspname = s_in::name;

    if hasoids
    then
       ddl:= ddl || chr(10) || 'with (' || chr(10) || '  OIDS=TRUE' || chr(10) || ');';
    else
       ddl:= ddl || chr(10) || 'with (' || chr(10) || '  OIDS=FALSE' || chr(10) || ');';
    end if;


    
    return ddl;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_table_ddl(character varying, character varying)
  OWNER TO postgres;
