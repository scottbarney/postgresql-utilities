create or replace function dbdelta.dump_fdw( schema_in character varying, fdw_in character varying ) returns text
as
$body$
declare
      s_in alias for $1;
      f_in alias for $2;

      foid oid;
      f_ddl text;

    fCur cursor(foid oid)
    for
    select n.nspname as schema_name
         , c.relname as fdw_name
         , pg_get_userbyid(relowner) as ft_owner
         , ftoptions::character varying as ftoptions
         , srvname
      from pg_catalog.pg_class c
      join pg_catalog.pg_foreign_table ft on c.oid = ft.ftrelid
      left join pg_catalog.pg_namespace n on n.oid = c.relnamespace
      left outer join pg_catalog.pg_foreign_server fs on ft.ftserver = fs.oid
      left outer join pg_catalog.pg_description des on ( des.objoid = c.oid and des.classoid = 'pg_class'::regclass)
     where c.oid = foid;

     faCur cursor(foid oid)
     for
     select attname
          , format_type(t.oid,NULL) as typname
          , attndims
          , atttypmod
          , nspname
          , attnotnull
       from pg_catalog.pg_attribute att
       join pg_catalog.pg_type t on t.oid = atttypid
       join pg_catalog.pg_namespace nsp on t.typnamespace = nsp.oid
       left outer join pg_catalog.pg_type b on t.typelem = b.oid
      where att.attrelid = foid
        and attnum > 0
      order by attnum;

      fRec record;
      faRec record;

      dt integer;
      opt character varying;
      sch_opt character varying;
      sch_n_opt character varying;
      tab_opt character varying;
      tab_n_opt character varying;

begin
    select c.oid
      into foid
      from pg_catalog.pg_class c
      join pg_catalog.pg_foreign_table ft on c.oid = ft.ftrelid
      left join pg_catalog.pg_namespace n on n.oid = c.relnamespace
     where n.nspname = s_in
       and c.relname = f_in;

    f_ddl := 'create foreign table ' || s_in || '.' || f_in || ' (' || chr(10);

    open faCur(foid);
    loop
       fetch faCur into faRec;
       exit when not found;

       if faRec.atttypmod > 0
       then
          dt := faRec.atttypmod - 4;
          f_ddl := f_ddl || '    ' || faRec.attname || ' ' || faRec.typname || '(' || dt || '),' || chr(10);
       else
          f_ddl := f_ddl || '    ' || faRec.attname || ' ' || faRec.typname || ',' || chr(10);
       end if;
    end loop;
    close faCur;

    f_ddl := substring(f_ddl from 1 for length(f_ddl) - 2) || chr(10) || ')' || chr(10);

    open fCur(foid);
    loop
       fetch fCur into fRec;
       exit when not found;

       f_ddl := f_ddl || 'server ' || fRec.srvname || chr(10);
       f_ddl := f_ddl || 'options (';

       f_ddl := f_ddl || replace(trim(trailing '}' from trim(leading '{' from fRec.ftoptions)),'=',' ') || ');';
    end loop;
    close fCur;

    raise notice '%', f_ddl;
    return f_ddl;
end;
$body$
language plpgsql;