create or replace function dbdelta.get_trigger( schema_in character varying, table_in character varying ) returns text
as
$body$
declare
      s_in alias for $1;
      t_in alias for $2;

      t_ddl text;
      tab_oid oid;

      tCur cursor( t_oid oid )
      for
      select oid
        from pg_trigger
       where tgrelid = t_oid;

      tRec record;
begin
    select c.oid
      into tab_oid
      from pg_catalog.pg_class c
      left join pg_catalog.pg_namespace n ON n.oid = c.relnamespace
     where n.nspname = s_in
       and c.relname = t_in;

    open tCur(tab_oid);
    loop
       fetch tCur into tRec;
       exit when not found;

       if t_ddl is null
       then
          t_ddl := pg_get_triggerdef( tRec.oid );
       else
          t_ddl := chr(10) || t_ddl;
       end if;
    end loop;
    close tCur;

    return t_ddl;
end;
$body$
language plpgsql;