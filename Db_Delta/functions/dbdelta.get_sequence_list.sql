create or replace function dbdelta.get_sequence_list( schema_in character varying ) returns
table ( seqs character varying ) as
$body$
declare
      in_s alias for $1;
begin
    return query select cl.relname::character varying
                   from pg_catalog.pg_class cl
                   left join pg_catalog.pg_namespace n on n.oid = cl.relnamespace
                  where relkind = 'S'
                    and n.nspname = in_s
                  order by cl.relname;
end;
$body$
language plpgsql;