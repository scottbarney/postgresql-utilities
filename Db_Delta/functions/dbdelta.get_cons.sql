CREATE OR REPLACE FUNCTION dbdelta.get_cons(
    schema_in character varying,
    table_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      t_in alias for $2;
      tab_oid oid;
      ddl text;

    pCur cursor( t_oid oid )
    for
    select 'alter table ' || nspname || '.' || relname || ' add constraint ' || conname ||' '||
           pg_get_constraintdef(pg_constraint.oid)||';' as fkey
      from pg_constraint
      inner join pg_class on conrelid=pg_class.oid
      inner join pg_namespace ON pg_namespace.oid=pg_class.relnamespace
      where pg_class.oid = t_oid
      order by case when contype='f' then 0 else 1 end desc
             , contype desc
             , nspname desc
             , relname desc
             , conname desc;

    pRec record;
begin
    select c.oid
      into tab_oid
      from pg_catalog.pg_class c
      left join pg_catalog.pg_namespace n ON n.oid = c.relnamespace
     where n.nspname = s_in
       and c.relname = t_in;

    open pCur(tab_oid);
    loop
       fetch pCur into pRec;
       exit when not found;

       if ddl is null
       then
          ddl := pRec.fkey;
       else 
          ddl := ddl || chr(10) || pRec.fkey;
       end if;
       
    end loop;
    close pCur;

    return ddl;
      
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
