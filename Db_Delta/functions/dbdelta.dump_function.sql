CREATE OR REPLACE FUNCTION dbdelta.dump_function(
    schema_in character varying,
    function_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      f_in alias for $2;

      f_ddl text;

      fCur cursor
      for
      select n.nspname AS schema_name
           , p.proname AS function_name
           , pg_get_functiondef(p.oid) AS func_def
           , pg_get_function_arguments(p.oid) AS args
           , pg_get_function_result(p.oid) AS result
        from pg_proc p
        join pg_namespace n ON n.oid = p.pronamespace
       where n.nspname = s_in
         and p.proname = f_in
       order by pg_get_function_arguments(p.oid);

      fRec record;

begin
    open fCur;
    loop
       fetch fCur into fRec;
       exit when not found;

       if f_ddl is null
       then
          f_ddl := fRec.func_def || ';';
       else
          f_ddl := f_ddl || chr(10) || fRec.func_def || ';';
       end if;
    end loop;
    close fCur;

    return f_ddl;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_function(character varying, character varying)
  OWNER TO postgres;
