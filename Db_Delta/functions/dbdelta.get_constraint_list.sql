CREATE OR REPLACE FUNCTION dbdelta.get_constraint_list(
    schema_in character varying)
  RETURNS table ( cons character varying ) AS
$BODY$
declare
      s_in alias for $1;

begin
    return query select conname::character varying
                   from pg_constraint
                   inner join pg_class on conrelid=pg_class.oid
                   inner join pg_namespace ON pg_namespace.oid=pg_class.relnamespace
                   where pg_namespace.nspname = s_in
                   order by case when contype='f' then 0 else 1 end desc
                          , contype desc
                          , nspname desc
                          , relname desc
                          , conname desc;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
