CREATE OR REPLACE FUNCTION dbdelta.dump_tablespace(tbs_in character varying)
  RETURNS text AS
$BODY$
declare
      in_tbs alias for $1;
      
      tCur cursor
      for
      select oid
           , spcname
           , pg_catalog.pg_get_userbyid(spcowner) AS spcowner
           , pg_catalog.pg_tablespace_location(oid)
           , ( select pg_catalog.array_agg(acl) 
                 from ( select pg_catalog.unnest(coalesce(spcacl,pg_catalog.acldefault('t',spcowner))) AS acl      
                        except 
                        select pg_catalog.unnest(pg_catalog.acldefault('t',spcowner))) as foo) AS spcacl
                               , ( select pg_catalog.array_agg(acl) 
                                     from ( select pg_catalog.unnest(pg_catalog.acldefault('t',spcowner)) AS acl      
                                            except 
                                            select pg_catalog.unnest(coalesce(spcacl,pg_catalog.acldefault('t',spcowner)))) as foo) AS rspcacl
                                                 , array_to_string(spcoptions, ', ')
                                                 , pg_catalog.shobj_description(oid, 'pg_tablespace')      
        from pg_catalog.pg_tablespace      
       where spcname = tbs_in     
       order by 1;

       tRec record;
       s_out text;
begin    
    open tCur;
    loop
       fetch tCur into tRec;
       exit when not found;

       if s_out is null
       then
          s_out := 'create tablespace ' || tRec.spcname || 
                   ' owner ' || tRec.spcowner || 
                   ' location ' || quote_literal(tRec.pg_tablespace_location) || ';' || chr(10);
       end if;
       
    end loop;
    close tCur;

    return s_out;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_tablespace(character varying)
  OWNER TO postgres;
