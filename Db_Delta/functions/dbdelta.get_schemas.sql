CREATE OR REPLACE FUNCTION dbdelta.get_schemas()
  RETURNS TABLE(schema_name character varying) AS
$BODY$
begin
return query select nspname::character varying 
               from pg_catalog.pg_namespace
              where nspname not like 'pg%' 
                and nspname != 'information_schema'
              order by nspname;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION dbdelta.get_schemas()
  OWNER TO postgres;