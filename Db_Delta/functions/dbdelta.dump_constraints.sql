CREATE OR REPLACE FUNCTION dbdelta.dump_constraints(
    schema_in character varying,
    constraint_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      c_in alias for $2;
      ddl text;

    pCur cursor
    for
    select 'alter table ' || nspname || '.' || relname || ' add constraint ' || conname ||' '||
           pg_get_constraintdef(pg_constraint.oid)||';' as fkey
      from pg_constraint
      inner join pg_class on conrelid=pg_class.oid
      inner join pg_namespace ON pg_namespace.oid=pg_class.relnamespace
      where conname = c_in
      order by case when contype='f' then 0 else 1 end desc
             , contype desc
             , nspname desc
             , relname desc
             , conname desc;

    pRec record;
begin
    open pCur;
    loop
       fetch pCur into pRec;
       exit when not found;

       if ddl is null
       then
          ddl := pRec.fkey;
       else 
          ddl := ddl || chr(10) || pRec.fkey;
       end if;
       
    end loop;
    close pCur;

    return ddl;
      
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_constraints(character varying, character varying)
  OWNER TO postgres;
