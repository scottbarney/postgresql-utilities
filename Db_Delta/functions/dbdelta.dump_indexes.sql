CREATE OR REPLACE FUNCTION dbdelta.dump_indexes(
    schema_in character varying,
    index_in character varying)
  RETURNS text AS
$BODY$
declare
      s_in alias for $1;
      i_in alias for $2;


      iCur cursor
      for
      select indexrelid
        from pg_index
           , pg_class
       where relname = i_in
         and indrelid not in ( select conrelid
                                 from pg_constraint );
       
      iRec record;

      i_ddl text;
begin
   open iCur;
   loop
      fetch iCur into iRec;
      exit when not found;

      if i_ddl is null
      then
         i_ddl := pg_get_indexdef(iRec.indexrelid) || ';';
      else
         i_ddl := chr(10) || pg_get_indexdef(iRec.indexrelid) || ';';
      end if;
   end loop;
   close iCur;

   return i_ddl;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dbdelta.dump_indexes(character varying, character varying)
  OWNER TO postgres;
