create or replace function dbdelta.dump_type(schema_in character varying, type_in character varying) returns text
as
$body$
declare
      s_in alias for $1;
      t_in alias for $2;

      toid oid;
      t_ddl text;

      tCur cursor(toid oid)
      for 
      select attname
           , format_type(t.oid,NULL) as typname
        from pg_catalog.pg_attribute att
        join pg_catalog.pg_type t on t.oid = atttypid
        join pg_catalog.pg_namespace nsp on t.typnamespace = nsp.oid
        left outer join pg_catalog.pg_type b on t.typelem = b.oid
        left outer join pg_catalog.pg_collation c on att.attcollation = c.oid
        left outer join pg_catalog.pg_namespace nspc on c.collnamespace = nspc.oid
       where att.attrelid = toid
       order by attnum;

      tRec record;
begin
    select cl.oid
      into toid
      from pg_catalog.pg_class cl
      left join pg_catalog.pg_namespace n on n.oid = cl.relnamespace
     where relkind = 'c'
       and cl.relname = t_in
       and n.nspname = s_in;

    t_ddl := 'create type ' || s_in || '.' || t_in || ' as ' || chr(10) || '(' || chr(10);
    
    open tCur(toid);
    loop
       fetch tCur into tRec;
       exit when not found;

       t_ddl := t_ddl || '  ' || tRec.attname || ' ' || tRec.typname || ',' || chr(10);
    end loop;
    close tCur;

    t_ddl := substring(t_ddl,1,length(t_ddl)-2);
    t_ddl := t_ddl || chr(10) || ');';

    return t_ddl;
end;
$body$
language plpgsql;