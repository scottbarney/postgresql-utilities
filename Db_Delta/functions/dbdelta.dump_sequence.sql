CREATE OR REPLACE FUNCTION dbdelta.dump_sequence(
    schema_in character varying,
    seq_in character varying)
  RETURNS text AS
$BODY$
declare
      sch_in alias for $1;
      s_in alias for $2;

      s_ddl text;
      s_sql text;
      
      last_val bigint;
      min_val bigint;
      max_val bigint;
      cache_val bigint;
      is_cycled boolean;
      incr_by bigint;
      is_called boolean;
      

begin
    s_sql := 'select last_value
                   , min_value
                   , max_value
                   , cache_value
                   , is_cycled
                   , increment_by
                   , is_called 
                from "' || sch_in || '"."' || s_in || '"';
                
    execute s_sql into last_val, min_val, max_val, cache_val, is_cycled, incr_by, is_called;

    s_ddl := 'create sequence ' || sch_in || '.' || s_in || chr(10) ||
             '  increment ' || incr_by || chr(10) ||
             '  minvalue ' || min_val || chr(10) ||
             '  maxvalue ' || max_val || chr(10) ||
             '  start ' || last_val + 4 || chr(10) ||
             '  cache ' || cache_val || ';' ;
                           
    return s_ddl;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
