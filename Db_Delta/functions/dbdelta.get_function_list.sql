create or replace function dbdelta.get_function_list(schema_in character varying) returns table ( function_name character varying )
as
$body$
declare
      in_s alias for $1;
begin
return query select p.proname::character varying
               from pg_proc p
               join pg_namespace n on n.oid = p.pronamespace
              where n.nspname = in_s
                and p.proisagg = 'f'
              order by p.proname;
end;
$body$
language plpgsql;
