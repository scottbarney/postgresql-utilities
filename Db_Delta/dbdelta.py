#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2
import sys
import os
import ConfigParser
import getopt
from filecmp import dircmp
import shutil
from timeit import default_timer as timer
from time import localtime, strftime



def dump_ns_ddl(cs,db,countFunc,listFunc,ddlFunc,typeDDL,ddlPath):

   try:
      print "     " + typeDDL
      conddl = None
      conddl = psycopg2.connect(cs)

      countCur = conddl.cursor()
      countCur.execute(countFunc)

      while True:
          exists = countCur.fetchone()
          if exists == None:
             break
          else:
             doesExist = exists[0]

      if doesExist > 0:
          new_path = ddlPath + os.sep + typeDDL

          if not os.path.exists(new_path):
             os.makedirs(new_path)

          os.chdir(new_path)

          listCur = conddl.cursor()
          listCur.execute(listFunc)

          while True:
              listRow = listCur.fetchone()
              if listRow == None:
                 break
              else:
                 objName = listRow[0]
                 ddlCur = conddl.cursor()
                 ddlCur.execute(ddlFunc,(objName,))

                 while True:
                     objRow = ddlCur.fetchone()
                     if objRow == None:
                        break
                     else:
                        objDDL = objRow[0]
                        fName = objName + ".sql"
                        file = open(fName,"w")
                        file.write(objDDL)
                        file.close()


   except psycopg2.DatabaseError, e:
       print 'Error %s' % e
       sys.exit(1)


   finally:
       if conddl:
           conddl.close()

def dump_ddl(cs,db,schemaName,countFunc,listFunc,ddlFunc,typeDDL,ddlPath):

   try:
      print "     " + typeDDL
      conddl = None
      conddl = psycopg2.connect(cs)

      countCur = conddl.cursor()
      countCur.execute(countFunc,(schemaName,))

      while True:
          exists = countCur.fetchone()
          if exists == None:
             break
          else:
             doesExist = exists[0]

      if doesExist > 0:
          new_path = ddlPath + os.sep + typeDDL

          if not os.path.exists(new_path):
             os.makedirs(new_path)

          os.chdir(new_path)

          listCur = conddl.cursor()
          listCur.execute(listFunc,(schemaName,))

          while True:
              listRow = listCur.fetchone()
              if listRow == None:
                 break
              else:
                 objName = listRow[0]
                 ddlCur = conddl.cursor()
                 ddlCur.execute(ddlFunc,(schemaName,objName))

                 while True:
                     objRow = ddlCur.fetchone()
                     if objRow == None:
                        break
                     else:
                        objDDL = objRow[0]
                        fName = schemaName + "." + objName + ".sql"
                        lfName = typeDDL + ".list"
                        file = open(fName,"w")
                        file.write(objDDL)
                        file.close()
                        file = open(lfName,"a")
                        file.write(fName + "\n")
                        file.close


   except psycopg2.DatabaseError, e:
       print 'Error %s' % e
       sys.exit(1)


   finally:
       if conddl:
           conddl.close()

def extract(cx,dbhost,db,pth):
   try:
      con = None
      cs = cx

      con = psycopg2.connect(cs)
      file_path = pth + dbhost + os.sep + db

      if not os.path.exists(file_path):
         os.makedirs(file_path)

      os.chdir(file_path)

      schemas = "select * from dbdelta.get_schemas()"

      ownerCount = "select count(*) from dbdelta.get_ownership()"
      ownerList = "select * from dbdelta.get_ownership()"
      ownerDDL = "select dbdelta.dump_ownership(%s)"

      grantCount = "select count(*) from dbdelta.get_grantees()"
      grantList = "select * from dbdelta.get_grantees()"
      grantDDL = "select dbdelta.dump_grants(%s)"

      roleCount = "select count(*) from dbdelta.get_role_list()"
      roleList = "select * from dbdelta.get_role_list()"
      roleDDL = "select dbdelta.dump_role(%s)"

      tbsCount = "select count(*) from dbdelta.get_tablespace_list()"
      tbsList = "select * from dbdelta.get_tablespace_list()"
      tbsDDL = "select dbdelta.dump_tablespace(%s)"

      schemaCount = "select count(*) from dbdelta.get_schema_list()"
      schemaList = "select * from dbdelta.get_schema_list()"
      schemaDDL = "select dbdelta.dump_schema(%s)"

      fdwCount = "select count(*) from dbdelta.get_fdw_list(%s)"
      fdwList = "select * from dbdelta.get_fdw_list(%s)"
      fdwDDL = "select dbdelta.dump_fdw(%s,%s)"

      seqCount = "select count(*) from dbdelta.get_sequence_list(%s)"
      seqList = "select * from dbdelta.get_sequence_list(%s)"
      seqDDL = "select dbdelta.dump_sequence(%s,%s)"

      typeCount = "select count(*) from dbdelta.get_type_list(%s)"
      typeList = "select * from dbdelta.get_type_list(%s)"
      typeDDL = "select dbdelta.dump_type(%s,%s)"

      tableCount = "select count(*) from dbdelta.get_table_list(%s)"
      tableList = "select dbdelta.get_table_list(%s)"
      tableDDL = "select dbdelta.dump_table_ddl(%s,%s)"

      consCount = "select count(*) from dbdelta.get_constraint_list(%s)"
      consList = "select dbdelta.get_constraint_list(%s)"
      consDDL = "select dbdelta.dump_constraints(%s,%s)"

      indexCount = "select count(*) from dbdelta.get_index_list(%s)"
      indexList = "select * from dbdelta.get_index_list(%s)"
      indexDDL = "select dbdelta.dump_indexes(%s,%s)"

      viewCount = "select count(*) from dbdelta.get_view_list(%s)"
      viewList = "select dbdelta.get_view_list(%s)"
      viewDDL = "select dbdelta.dump_view(%s,%s)"

      matViewCount = "select count(*) from dbdelta.get_materialized_view_list(%s)"
      matViewList = "select dbdelta.get_materialized_view_list(%s)"
      matViewDDL = "select dbdelta.dump_materialized_view(%s,%s)"

      funcCount = "select count(*) from dbdelta.get_function_list(%s)"
      funcList = "select dbdelta.get_function_list(%s)"
      funcDDL = "select dbdelta.dump_function(%s,%s)"

      aggFuncCount = "select count(*) from dbdelta.get_agg_function_list(%s)"
      aggFuncList = "select dbdelta.get_agg_function_list(%s)"
      aggFuncDDL = "select dbdelta.dump_agg_function(%s,%s)"

      cluster_path = file_path + os.sep + "cluster"

      if not os.path.exists(cluster_path):
         os.makedirs(cluster_path)

      os.chdir(cluster_path)

      dump_ns_ddl(cs,db,tbsCount,tbsList,tbsDDL,"a_tablespaces",cluster_path)
      dump_ns_ddl(cs,db,roleCount,roleList,roleDDL,"b_roles",cluster_path)
      dump_ns_ddl(cs,db,schemaCount,schemaList,schemaDDL,"c_schemas",cluster_path)
      dump_ns_ddl(cs,db,ownerCount,ownerList,ownerDDL,"m_ownership",cluster_path)
      dump_ns_ddl(cs,db,grantCount,grantList,grantDDL,"n_grants",cluster_path)


      os.chdir(file_path)

      schema_path = file_path + os.sep + "schemas"

      if not os.path.exists(schema_path):
         os.makedirs(schema_path)

      os.chdir(schema_path)

      cur = con.cursor()
      cur.execute(schemas)

      while True:
          row = cur.fetchone()

          if row == None:
             break

          schemaName = row[0]
          new_path = schema_path + os.sep + schemaName

          if not os.path.exists(new_path):
             os.makedirs(new_path)

          os.chdir(new_path)

          print "   schema " + schemaName
          print "   ----------------------------------------"

          dump_ddl(cs,db,schemaName,fdwCount,fdwList,fdwDDL,"k_foreign_data_wrappers",new_path)
          dump_ddl(cs,db,schemaName,seqCount,seqList,seqDDL,"d_sequences",new_path)
          dump_ddl(cs,db,schemaName,typeCount,typeList,typeDDL,"e_types",new_path)
          dump_ddl(cs,db,schemaName,tableCount,tableList,tableDDL,"f_tables",new_path)
          dump_ddl(cs,db,schemaName,indexCount,indexList,indexDDL,"g_indexes",new_path)
          dump_ddl(cs,db,schemaName,consCount,consList,consDDL,"h_constraints",new_path)
          dump_ddl(cs,db,schemaName,viewCount,viewList,viewDDL,"i_views",new_path)
          dump_ddl(cs,db,schemaName,matViewCount,matViewList,matViewDDL,"j_materialized_views",new_path)
          dump_ddl(cs,db,schemaName,funcCount,funcList,funcDDL,"l_functions",new_path)
          dump_ddl(cs,db,schemaName,aggFuncCount,aggFuncList,aggFuncDDL,"o_agg_functions",new_path)


   except psycopg2.DatabaseError, e:
       print 'Error %s' % e
       sys.exit(1)


   finally:
       if con:
           con.close()

def print_diff_files(dcmp,dsync,cs):
    for name in dcmp.diff_files:
        diffcmd = "diff %s%s %s%s" % (dcmp.left + os.sep, name, dcmp.right + os.sep, name)
        print "Difference Found:\n" + dcmp.left + os.sep + name + "\n" + dcmp.right + os.sep + name
        os.system(diffcmd)
        if dsync == 'True' and name.endswith(".sql"):
           sync(dcmp.left + os.sep + name,cs)
        print "\n"
    for sub_dcmp in dcmp.subdirs.values():
        print_diff_files(sub_dcmp,dsync,cs)

def walk(delta_path,db1host,db1,db2host,db2,dsync,cs):
   src_path = delta_path + db1host + os.sep + db1
   com_path = delta_path + db2host + os.sep + db2
   dcmp = dircmp(src_path, com_path)
   print_diff_files(dcmp,dsync,cs)

def gettype(path,src,db):

    dtype = src.replace(path,"")
    dtype = dtype.replace(db,"")
    dtype = dtype.replace(os.sep,"-")
    dtype = dtype[1:len(dtype)]

    st = dtype.find("-")
    dtype = dtype[st+1:len(dtype)]

    st = dtype.find("-")
    dtype = dtype[st+1:len(dtype)]

    st = dtype.find("-")
    dtype = dtype[st+1:len(dtype)]

    return dtype



def sync(src_file,csx):
   try:

     con = None
     cs = csx
     con = psycopg2.connect(cs)
     con.set_session(autocommit=True)
     cur = con.cursor()

     print "      attempting to synchronize: " + src_file
     cur.execute(open(src_file,"r").read())
     print "      success"

   except psycopg2.DatabaseError, e:
       print "      error: %s" % e


   finally:
       if con:
           con.close()

def main(argv):

   start = timer()
   start_dt = strftime("%Y-%m-%d %H:%M:%S", localtime())

   configFile = ""
   try:
     opts, args = getopt.getopt(argv,"hc:",["cfile="])

   except getopt.GetoptError:
     print 'dbdelta.py -c <config file>'
     sys.exit(2)

   for opt,arg in opts:
      if opt == '-c':
         configFile = arg

   config = ConfigParser.ConfigParser()
   config.read(configFile)

   db1 = config.get('dbOne','dbname')
   db1host = config.get('dbOne','dbhost')
   db1port = config.get('dbOne','dbport')
   db1user = config.get('dbOne','dbuser')
   db1pass = config.get('dbOne','dbpass')
   cs1 = "dbname=" + db1 + " host=" + db1host + " port=" + db1port + " user=" + db1user + " password=" + db1pass

   db2 = config.get('dbTwo','dbname')
   db2host = config.get('dbTwo','dbhost')
   db2port = config.get('dbTwo','dbport')
   db2user = config.get('dbTwo','dbuser')
   db2pass = config.get('dbTwo','dbpass')
   cs2 = "dbname=" + db2 + " host=" + db2host + " port=" + db2port + " user=" + db2user + " password=" + db2pass

   spath = config.get('filePath','path')
   do_sync = config.get('sync','do_sync')

   if os.path.exists(spath):
      shutil.rmtree(spath)

   print "dbdelta.py on: " + start_dt
   print "======================================================================"
   print " compare: " + cs1
   print "      to: " + cs2
   print " synchronize is set to: " + do_sync
   print "             output to: " + spath
   print "======================================================================"


   print "extracting " + db1 + " from " + db1host
   extract(cs1,db1host,db1,spath)
   print "extract complete for " + db1 + " from: " + db1host

   print "extracting " + db2 + " from " + db2host
   extract(cs2,db2host,db2,spath)
   print "extract complete for " + db2 + " from: " + db2host

   print ""
   print "delta compare"
   print ""
   walk(spath,db1host,db1,db2host,db2,do_sync,cs2)
   
   end_dt = strftime("%Y-%m-%d %H:%M:%S", localtime())

   end = timer()
   m, s = divmod(end - start, 60)
   h, m = divmod(m, 60)
   time_str = "%02d:%02d:%02d" % (h, m, s)

   print "        delta complete at: " + end_dt
   print("execution time (hh:mm:ss): %s" % time_str )



if __name__ == "__main__":
   main(sys.argv[1:])
