#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2
import sys
import os
import shutil
import ConfigParser
import getopt
from timeit import default_timer as timer
from time import localtime, strftime


def dump_ns_ddl(cs,db,countFunc,listFunc,ddlFunc,typeDDL,ddlPath):

   try:
      print "     " + typeDDL
      conddl = None
      conddl = psycopg2.connect(cs)

      countCur = conddl.cursor()
      countCur.execute(countFunc)

      while True:
          exists = countCur.fetchone()
          if exists == None:
             break
          else:
             doesExist = exists[0]

      if doesExist > 0:
          new_path = ddlPath + os.sep + typeDDL

          if not os.path.exists(new_path):
             os.makedirs(new_path)

          os.chdir(new_path)

          listCur = conddl.cursor()
          listCur.execute(listFunc)

          while True:
              listRow = listCur.fetchone()
              if listRow == None:
                 break
              else:
                 objName = listRow[0]
                 ddlCur = conddl.cursor()
                 ddlCur.execute(ddlFunc,(objName,))

                 while True:
                     objRow = ddlCur.fetchone()
                     if objRow == None:
                        break
                     else:
                        objDDL = objRow[0]
                        fName = objName + ".sql"
                        file = open(fName,"w")
                        file.write(objDDL)
                        file.close()

   
   except psycopg2.DatabaseError, e:
       print 'Error %s' % e    
       sys.exit(1)
    
    
   finally:
       if conddl:
           conddl.close()    

def dump_ddl(cs,db,schemaName,countFunc,listFunc,ddlFunc,typeDDL,ddlPath):

   try:
      print "     " + typeDDL 
      conddl = None
      conddl = psycopg2.connect(cs)

      countCur = conddl.cursor()
      countCur.execute(countFunc,(schemaName,))

      while True:
          exists = countCur.fetchone()
          if exists == None:
             break
          else:
             doesExist = exists[0]

      if doesExist > 0:
          new_path = ddlPath + os.sep + typeDDL

          if not os.path.exists(new_path):
             os.makedirs(new_path)

          os.chdir(new_path)

          listCur = conddl.cursor()
          listCur.execute(listFunc,(schemaName,))

          while True:
              listRow = listCur.fetchone()
              if listRow == None:
                 break
              else:
                 objName = listRow[0]
                 ddlCur = conddl.cursor()
                 ddlCur.execute(ddlFunc,(schemaName,objName))

                 while True:
                     objRow = ddlCur.fetchone()
                     if objRow == None:
                        break
                     else:
                        objDDL = objRow[0]
                        fName = objName + ".sql"
                        file = open(fName,"w")
                        file.write(objDDL)
                        file.close()

   
   except psycopg2.DatabaseError, e:
       print 'Error %s' % e    
       sys.exit(1)
    
    
   finally:
       if conddl:
           conddl.close()    

def dump_ns_list(cs,listFunc):
   returnarray=[]
   conddl = psycopg2.connect(cs)
   listCur = conddl.cursor()
   listCur.execute(listFunc)

   while True:
       listRow = listCur.fetchone()
       if listRow == None:
          break
       else:
          returnarray.append(listRow[0])
   return returnarray
           
def extract(cx,dbhost,db,pth,schema):
   try:
      con = None 
      cs = cx

      con = psycopg2.connect(cs) 
      file_path = pth + dbhost + os.sep + db    

      if not os.path.exists(file_path):
         os.makedirs(file_path)

      os.chdir(file_path)
 

      ownerCount = "select count(*) from dbdelta.get_ownership()"
      ownerList = "select * from dbdelta.get_ownership()"
      ownerDDL = "select dbdelta.dump_ownership(%s)"

      grantCount = "select count(*) from dbdelta.get_grantees()"
      grantList = "select * from dbdelta.get_grantees()"
      grantDDL = "select dbdelta.dump_grants(%s)"

      roleCount = "select count(*) from dbdelta.get_role_list()"
      roleList = "select * from dbdelta.get_role_list()"
      roleDDL = "select dbdelta.dump_role(%s)"

      tbsCount = "select count(*) from dbdelta.get_tablespace_list()"
      tbsList = "select * from dbdelta.get_tablespace_list()"
      tbsDDL = "select dbdelta.dump_tablespace(%s)"

      schemaCount = "select count(*) from dbdelta.get_schema_list()"
      schemaList = "select * from dbdelta.get_schema_list()"
      schemaDDL = "select dbdelta.dump_schema(%s)"

      fdwCount = "select count(*) from dbdelta.get_fdw_list(%s)"
      fdwList = "select * from dbdelta.get_fdw_list(%s)"
      fdwDDL = "select dbdelta.dump_fdw(%s,%s)"

      seqCount = "select count(*) from dbdelta.get_sequence_list(%s)"
      seqList = "select * from dbdelta.get_sequence_list(%s)"
      seqDDL = "select dbdelta.dump_sequence(%s,%s)"

      typeCount = "select count(*) from dbdelta.get_type_list(%s)"
      typeList = "select * from dbdelta.get_type_list(%s)"
      typeDDL = "select dbdelta.dump_type(%s,%s)"

      tableCount = "select count(*) from dbdelta.get_table_list(%s)"
      tableList = "select dbdelta.get_table_list(%s)"
      tableDDL = "select dbdelta.dump_table_ddl(%s,%s)"

      consCount = "select count(*) from dbdelta.get_constraint_list(%s)"
      consList = "select dbdelta.get_constraint_list(%s)"
      consDDL = "select dbdelta.dump_constraints(%s,%s)"

      indexCount = "select count(*) from dbdelta.get_index_list(%s)"
      indexList = "select * from dbdelta.get_index_list(%s)"
      indexDDL = "select dbdelta.dump_indexes(%s,%s)"

      viewCount = "select count(*) from dbdelta.get_view_list(%s)"
      viewList = "select dbdelta.get_view_list(%s)"
      viewDDL = "select dbdelta.dump_view(%s,%s)"

      matViewCount = "select count(*) from dbdelta.get_materialized_view_list(%s)"
      matViewList = "select dbdelta.get_materialized_view_list(%s)"
      matViewDDL = "select dbdelta.dump_materialized_view(%s,%s)"

      funcCount = "select count(*) from dbdelta.get_function_list(%s)"
      funcList = "select dbdelta.get_function_list(%s)"
      funcDDL = "select dbdelta.dump_function(%s,%s)"

      cluster_path = file_path + os.sep + "cluster"
 
      if not os.path.exists(cluster_path):
         os.makedirs(cluster_path)

      os.chdir(cluster_path)

      # dump_ns_ddl(cs,db,tbsCount,tbsList,tbsDDL,"a_tablespaces",cluster_path)
      # dump_ns_ddl(cs,db,roleCount,roleList,roleDDL,"b_roles",cluster_path)
      # dump_ns_ddl(cs,db,schemaCount,schemaList,schemaDDL,"c_schemas",cluster_path)
      # dump_ns_ddl(cs,db,ownerCount,ownerList,ownerDDL,"m_ownership",cluster_path)
      # dump_ns_ddl(cs,db,grantCount,grantList,grantDDL,"n_grants",cluster_path)

      os.chdir(file_path)

      schema_path = file_path + os.sep + "schemas"
 
      if not os.path.exists(schema_path):
         os.makedirs(schema_path)

      os.chdir(schema_path)

      schemaName = schema
      new_path = schema_path + os.sep + schemaName
 
      if not os.path.exists(new_path):
          os.makedirs(new_path)

      os.chdir(new_path)

      print "   schema " + schemaName
      print "   ----------------------------------------"

      # dump_ddl(cs,db,schemaName,fdwCount,fdwList,fdwDDL,"k_foreign_data_wrappers",new_path)
      # dump_ddl(cs,db,schemaName,seqCount,seqList,seqDDL,"d_sequences",new_path)
      dump_ddl(cs,db,schemaName,typeCount,typeList,typeDDL,"e_types",new_path)
      dump_ddl(cs,db,schemaName,tableCount,tableList,tableDDL,"f_tables",new_path)
      dump_ddl(cs,db,schemaName,indexCount,indexList,indexDDL,"g_indexes",new_path)
      dump_ddl(cs,db,schemaName,consCount,consList,consDDL,"h_constraints",new_path)
      dump_ddl(cs,db,schemaName,viewCount,viewList,viewDDL,"i_views",new_path)
      dump_ddl(cs,db,schemaName,matViewCount,matViewList,matViewDDL,"j_materialized_views",new_path)
      dump_ddl(cs,db,schemaName,funcCount,funcList,funcDDL,"l_functions",new_path)

   except IOError, e:
       print 'Error %s' % e    
       sys.exit(1)
    
   finally:
       if con:
           con.close()

def compare(delta_path,db1,db2,src,com,file1,file2,cs,schema1,schema2):

   filea = src + os.sep + file1
   fileb = com + os.sep + file2

   delta_type = gettype(delta_path,src,db1)

   match = 0
   differs = 0

   d_path = delta_path + os.sep + "deltas"
 
   if not os.path.exists(d_path):
      os.makedirs(d_path)

   os.chdir(d_path)

   fComp = file1.replace(".sql","")
   fName = d_path + os.sep + fComp + ".out"


   try:
     
     f1 = open(filea)
     f2 = open(fileb)

     f1_line = f1.readline()
     f2_line = f2.readline()

     filea_written = False
     fileb_written = False

     line_no = 1
   
     while f1_line != '' or f2_line != '':
         f1_line = f1_line.strip()
         f2_line = f2_line.strip()

         if (f1_line != f2_line) and (schema1+"." not in f1_line) and (schema2+"." not in f2_line):
           file = open(fName,"w")

           if f2_line == '' and f1_line != '':    
             file.write(filea + " >+ line-" + str(line_no) + ": " + f1_line + "\n")
           elif f1_line != '':
             file.write(filea + " > line-" + str(line_no) + ": " + f1_line + "\n")

           if f1_line == '' and f2_line != '':
             file.write(fileb + " <+ line-" + str(line_no) + ": " + f2_line + "\n")
           elif f2_line != '':
             file.write(fileb + " < line-" + str(line_no) + ": " + f2_line + "\n")

           file.close()

           differs += 1
         else:
           match += 1

         f1_line = f1.readline()
         f2_line = f2.readline()

         line_no += 1

     f1.close()
     f2.close()

     if os.path.isfile(fName):
         file = open(fName,"r")
         file_line = file.readline()

         while file_line != '':
           file_line = file.readline()

           if filea in file_line:
             filea_written = True
           if fileb in file_line:
             fileb_written = True

           file_line = file.readline()

         file.close()

         if not filea_written:
           file = open(fName,"a")
           file.write(filea + "\n")
           file.close()
         if not fileb_written:
           file = open(fName,"a")
           file.write(fileb + "\n")
           file.close()
     
     return [delta_type, match, differs]

   except IOError, e:
      print 'Error %s' % e

      
def walk(delta_path,host1,host2,db1,db2,cs,schema1,schema2):

    src_path = delta_path + os.sep + "schemas" + os.sep + schema1
    com_path = delta_path + os.sep + "schemas" + os.sep + schema2

    for subdir, dirs, files in sorted(os.walk(src_path)):
       for file1 in files:
           
           filepath = subdir + os.sep + file1
           for subdir2, dirs2, filesx in sorted(os.walk(com_path)):
              for file2 in filesx:
                  filepath2 = subdir2 + os.sep + file2
                  if filepath.endswith(".sql"):
                    #s1 = len(schema1)
                    #s2 = len(schema2)
                    f1 = file1
                    f2 = file2
                    if (f1 == f2):
                      result = compare(delta_path,db1,db2,subdir,subdir2,file1,file2,cs,schema1,schema2)
                      if (result[2] > 0):
                        print ("file1 path:" + subdir + os.sep + file1)
                        print ("file2 path:" + subdir2 + os.sep + file2)
                        print ("---------------------------------")
                        print ("matches:     %s" % result[1])
                        print ("differences: %s" % result[2])
                        print ("=================================")

              
              
def gettype(path,src,db):

    dtype = src.replace(path,"")
    dtype = dtype.replace(db,"")
    dtype = dtype.replace(os.sep,"-")
    dtype = dtype[1:len(dtype)]
   
    st = dtype.find("-")
    dtype = dtype[st+1:len(dtype)]

    st = dtype.find("-")
    dtype = dtype[st+1:len(dtype)]

    st = dtype.find("-")
    dtype = dtype[st+1:len(dtype)]

    return dtype



def main(argv):

   start = timer()
   start_dt = strftime("%Y-%m-%d %H:%M:%S", localtime())
 
   configFile = ""
   try:
     opts, args = getopt.getopt(argv,"hc:",["cfile="])

   except getopt.GetoptError:
     print 'dbdelta.py -c <config file>'
     sys.exit(2)

   for opt,arg in opts:
      if opt == '-c':
         configFile = arg

   config = ConfigParser.ConfigParser()
   config.read(configFile)

   db1 = config.get('dbOne','dbname')
   db1host = config.get('dbOne','dbhost')
   db1port = config.get('dbOne','dbport')
   db1user = config.get('dbOne','dbuser')
   db1schema = config.get('dbOne','schema')
   cs1 = "dbname=" + db1 + " host=" + db1host + " port=" + db1port + " user=" + db1user

   db2 = config.get('dbTwo','dbname')
   db2host = config.get('dbTwo','dbhost')
   db2port = config.get('dbTwo','dbport')
   db2user = config.get('dbTwo','dbuser')

   # either 'schema' or 'exclude_schema' can be specified for dbTwo. If both are specified, only 'schema' will be used
   if config.has_option('dbTwo','schema'):
      schemaList = "select * from dbdelta.get_schema_list() where schema_name in (" + config.get('dbTwo','schema') + ")"
   elif config.has_option('dbTwo','exclude_schema'):
      schemaList = "select * from dbdelta.get_schema_list() where schema_name not in ('dbdelta','" + db1schema + "'," + config.get('dbTwo','exclude_schema') + ")"
   else:
      schemaList = "select * from dbdelta.get_schema_list() where schema_name not in ('dbdelta','" + db1schema + "')"

   cs2 = "dbname=" + db2 + " host=" + db2host + " port=" + db2port + " user=" + db2user
   
   db2schemas = dump_ns_list(cs2,schemaList)

   spath = config.get('filePath','path')
   if os.path.exists(spath):
      shutil.rmtree(spath)

   print "schemadelta.py on: " + start_dt
   print "comparing " + db1schema + " to " + str(db2schemas)
   print "extracting from " + db1 + " on " + db1host + " for schema " + db1schema
   extract(cs1,db1host,db1,spath,db1schema)
   print "extract complete from " + db1 + " on " + db1host + " for schema " + db1schema

   for db2schema in db2schemas:
      print "======================================================================"
      print " compare: " + cs1 + " schema=" + db1schema
      print "      to: " + cs2 + " schema=" + db2schema
      print " output to: " + spath
      print "======================================================================"

      print "extracting from " + db2 + " on " + db2host + " for schema " + db2schema
      extract(cs2,db2host,db2,spath,db2schema)
      print "extract complete from " + db2 + " on " + db2host + " for schema " + db2schema
 
      print ""
      print "delta schema compare between " + db1schema + " and " + db2schema
      print ""
   
      dpath=spath + db1host + os.sep + db1
      print "delta path set to: " + dpath
  
      walk(dpath,db1host,db2host,db1,db2,cs2,db1schema,db2schema)

   end_dt = strftime("%Y-%m-%d %H:%M:%S", localtime())

   end = timer()
   m, s = divmod(end - start, 60)
   h, m = divmod(m, 60)
   time_str = "%02d:%02d:%02d" % (h, m, s)

   print "        delta complete at: " + end_dt
   print("execution time (hh:mm:ss): %s" % time_str ) 
   


if __name__ == "__main__":
   main(sys.argv[1:])
