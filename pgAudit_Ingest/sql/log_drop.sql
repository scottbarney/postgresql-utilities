CREATE TABLE audit.log_drop
(
    raw_id serial,
    raw_batch integer,
    raw_loadtime timestamp with time zone,
    raw_data text COLLATE pg_catalog."default",
    processed boolean DEFAULT false
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


-- Index: log_drop_batch_idx

-- DROP INDEX audit.log_drop_batch_idx;

CREATE INDEX log_drop_batch_idx
    ON audit.log_drop USING btree
    (raw_batch)
    TABLESPACE pg_default;

-- Index: log_drop_raw_id_idx

-- DROP INDEX audit.log_drop_raw_id_idx;

CREATE UNIQUE INDEX log_drop_raw_id_idx
    ON audit.log_drop USING btree
    (raw_id)
    TABLESPACE pg_default;
