CREATE OR REPLACE FUNCTION audit.log_loader_end(
	in_log_id integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare      
      log_in alias for $1;
      s_stmt text;
      cdb character varying;
      pdb character varying;        
     
begin
    select current_database()
      into cdb;
    
    select setting 
      into pdb
      from pg_settings 
     where name = 'port';
      
    s_stmt := 'update audit.log_queue ' ||
              '   set run_status = ' || quote_literal('complete') ||
              '     , end_time = ' || quote_literal(clock_timestamp()) ||
              ' where log_id = ' || log_in ||
              ';';

    perform dblink_exec('port=' || pdb || ' dbname=' || cdb,s_stmt);          

return 0;
end;

$BODY$;

