CREATE OR REPLACE FUNCTION audit.get_drop_limit(
	)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      outJobs integer;
begin
    select param_val::integer
      into outJobs
      from audit.configuration
     where param_name = 'drop_loader_max';

    return outJobs;
end;

$BODY$;
