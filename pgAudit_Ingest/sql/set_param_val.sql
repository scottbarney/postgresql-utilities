CREATE OR REPLACE FUNCTION audit.set_param_val(
	param_name_in character varying,
	param_val_in character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      inParam alias for $1;
      inVal alias for $2;
      rec_exists integer;
begin
     select count(*)
       into rec_exists
       from audit.configuration
      where param_name = inParam;
      
      if rec_exists > 0
      then
         update audit.configuration
            set param_val = inVal
          where param_name = inParam;
       else            
          begin
              insert into audit.configuration
                   ( param_name
                   , param_val
                   )
              values
                   ( inParam
                   , inVal
                   );
        exception
              when others
              then
                 raise notice 'insert failed: %', sqlerrm;
                 return 1;
        end;
     end if;
     return 0;
end;

$BODY$;

