CREATE TABLE audit.client_audit
(
    audit_id integer NOT NULL,
    batch_id integer NOT NULL,
    user_name character varying COLLATE pg_catalog."default",
    schema_destination character varying COLLATE pg_catalog."default",
    qualified_schema boolean DEFAULT false,
    processed boolean DEFAULT false,
    CONSTRAINT client_audit_pk PRIMARY KEY (audit_id, batch_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

