CREATE OR REPLACE FUNCTION audit.get_drop_queue(
	)
    RETURNS TABLE(l_id integer) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

declare
begin  
    return query select distinct log_id
                   from audit.log_queue
                  where run_status = 'pending'
                    and run_after <= now()::timestamp with time zone
                  limit audit.get_drop_limit();

end;

$BODY$;

