CREATE OR REPLACE FUNCTION audit.queue_log_to_drop(
	client_in character varying,
	reader_in character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare 
      in_client alias for $1;
	  in_reader alias for $2;
	  new_batch integer;
	  batch_size integer;
	  
      pCur cursor
      for
      select log_id
           , log_file_name
        from audit.log_queue
       where run_status in ( 'pending', 'reprocess' ) 
       order by log_id desc
       limit audit.get_drop_limit();

      pRec record;
      cmd text;     
      loader_enabled character varying;
      cdb character varying;
	  pdb character varying;
begin

    select current_database()
      into cdb;
    
    select setting 
      into pdb
      from pg_settings 
     where name = 'port';

    loader_enabled := audit.get_param_val('drop_loader_enabled');
	
    if loader_enabled = 'Y'
    then
       cmd := 'vacuum audit.log_drop';
	   perform dblink_exec('port=' || pdb || ' dbname=' || cdb,cmd); 
       -- create a batch and update
       open pCur;
       loop
          fetch pCur into pRec;
          exit when not found;

          perform audit.log_loader_start(pRec.log_id);

          select nextval('audit.audit_loader_batch_seq'::regclass)
            into new_batch;
          
	     raise notice 'batch: %', new_batch;
		  
          begin
              cmd := 'copy audit.log_drop(raw_data) from program ' || quote_literal(in_reader || ' ' || in_client || ' ' || pRec.log_file_name );            	     			 
              execute cmd;			  
        		   
          exception
                when others
                then
                   raise notice 'operation failed %', sqlerrm;
          end;

		  delete from audit.log_drop
		   where length(raw_data) = 0;

		  update audit.log_drop
		     set raw_batch = new_batch
		       , raw_loadtime = clock_timestamp()
		   where raw_batch is null;		
   
		  select count(*)
            into batch_size
            from audit.log_drop
		   where raw_batch = new_batch;
       
          perform audit.create_audit_loader_batch(new_batch,batch_size );		  
          perform audit.log_loader_end(pRec.log_id);
		  
       end loop;
       close pCur;
   else
      raise notice 'drop loader disabled';
   end if;
return 0;
end;

$BODY$;

