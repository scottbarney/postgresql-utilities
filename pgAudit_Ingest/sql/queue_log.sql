CREATE OR REPLACE FUNCTION audit.queue_log(
	in_log character varying,
	in_date character varying,
	in_size bigint,
	in_debug boolean DEFAULT false)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
       log_in alias for $1;
       date_in alias for $2;
       size_in alias for $3;
       debug_in alias for $4;

       fCur cursor 
       for
       select log_id
             , log_file_name
             , log_file_date
             , log_file_size
             , run_status
         from audit.log_queue
        where log_file_name = log_in;
        
        fRec record;
        rec_exists integer;
        loader_enabled character varying;
begin
     loader_enabled := audit.get_param_val('log_loader_enabled');
     
     if loader_enabled = 'Y'
     then
        date_in := substring(date_in,1,public.instr(date_in,'.')-1);
     
        select count(*)
          into rec_exists
          from audit.log_queue
         where log_file_name = log_in;
      
        if rec_exists > 0
        then
           open fCur;
           loop
              fetch fCur into fRec;
              exit when not found;
            
              if fRec.log_file_name = log_in and
                 fRec.log_file_size = size_in and
                 fRec.run_status = 'reprocess'
              then
                 update audit.log_queue
                    set run_status = 'pending'
                      , start_time = null
                      , end_time = null
                 where log_id = fRec.log_id;
                 
              elsif fRec.log_file_name = log_in and
                     ( fRec.log_file_date != date_in::timestamp or
                       fRec.log_file_size != size_in )
              then
                 update audit.log_queue
                    set log_file_date = date_in::timestamp
                      , log_file_size = size_in
                      , run_status = 'reprocess'
                      , start_time = null
                      , end_time = null
                 where log_id = fRec.log_id;
              end if;            
          end loop;
          close fCur;
        else
           insert into audit.log_queue
                ( log_file_name
                , log_file_date
                , log_file_size
                , load_date
                , run_status
                , queue_time
                , run_after
                )
          values  
                ( log_in
                , date_in::timestamp
                , size_in
                , now()::date
                , 'pending' 
                , clock_timestamp()
                , date_in::timestamp                
                );
        end if;         
     else
        raise notice 'log loader disabled';
     end if;
      
     return 0;
end;
$BODY$;
