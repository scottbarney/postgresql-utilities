CREATE OR REPLACE FUNCTION audit.parse_load_audit_data(
	record_in character varying,
	batch_id_in integer,
	raw_id integer,
	debug_in boolean DEFAULT false)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
        in_record alias for $1;
        in_batch alias for $2;
        in_raw alias for $3;
        in_debug alias for $4;
       
        pid_id character varying;
        pid_start integer;
        pid_end integer;       
        audit_start integer;
        audit_end integer;
        dte_stmp character varying;        
        audit_record character varying;
        audit_type_name character varying;
        stmt_id character varying;
        sub_stmt_id character varying;
        class_read character varying;
        class_write character varying;
        class_insert character varying;
        class_update character varying;
        class_delete character varying;
        class_truncate character varying;
        class_function character varying;
        class_role character varying;
        class_ddl character varying;
        class_misc character varying;
        class_misc_set character varying;
        class_all character varying;
        statement_exec character varying;
        sql_stmt text;
   	cdb character varying;
        pdb character varying;        
        user_start integer;
        user_name character varying;       
        app_start integer;
        app_name character varying;
        loader_enabled character varying;
begin
    loader_enabled := audit.get_param_val('audit_data_loader_enabled');
    if loader_enabled = 'Y'
    then
       select current_database()
         into cdb;
    
       select setting 
         into pdb
         from pg_settings 
        where name = 'port';
       begin 
           in_record := replace(in_record,'>','');
                  
           user_start := instr(in_record,'user=');  
           user_name := substring(in_record,user_start+5,length(in_record));
           user_name := substring(user_name,1,instr(user_name,',')-1);
         
           app_start := instr(in_record,'app=');         
           app_name := substring(in_record,app_start+4,length(in_record));
           app_name := substring(app_name,1,instr(app_name,',')-1);
                  
           dte_stmp := substring(in_record,2,public.instr(in_record,'[')-2);
     
           if instr(in_record,'SESSION') > 0
           then
              audit_type_name := 'SESSION';
           else
              audit_type_name := 'OBJECT';
           end if;      

           pid_start := instr(in_record,'[');
           pid_end := instr(in_record,']');
           audit_start := instr(in_record,'AUDIT: SESSION');
           audit_end :=instr(in_record,',<not logged>');

           pid_id := substring(in_record,pid_start+1, pid_end-1);
           pid_id := substring(pid_id,1,instr(pid_id,']')-1); 
           
           audit_record := substring(in_record,audit_start+14,length(in_record));
           audit_record := substring(audit_record,2,length(audit_record));
           
           stmt_id := substring(audit_record,1,1);
           audit_record := substring(audit_record,2,length(audit_record));
           
           sub_stmt_id := substring(audit_record,2,1);
           audit_record := substring(audit_record,2,length(audit_record));
         
           if instr(audit_record,'READ') > 0
           then
              class_read := 'read';
           else
              class_read := '';
           end if;
         
           if instr(audit_record,'WRITE') > 0
           then
              class_write := 'write';
           else
              class_write := '';
           end if;

           if instr(audit_record,'FUNCTION') > 0
           then
              class_function := 'function';
           else
              class_function := '';
           end if;
         
           if instr(audit_record,'ROLE') > 0
           then
              class_role := 'role';
           else
              class_role := '';
           end if;

           if instr(audit_record,'DDL') > 0
           then
              class_ddl := 'ddl';
           else
              class_ddl := '';
           end if;
         
           if instr(audit_record,'MISC') > 0
           then          
              class_misc := 'misc';
           else
              class_misc := '';
           end if;
         
           if instr(audit_record,'MISC_SET') > 0
           then
              class_misc_set := 'misc_set';
           else
              class_misc_set := '';
           end if;
         
           if instr(audit_record,'ALL') > 0
           then
              class_all := 'all';
           else 
              class_all := '';
           end if;

           if instr(audit_record,'INSERT') > 0
           then
              class_insert := 'insert';
           else
              class_insert := '';
           end if;
         
           if instr(audit_record,'UPDATE') > 0
           then
              class_update := 'update';
           else
              class_update := '';
           end if;
         
           if instr(audit_record,'DELETE') > 0
           then
              class_delete := 'delete';
           else 
              class_delete := '';
           end if;
         
           if instr(audit_record,'TRUNCATE',1) > 0
           then
              class_truncate := 'truncate table';
           else
              class_truncate := '';
           end if;
           
           begin
               statement_exec := substr(audit_record,instr(audit_record,'"'),length(audit_record));
               statement_exec := replace(statement_exec,',<not logged>','');
               statement_exec := replace(statement_exec,'"','');
           exception
                 when others
                 then
                    raise notice 'error parsing: %, %', statement_exec, sqlerrm;
           end;
       exception
             when others
             then
                raise notice 'error at batch: % -> %',in_batch, sqlerrm;
       end;
    
          
       if in_debug
       then
          raise notice 'audit type: %', audit_type_name;
          raise notice 'date: %', dte_stmp;
          raise notice 'pid start: %', pid_start;
          raise notice 'pid end: %', pid_end;
          raise notice 'pid: %', pid_id;
          raise notice 'audit_record: %', audit_record;
          raise notice 'stmt_id: %', stmt_id;
          raise notice 'sub_stmt_id: %', sub_stmt_id;
          raise notice 'statement: %', statement_exec;
          raise notice 'class_read: %', class_read;
          raise notice 'class_write %', class_write;
          raise notice 'class_function: %', class_function;
          raise notice 'class_role: %', class_role;
          raise notice 'class_ddl: %', class_ddl;
          raise notice 'class_misc: %', class_misc;
          raise notice 'class_misc_set: %', class_misc_set;
          raise notice 'class_all: %', class_all;
          raise notice 'class_insert: %', class_insert;
          raise notice 'class_update: %', class_update;
          raise notice 'class_delete: %', class_delete;
          raise notice 'class_truncate: %', class_truncate;
       end if;
	  
       begin
           sql_stmt :=
          'insert into audit.activity_audit
            ( user_name
            , app_name 
            , audit_type
            , audit_timestamp
            , process_id
            , statement_id
            , sub_statement_id
            , sql_statement
            , operation_insert
            , operation_delete
            , operation_update
            , operation_truncate
            , operation_read
            , operation_write
            , operation_function
            , operation_role
            , operation_ddl
            , operation_misc
            , operation_misc_set
            , operation_all
            , batch_id
            )
          values 
            ('  || quote_literal(user_name) ||
            ',' || quote_literal(app_name) ||
            ',' || quote_literal(audit_type_name) ||
            ',' || quote_literal(dte_stmp) ||
            ',' || quote_literal(pid_id) ||
            ',' || quote_literal(stmt_id) ||
            ',' || quote_literal(sub_stmt_id) ||
            ',' || quote_literal(statement_exec) ||
            ',' || quote_literal(class_insert) ||
            ',' || quote_literal(class_delete) ||
            ',' || quote_literal(class_update) ||
            ',' || quote_literal(class_truncate) ||
            ',' || quote_literal(class_read) ||
            ',' || quote_literal(class_write) ||
            ',' || quote_literal(class_function) ||
            ',' || quote_literal(class_role) ||
            ',' || quote_literal(class_ddl) ||
            ',' || quote_literal(class_misc) ||
            ',' || quote_literal(class_misc_set) ||
            ',' || quote_literal(class_all) || 
            ',' || in_batch ||
            ');';
           
          
          perform dblink_exec('port=' || pdb || ' dbname=' || cdb,sql_stmt); 
          
          sql_stmt := 'update audit.log_drop
		          set processed = true
		        where raw_id = ' || in_raw;
						   
	  perform dblink_exec('port=' || pdb || ' dbname=' || cdb,sql_stmt);	

       exception
             when others
             then
                raise notice 'error loading data % - %', sql_stmt, sqlerrm;
        end;
    else
       raise notice 'audit data loader disabled';
    end if;
    return 0;
end;
$BODY$;

