CREATE TABLE audit.client_loader_queue
(
    batch_id integer,
    batch_size bigint,
    load_date date,
    run_status character varying COLLATE pg_catalog."default",
    queue_time timestamp with time zone,
    run_after timestamp with time zone,
    start_time timestamp with time zone,
    end_time timestamp with time zone
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

