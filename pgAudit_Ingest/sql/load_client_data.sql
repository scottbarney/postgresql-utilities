CREATE OR REPLACE FUNCTION audit.load_client_data(
	batch_in integer,
	debug_in boolean DEFAULT false)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
       in_batch alias for $1;
       in_debug alias for $2;
       
       cCur cursor 
       for
       select audit_id
            , user_name
            , app_name
            , audit_type
            , audit_timestamp
            , process_id
            , statement_id
            , sub_statement_id
            , sql_statement
            , operation_insert
            , operation_delete
            , operation_update
            , operation_truncate
            , operation_read
            , operation_write
            , operation_function
            , operation_role
            , operation_ddl
            , operation_misc
            , operation_misc_set
            , operation_all      
            , batch_id
         from audit.activity_audit
        where batch_id = in_batch
        order by audit_id;
        
       cRec record;
       cdb character varying;
       pdb character varying;        
       s_stmt text;
       batch_size bigint;
       batch_id integer;
       loader_enabled character varying;
       schema_dest character varying;
       user_name character varying;
begin
    loader_enabled := audit.get_param_val('client_loader_enabled');
    if loader_enabled = 'Y'
    then
       select current_database()
         into cdb;
    
       select setting 
         into pdb
         from pg_settings 
        where name = 'port';      
    
       
       perform audit.client_loader_start(in_batch);
       
       open cCur;
       loop
          fetch cCur into cRec;
          exit when not found;
          begin
               begin
                   schema_dest := replace(cRec.app_name,'[','');
                   schema_dest := replace(schema_dest,']','');
                   -- parse user
                   user_name := substring(schema_dest,instr(schema_dest,'_')+1, length(schema_dest));
                   -- parse schema
                  schema_dest := substring(schema_dest,1,instr(schema_dest,'_')-1);
               exception
                     when others
                     then
                        raise notice '%', sqlerrm;
               end;
               if in_debug
               then
                  raise notice 'schema: %', schema_dest;
                  raise notice 'user: %', user_name;
               end if;            
               -- insert client_audit data to track progress
               s_stmt := 'insert into audit.client_audit
                               ( audit_id 
                               , batch_id
                               , user_name
                               , schema_destination
                               , qualified_schema
                               )
                         values( ' || cRec.audit_id || 
                               ',' || cRec.batch_id ||
                               ',' || quote_literal(user_name) ||
                               ',' || quote_literal(schema_dest) ||
                               ',' || audit.is_schema_audited(schema_dest) ||
                               ');';     
                               
               perform dblink_exec('port=' || pdb || ' dbname=' || cdb,s_stmt);             
               
               if audit.is_schema_audited(schema_dest)
               then
                   -- load data per schema
                   s_stmt := 'insert into ' || schema_dest || '.activity_audit
                               ( audit_id
                               , user_name
                               , app_name
                               , audit_type
                               , audit_timestamp
                               , process_id
                               , statement_id
                               , sub_statement_id
                               , sql_statement
                               , operation_insert
                               , operation_delete
                               , operation_update
                               , operation_truncate
                               , operation_read
                               , operation_write
                               , operation_function
                               , operation_role
                               , operation_ddl
                               , operation_misc
                               , operation_misc_set
                               , operation_all
                               , batch_id )
                        values ( ' || cRec.audit_id ||
                              ', ' || quote_literal(user_name) ||
                              ', ' || quote_literal(cRec.app_name) ||
                              ', ' || quote_literal(cRec.audit_type) ||
                              ', ' || quote_literal(cRec.audit_timestamp) ||
                              ', ' || quote_literal(cRec.process_id) ||
                              ', ' || quote_literal(cRec.statement_id) ||
                              ', ' || quote_literal(cRec.sub_statement_id) ||
                              ', ' || quote_literal(cRec.sql_statement) ||
                              ', ' || quote_literal(cRec.operation_insert) ||
                              ', ' || quote_literal(cRec.operation_delete) ||
                              ', ' || quote_literal(cRec.operation_update) ||
                              ', ' || quote_literal(cRec.operation_truncate) ||
                              ', ' || quote_literal(cRec.operation_read) ||
                              ', ' || quote_literal(cRec.operation_write) ||
                              ', ' || quote_literal(cRec.operation_function) ||
                              ', ' || quote_literal(cRec.operation_role) ||
                              ', ' || quote_literal(cRec.operation_ddl) ||
                              ', ' || quote_literal(cRec.operation_misc) ||
                              ', ' || quote_literal(cRec.operation_misc_set) ||
                              ', ' || quote_literal(cRec.operation_all) ||
                              ', ' || cRec.batch_id ||
                              ');';
               
                  perform dblink_exec('port=' || pdb || ' dbname=' || cdb,s_stmt); 
               end if;        
               -- set client_audit to processed
               s_stmt := 'update audit.client_audit
                             set processed = true 
                           where audit_id=' || cRec.audit_id ||
                            'and batch_id=' || cRec.batch_id;
                            
               perform dblink_exec('port=' || pdb || ' dbname=' || cdb,s_stmt);             
                            
                            
          exception
                when others
                then
                   perform audit.client_loader_fail(in_batch);
                   raise notice 'client loader failed %',sqlerrm;
                   return 1;
           end;        
       end loop;
       close cCur;
    else
       raise notice 'client loader disabled';
    end if;      
    -- signal end of load
    perform audit.client_loader_end(in_batch);
     
    return 0;
end;
$BODY$;

