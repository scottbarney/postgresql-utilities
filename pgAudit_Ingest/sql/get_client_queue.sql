CREATE OR REPLACE FUNCTION audit.get_client_queue(
	)
    RETURNS TABLE(b_id integer) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

declare
begin  
    return query select distinct batch_id
                   from audit.client_loader_queue
                  where run_status = 'pending'
                    and run_after <= now()::timestamp with time zone
                  limit audit.get_client_limit();

end;

$BODY$;

