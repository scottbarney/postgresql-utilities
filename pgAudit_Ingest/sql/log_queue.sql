CREATE TABLE audit.log_queue
(
    log_id serial,
    log_file_name character varying COLLATE pg_catalog."default" NOT NULL,
    log_file_date timestamp with time zone NOT NULL,
    log_file_size bigint NOT NULL,
    load_date date NOT NULL,
    run_status character varying COLLATE pg_catalog."default",
    queue_time timestamp with time zone,
    run_after timestamp with time zone,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    CONSTRAINT log_queue_pk PRIMARY KEY (log_id, log_file_name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

