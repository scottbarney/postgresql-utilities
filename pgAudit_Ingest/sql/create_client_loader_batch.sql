CREATE OR REPLACE FUNCTION audit.create_client_loader_batch(
	batch_in integer,
	batch_size_in bigint)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
      in_batch alias for $1;
      in_batch_size alias for $2;
      cdb character varying;
      pdb character varying;        
      s_stmt text;
begin
     select current_database()
        into cdb;
    
     select setting 
        into pdb
        from pg_settings 
       where name = 'port';

     s_stmt := 'insert into audit.client_loader_queue
                     ( batch_id
                     , batch_size
                     , load_date
                     , run_status
                     , queue_time
                     , run_after
                     )
                values
                     ( '  || in_batch ||
                     ', ' || in_batch_size ||
                     ', ' || quote_literal(now()) ||
                     ', ' || quote_literal('pending') ||
                     ', ' || quote_literal(clock_timestamp()) ||
                     ', ' || quote_literal(clock_timestamp()) ||
                     ');';
                     
     perform dblink_exec('port=' || pdb || ' dbname=' || cdb,s_stmt);
     return 0;
end;
$BODY$;

