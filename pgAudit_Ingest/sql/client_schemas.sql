CREATE TABLE audit.client_schemas
(
    audit_schema_name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT client_schemas_pk PRIMARY KEY (audit_schema_name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

