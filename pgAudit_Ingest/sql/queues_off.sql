CREATE OR REPLACE FUNCTION audit.queues_off(
	)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
       qCur cursor
       for
       select param_name
         from audit.configuration
        where param_name like '%enabled';
         
      qRec record;
begin
     open qCur;
     loop
        fetch qCur into qRec;
        exit when not found;
        
        update audit.configuration
           set param_val = 'N'
         where param_name = qRec.param_name;
      end loop;
      close qCur;
      
      return 0;
end;
$BODY$;
