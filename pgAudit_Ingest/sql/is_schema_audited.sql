CREATE OR REPLACE FUNCTION audit.is_schema_audited(
	s_in character varying)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
       in_s alias for $1;
       n_count integer;
begin
     select count(*)
       into n_count
       from audit.client_schemas
      where audit_schema_name = in_s;
      
     if n_count > 0
     then
        return true;
     else
        return false;
     end if;
end;
$BODY$;

