CREATE OR REPLACE FUNCTION audit.get_param_val(
	param_name_in character varying)
    RETURNS character varying
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      inParam alias for $1;
      outVal character varying;
begin
    select param_val
      into outVal
      from audit.configuration
     where param_name = inParam;

    return outVal;
end;

$BODY$;

