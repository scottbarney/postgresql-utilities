CREATE OR REPLACE FUNCTION audit.load_audit_data(
	batch_in integer,
	debug_in boolean DEFAULT false)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
       in_batch alias for $1;
       in_debug alias for $2;
       
       dCur cursor (batch_size bigint)
       for
       select raw_id
	        , raw_data
         from audit.log_drop
        where raw_data is not null
		  and raw_batch = in_batch
		  and processed = false;
	
        
       dRec record;
       cdb character varying;
       pdb character varying;        
       s_stmt text;
       batch_size bigint;
       batch_id integer;
       loader_enabled character varying;

begin
    loader_enabled := audit.get_param_val('audit_data_loader_enabled');
    if loader_enabled = 'Y'
    then
       select current_database()
         into cdb;
    
       select setting 
         into pdb
         from pg_settings 
        where name = 'port';
       
       -- cleanup any useless records
       s_stmt := 
              'delete from audit.log_drop
                where raw_data is null;';
                
       perform dblink_exec('port=' || pdb || ' dbname=' || cdb,s_stmt);      
      
       s_stmt := 
              'delete from audit.log_drop
                where length(trim(raw_data)) = 0;';
                
       perform dblink_exec('port=' || pdb || ' dbname=' || cdb,s_stmt);      

       perform audit.audit_loader_start(in_batch);
       
	   select count(*)
	     into batch_size
		 from audit.log_drop
		where raw_batch = in_batch;
	   
       open dCur(batch_size);
       loop
          fetch dCur into dRec;
          exit when not found;
          begin
               if length(trim(dRec.raw_data)) > 0
               then
                  perform audit.parse_load_audit_data(dRec.raw_data,in_batch,dRec.raw_id,in_debug);
			   end if;
               
          exception
                when others
                then
                   perform audit.audit_loader_fail(in_batch);
                   raise notice 'audit loader failed %',sqlerrm;
                   return 1;
           end;        
       end loop;
       close dCur;
    else
       raise notice 'audit data loader disabled';
    end if;
    
    perform audit.audit_loader_end(in_batch);
    perform audit.create_client_loader_batch(in_batch,batch_size);
    
    return 0;
end;
$BODY$;

