CREATE OR REPLACE FUNCTION audit.get_audit_queue(
	)
    RETURNS TABLE(b_id integer) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$

declare
begin  
    return query select distinct batch_id
                   from audit.audit_loader_queue
                  where run_status in ('pending','reprocess')
                    and run_after <= now()::timestamp with time zone
                  limit audit.get_audit_limit();

end;

$BODY$;

