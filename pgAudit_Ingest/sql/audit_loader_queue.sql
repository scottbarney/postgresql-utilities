CREATE TABLE audit.audit_loader_queue
(
    batch_id integer NOT NULL,
    batch_size bigint NOT NULL,
    load_date date NOT NULL,
    run_status character varying COLLATE pg_catalog."default",
    queue_time timestamp with time zone,
    run_after timestamp with time zone,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    CONSTRAINT audit_loader_queue_pk PRIMARY KEY (batch_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

