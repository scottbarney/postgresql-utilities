CREATE TABLE audit.activity_audit
(
    audit_id serial,
    user_name character varying COLLATE pg_catalog."default",
    app_name character varying COLLATE pg_catalog."default",
    audit_type character varying COLLATE pg_catalog."default",
    audit_timestamp timestamp with time zone,
    process_id character varying COLLATE pg_catalog."default",
    statement_id character varying COLLATE pg_catalog."default",
    sub_statement_id character varying COLLATE pg_catalog."default",
    sql_statement character varying COLLATE pg_catalog."default",
    operation_insert character varying COLLATE pg_catalog."default",
    operation_delete character varying COLLATE pg_catalog."default",
    operation_update character varying COLLATE pg_catalog."default",
    operation_truncate character varying COLLATE pg_catalog."default",
    operation_read character varying COLLATE pg_catalog."default",
    operation_write character varying COLLATE pg_catalog."default",
    operation_function character varying COLLATE pg_catalog."default",
    operation_role character varying COLLATE pg_catalog."default",
    operation_ddl character varying COLLATE pg_catalog."default",
    operation_misc character varying COLLATE pg_catalog."default",
    operation_misc_set character varying COLLATE pg_catalog."default",
    operation_all character varying COLLATE pg_catalog."default",
    processed boolean DEFAULT false,
    batch_id integer,
    CONSTRAINT activity_audit_pk PRIMARY KEY (audit_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- Index: activity_audit_app_n1

-- DROP INDEX audit.activity_audit_app_n1;

CREATE INDEX activity_audit_app_n1
    ON audit.activity_audit USING btree
    (app_name COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Index: activity_audit_user_n1

-- DROP INDEX audit.activity_audit_user_n1;

CREATE INDEX activity_audit_user_n1
    ON audit.activity_audit USING btree
    (user_name COLLATE pg_catalog."default")
    TABLESPACE pg_default;
