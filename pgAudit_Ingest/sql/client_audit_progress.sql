create or replace view audit.client_audit_progress
as
SELECT t1.batch_id,
    t1.batch_size,
    t1.run_status,
    t2.num_loaded,
    round(t2.num_loaded::numeric / t1.batch_size::numeric * 100::numeric, 2) AS pct_loaded,
    t1.start_time,
    t1.end_time,
    t1.end_time::timestamp(0) without time zone - t1.start_time::timestamp(0) without time zone AS elapsed
   FROM audit.client_loader_queue t1
     LEFT JOIN ( SELECT client_audit.batch_id,
            count(*) AS num_loaded
           FROM audit.client_audit
          GROUP BY client_audit.batch_id) t2 ON t2.batch_id = t1.batch_id
  ORDER BY t1.batch_id DESC;
