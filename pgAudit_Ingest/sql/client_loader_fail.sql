CREATE OR REPLACE FUNCTION audit.client_loader_fail(
	in_batch_id integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare      
      batch_in alias for $1;
      s_stmt text;
      cdb character varying;
      pdb character varying;        
     
begin
    select current_database()
      into cdb;
    
    select setting 
      into pdb
      from pg_settings 
     where name = 'port';
      
    s_stmt := 'update audit.client_loader_queue ' ||
              '   set run_status = ' || quote_literal('failed') ||
              '     , start_time = ' || quote_literal(clock_timestamp()) ||
              ' where batch_id = ' || batch_in ||
              ';';

    perform dblink_exec('port=' || pdb || ' dbname=' || cdb,s_stmt);          

return 0;
end;

$BODY$;

