CREATE TABLE audit.configuration
(
    param_name character varying COLLATE pg_catalog."default" NOT NULL,
    param_val character varying COLLATE pg_catalog."default",
    CONSTRAINT configuration_pk PRIMARY KEY (param_name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

