#!/bin/bash

#################################################
# Script to toggle all queues on
# USAGE:  ./queue_on.sh <CLIENT> <HOST> <PORT_NUMBER> <DATABASE> <DEBUG>

#################################################
# Help section
if [ -z $1 ]
then
   echo "Usage: ./queue_on.sh <CLIENT> <HOST> <PORT_NUMBER> <DATABASE> <DEBUG>"
   echo "   --Script that starts up queues for postgres database log processing"
   exit 0
fi


# Set environment
CLIENT="$1"
HOST="$2"
PORT_NUMBER="$3"
DB="$4"
DEBUG="$5"

export ENV_HOME=$HOME/${CLIENT}/config
export LOG_HOME=$HOME/${CLIENT}/log
export SQL_HOME=$HOME/${CLIENT}/sql
export LOCK_HOME=$HOME/${CLIENT}/lock
export LOCKFILE=$LOCK_HOME/queue_on.lock

if [ ! -f $LOCKFILE ]; then
   touch ${LOCKFILE}

   if [ -z "$Hostname" ]
   then

        if [ "$Hostname" = "" ]
        then
           #Host name is not defined
           Hostname=`hostname -s`
        fi
   fi

   . $ENV_HOME/$PORT_NUMBER.env

   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -c "select audit.queues_on()"    



# Cleanup
   rm -Rf ${LOCKFILE}
else
   echo "queue_on.sh already running"
fi

exit 0

#######################################
# End of script
#######################################

