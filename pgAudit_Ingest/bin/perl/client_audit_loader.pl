#!/usr/bin/perl
#
# @author  
# @version $Id: client_audit_loader.pl,v 1.0.0.1 2019/08/25 08:43:21 sbarney Exp $
#

# Usage: client_audit_loader.pl
#use strict;
use warnings;
use Carp;
use DBI;
use English qw( -no_match_vars );
use File::Basename;
use FindBin;
use Readonly;
use Sys::Hostname;

use lib "$FindBin::Bin";

Readonly::Scalar our $VERSION => '1.0';

$DBNAME=$ARGV[0];
$DBHOST=$ARGV[1];
$DBPORT=$ARGV[2];
$DBUSER=$ARGV[3];
$DBPASS=$ARGV[4];

sub db_connect {
    #my ($DBNAME, $DBHOST, $DBPORT, $DBUSER, $DBPASS) = $ARGV;
    Readonly::Scalar my $MAX_PACKET => 1_000_000;
    Readonly::Scalar my $PGAPPNAME => 'client_audit_loader';

    my $dbh = DBI->connect( qq{dbi:Pg:database=$DBNAME;host=$DBHOST;port=$DBPORT;application_name=$PGAPPNAME}, $DBUSER, $DBPASS, { RaiseError => 1, PrintError => 0, ShowErrorStatement => 1, AutoCommit => 0, LongReadLen => $MAX_PACKET } )
            or croak 'DBI->connect failed!';
    return $dbh;
}

sub max_job_count {
    my( $dbh ) = @_;
    my $sql = q{SELECT param_val AS val FROM audit.configuration where param_name = 'client_loader_max'};
    my $sth = $dbh->prepare( $sql )
            or croak $dbh->errstr;
    my $rc = $sth->execute()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    my $val = undef;
    while ( my $hash_ref = $sth->fetchrow_hashref()) {
        $val = $hash_ref->{'val'};
    }
    $rc = $dbh->commit()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    return $val;
}

sub queue_enabled {
    my( $dbh ) = @_;
    my $sql = q{SELECT param_val as val from audit.configuration where param_name = 'client_loader_enabled'};
    my $sth = $dbh->prepare( $sql )
            or croak $dbh->errstr;
    my $rc = $sth->execute()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    my $val = undef;
    while ( my $hash_ref = $sth->fetchrow_hashref()) {
        $val = $hash_ref->{'val'};
    }
    $rc = $dbh->commit()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    return $val;
}

sub active_jobs {
    my( $dbh ) = @_;
    my $sql = q{SELECT COUNT(DISTINCT batch_id ) AS val FROM audit.client_loader_queue WHERE run_status = 'executing' };
    my $sth = $dbh->prepare( $sql )
            or croak $dbh->errstr;
    my $rc = $sth->execute()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    my $val = undef;
    while ( my $hash_ref = $sth->fetchrow_hashref()) {
        $val = $hash_ref->{'val'};
    }
    $rc = $dbh->commit()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    return $val;
}

sub get_jobs {
    my( $dbh, $arr_ref ) = @_;
    my $sql = q{SELECT distinct b_id from audit.get_client_queue()};
    my $sth = $dbh->prepare( $sql )
            or croak $dbh->errstr;
    my $rc = $sth->execute()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    while ( my $hash_ref = $sth->fetchrow_hashref()) {
        my %hash = %{$hash_ref};
        push @{$arr_ref}, \%hash;
    }
    $rc = $dbh->commit()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    return;
}

sub db_disconnect {
    my( $dbh ) = @_;
    my $rc = $dbh->disconnect()
            or carp $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    return;
}

sub pid_and_stamp {
    return sprintf q{[%d @ %s] }, $PID, scalar localtime;
}

sub execute_job {
    my( $sql ) = @_;
    my $dbh = db_connect();
   
    my $sth = $dbh->prepare( $sql )
            or croak $dbh->errstr;
    my $rc = $sth->execute()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    $sth->finish();
    $rc = $dbh->commit()
            or croak $dbh->errstr;
    carp 'zero return code' if( ! $rc );
    db_disconnect( $dbh );
    return;
}

sub process_jobs {
    my( $queue_enabled, $max_job_count, $active_jobs, $arr_ref ) = @_;
    $SIG{CHLD} = 'IGNORE';
    # for testing...
    Readonly::Scalar my $TESTING => 0;
    my $enqueued = $#{$arr_ref} + 1;
    print pid_and_stamp(), "PARENT STATUS: queue_enabled($queue_enabled) max_job_count($max_job_count) active_jobs($active_jobs) enqueued($enqueued) testing($TESTING)\n";
    foreach my $hash_ref ( @{$arr_ref} ) {
        if(( $queue_enabled ne 'Y' ) || ( $active_jobs >= $max_job_count )) {
            last;
        }

        my $sql = sprintf q{SELECT audit.load_client_data('%s')}, map { $hash_ref->{$_} } qw( b_id );
       #print $sql
        my $child_pid = fork;
        if( ! defined $child_pid ) {
            carp 'ERROR: fork failed!';
            last;
        }
        if( $child_pid == 0 ) {
            if( $TESTING ) {
                print pid_and_stamp(), "CHILD NOT STARTING: $sql\n";
            }
            else {
                print pid_and_stamp(), "CHILD STARTING: $sql\n";
                execute_job( $sql );
            }
            print pid_and_stamp(), "CHILD ENDING\n";
            
            exit;
        }
        $active_jobs++;
        sleep 2;
    }
    return;
}

sub main {
    local $SIG{'__DIE__'} = sub { confess 'Caught DIE signal' . join "\n", q{}, @_; };
        
    select STDERR;
    $OUTPUT_AUTOFLUSH = 1;
    select STDOUT;
    $OUTPUT_AUTOFLUSH = 1;
    my $pgm = basename( $PROGRAM_NAME );
    print pid_and_stamp(), "PARENT STARTING: $pgm\n";
    my $dbh = db_connect();
    my $max_job_count = max_job_count( $dbh );
    my $queue_enabled = queue_enabled( $dbh );
    my $active_jobs = active_jobs( $dbh );
    my @jobs;
    get_jobs( $dbh, \@jobs );
    db_disconnect( $dbh );
    process_jobs( $queue_enabled, $max_job_count, $active_jobs, \@jobs );
    print pid_and_stamp(), "PARENT ENDING\n";
    exit;
}


main();

1;

__END__
