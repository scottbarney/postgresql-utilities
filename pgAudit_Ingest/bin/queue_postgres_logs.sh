#!/bin/bash

#################################################
# Script to queue_postgres_logs
# USAGE:  ./queue_postgres_logs.sh <CLIENT> <HOST> <DATA_DIRECTORY> <PORT_NUMBER> <DATABASE> <DEBUG>

#################################################
# Help section
if [ -z $1 ]
then
   echo "Usage: ./queue_postgres_logs.sh <CLIENT> <HOST> <DATA_DIRECTORY> <PORT_NUMBER> <DATABASE> <DEBUG>"
   echo "   --Script that will queue postgres database logs for processing"
   exit 0
fi


# Set environment
CLIENT="$1"
HOST="$2"
DATA_DIRECTORY="$3"
PORT_NUMBER="$4"
DB="$5"
DEBUG="$6"

#filename = 'postgresql-%Y%m%d.log'

dte=$(date +"%Y%m%d")
echo $dte
yte=$(date +"%Y%m%d" -d yesterday)


export ENV_HOME=$HOME/${CLIENT}/config
export LOG_HOME=$HOME/${CLIENT}/log
export SQL_HOME=$HOME/${CLIENT}/sql
export LOCK_HOME=$HOME/${CLIENT}/lock
export LOCKFILE=$LOCK_HOME/queue_logs.lock

if [ ! -f $LOCKFILE ]; then
   touch ${LOCKFILE}

   if [ -z "$Hostname" ]
   then

        if [ "$Hostname" = "" ]
        then
           #Host name is not defined
           Hostname=`hostname -s`
        fi
   fi

   . $ENV_HOME/$PORT_NUMBER.env

   for f in $DATA_DIRECTORY/log/post*
   do
     echo "Processing $f file..."
     modDate=$(stat -c %y "$f")
     fileSize=$(stat -c%s "$f")
     #echo $f
     #echo $modDate
     #echo $fileSize
     psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -c "select audit.queue_log('${f}'::character varying,'${modDate}'::character varying,${fileSize},false)"    
   done



# Cleanup
   rm -Rf ${LOCKFILE}
else
   echo "queue_postgres_logs.sh already running"
fi

exit 0

#######################################
# End of script
#######################################

