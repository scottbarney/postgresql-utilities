#!/bin/bash

#################################################
# Script to toggle all queues off
# USAGE:  ./deploy.sh <CLIENT> <HOST> <PORT_NUMBER> <DATABASE> <DEBUG>

#################################################
# Help section
if [ -z $1 ]
then
   echo "Usage: ./deploy.sh <CLIENT> <HOST> <PORT_NUMBER> <DATABASE> <DEBUG>"
   echo "   --Script that deploy's audit schema and associated objects"
   exit 0
fi


# Set environment
CLIENT="$1"
HOST="$2"
PORT_NUMBER="$3"
DB="$4"
DEBUG="$5"

export BIN_HOME=$HOME/${CLIENT}/bin
export ENV_HOME=$HOME/${CLIENT}/config
export LOG_HOME=$HOME/${CLIENT}/log
export SQL_HOME=$HOME/${CLIENT}/sql
export LOCK_HOME=$HOME/${CLIENT}/lock
export LOCKFILE=$LOCK_HOME/deploy.lock

if [ ! -f $LOCKFILE ]; then
   touch ${LOCKFILE}

   if [ -z "$Hostname" ]
   then

        if [ "$Hostname" = "" ]
        then
           #Host name is not defined
           Hostname=`hostname -s`
        fi
   fi

   . $ENV_HOME/$PORT_NUMBER.env

# deployment - make sure dblink exists
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -c "create extension dblink;"

# deployment - schema
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -c "create schema audit;"

# deployment - tables
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/activity_audit.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/audit_loader_queue.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/client_audit.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/client_loader_queue.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/configuration.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/log_drop.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/log_queue.sql

# deployment - views
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/audit_loader_progress.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/client_loader_progress.sql

# deployment - functions
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/audit_loader_end.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/audit_loader_fail.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/audit_loader_start.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/client_loader_end.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/client_loader_fail.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/client_loader_start.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/create_audit_loader_batch.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/create_client_loader_batch.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/get_audit_limit.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/get_audit_queue.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/get_client_limit.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/get_client_queue.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/get_drop_batch_size.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/get_drop_limit.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/get_drop_queue.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/get_param_val.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/load_audit_data.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/load_client_data.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/log_loader_end.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/log_loader_fail.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/log_loader_start.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/parse_load_audit_data.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/queue_log.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/queue_log_to_drop.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/queues_off.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/queues_on.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/set_param_val.sql
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/instr.sql

# deployment - meta data
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/add_configuration_data.sql

# deployment - sequences
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -f $SQL_HOME/audit_loader_batch_sequence.sql

# Cleanup
   rm -Rf ${LOCKFILE}
else
   echo "deploy.sh already running"
fi

exit 0

#######################################
# End of script
#######################################

