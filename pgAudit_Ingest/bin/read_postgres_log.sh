#!/bin/bash

#################################################
# Script to read_postgres_log 
# USAGE:  ./read_postgres_log.sh <CLIENT> <FILENAME> <DATA DIRECTORY>
#################################################
# Help section
if [ -z $1 ]
then
   echo "Usage: ./read_postgres_log.sh <CLIENT> <FILENAME> <DATA DIRECTORY>"
   echo "   --Script that will check for messages in the database audit log"
   exit 0
fi

CLIENT="$1"
DATA_DIR="$3"

export ENV_HOME=$HOME/${CLIENT}/config
export LOG_HOME=$HOME/${CLIENT}/log
export SQL_HOME=$HOME/${CLIENT}/sql
export LOCK_HOME=$HOME/${CLIENT}/lock

LOCK_NAME=$(basename $2 .log)
export LOCKFILE=$LOCK_HOME/$LOCK_NAME.lock

if [ ! -f $LOCKFILE ]; then
   touch ${LOCKFILE}

   if [ -z "$Hostname" ]
   then

        if [ "$Hostname" = "" ]
        then
           #Host name is not defined
           Hostname=`hostname -s`
        fi
   fi


   # Set environment
   filename="$2"

#   sed -i s/\'//g "$filename"

   catline=""
   while IFS= read -r line; do
       # If the line starts with ST then set var to yes.
       # Allow the string to concatenate until we hit the
       # end of the record, then insert to the database
       if [[ $line == *AUDIT* ]] ; then
           # ignore any of the audit processors, such
           # as this one and the database activity parser
           if [[ $line != *audit* ]] ; then
               if [[ $line != *cron* ]] ; then
                   printline="yes"
               fi
           fi
       fi
       # If variable is yes, print the line.
       if [[ $printline == "yes" ]] ; then
           catline="$catline $line"
#           catline="$(echo "$catline" | tr -d '\040\011\012\015')"
           catline="$(echo "$catline" | tr -d '\011\012\015')"
       fi

       # If the line starts with ST then set var to no. 
       # When we hit the end of the record insert to 
       # database a reset the string
       if [[ $line == *logged* ]] ; then
           printline="no"
           echo ${catline}
           catline=""
       fi
   done < "$filename"

   # Cleanup
   rm -Rf ${DATA_DIR}/log/sed*
   rm -Rf ${LOCKFILE}
else
   echo "Read Log  already running"
fi

exit 0

#######################################
# End of script
#######################################

