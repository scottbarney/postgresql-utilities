#!/bin/bash

#################################################
# Script to toggle all queues off
# USAGE:  ./vacuum_log_drop.sh <CLIENT> <HOST> <PORT_NUMBER> <DATABASE> <DEBUG>

#################################################
# Help section
if [ -z $1 ]
then
   echo "Usage: ./vacuum_log_drop.sh <CLIENT> <HOST> <PORT_NUMBER> <DATABASE> <DEBUG>"
   echo "   --Script that vacuums full the audit.log_drop table"
   exit 0
fi


# Set environment
CLIENT="$1"
HOST="$2"
PORT_NUMBER="$3"
DB="$4"
DEBUG="$5"

export BIN_HOME=$HOME/${CLIENT}/bin
export ENV_HOME=$HOME/${CLIENT}/config
export LOG_HOME=$HOME/${CLIENT}/log
export SQL_HOME=$HOME/${CLIENT}/sql
export LOCK_HOME=$HOME/${CLIENT}/lock
export LOCKFILE=$LOCK_HOME/vacuum_log_drop.lock

if [ ! -f $LOCKFILE ]; then
   touch ${LOCKFILE}

   if [ -z "$Hostname" ]
   then

        if [ "$Hostname" = "" ]
        then
           #Host name is not defined
           Hostname=`hostname -s`
        fi
   fi

   . $ENV_HOME/$PORT_NUMBER.env

#   . $BIN_HOME/queue_off.sh
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -c "select audit.queues_off()"
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -c "select pg_sleep(180)"
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -c "vacuum full audit.log_drop"   
#   . $BIN_HOME/queue_on.sh
   psql -U postgres -d $DB -h $HOST -p $PORT_NUMBER -c "select audit.queues_on()"    



# Cleanup
   rm -Rf ${LOCKFILE}
else
   echo "queue_off.sh already running"
fi

exit 0

#######################################
# End of script
#######################################

