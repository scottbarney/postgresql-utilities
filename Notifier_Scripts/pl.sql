CREATE OR REPLACE FUNCTION infra.activate_notify(
	in_email character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      email_in alias for $1;
begin
    begin
        update infra.notifies
           set n_active = true
         where lower(n_email) = lower(email_in);
    exception
         when others
         then
            raise notice 'error on update %', sqlerrm;
            return 1;
    end;
return 0;
end;

$BODY$;

ALTER FUNCTION infra.activate_notify(character varying)
    OWNER TO postgres;


CREATE OR REPLACE FUNCTION infra.add_notify(
	in_level character varying,
	in_email character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      level_in alias for $1;
      email_in alias for $2;
begin
    begin
        insert into infra.notifies
             ( n_level
             , n_email
             )
        values
             ( level_in
             , email_in
             );
    exception
         when others
         then
            raise notice 'error on insert %', sqlerrm;
            return 1;
    end;
return 0;
end;

$BODY$;

ALTER FUNCTION infra.add_notify(character varying, character varying)
    OWNER TO postgres;


CREATE OR REPLACE FUNCTION infra.check_rules(
	)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      rCur cursor
      for 
      select rule_name
           , rule_sql
           , rule_alert_value
           , alert_level
        from infra.event_rules;
        
      rRec record;
      
      r_sql text;
      r_result integer;
begin
    open rCur;
    loop
       fetch rCur into rRec;
       exit when not found;
       
       r_sql := rRec.rule_sql;
       execute r_sql into r_result;
       
       if r_result = rRec.rule_alert_value
       then
          raise notice 'alert triggered';
          perform infra.save_event( rRec.rule_name::text, false::text, now()::text, 
                                   'alert triggered: ' || rRec.rule_sql || ' has ' || r_result || ' rows '::text,
                                    rRec.alert_level );
       else
          raise notice 'no alert';
       end if;
       
    end loop;
    close rCur;
    
  return 0;
end;

$BODY$;

ALTER FUNCTION infra.check_rules()
    OWNER TO postgres;


CREATE OR REPLACE FUNCTION infra.deactivate_notify(
	in_email character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      email_in alias for $1;
begin
    begin
        update infra.notifies
           set n_active = false
         where lower(n_email) = lower(email_in);
    exception
         when others
         then
            raise notice 'error on update %', sqlerrm;
            return 1;
    end;
return 0;
end;

$BODY$;

ALTER FUNCTION infra.deactivate_notify(character varying)
    OWNER TO postgres;

CREATE OR REPLACE FUNCTION infra.get_email(
	in_level character varying)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      level_in alias for $1;

      lemail_block text;

      mCur cursor (level_in character varying)
      for
      select n_email
       from infra.notifies
      where lower(n_level) = lower(level_in);

      mRec record;

begin
      lemail_block := '';
      open mCur(level_in);
      loop
         fetch mCur into mRec;
         exit when not found;

         lemail_block := lemail_block ||
                         mRec.n_email || ', ';

      end loop;
      close mCur;

      lemail_block := trim(trailing ', ' from lemail_block);

return lemail_block;
end;

$BODY$;

ALTER FUNCTION infra.get_email(character varying)
    OWNER TO postgres;

CREATE OR REPLACE FUNCTION infra.get_email(
	in_level character varying)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      level_in alias for $1;

      lemail_block text;

      mCur cursor (level_in character varying)
      for
      select n_email
       from infra.notifies
      where lower(n_level) = lower(level_in);

      mRec record;

begin
      lemail_block := '';
      open mCur(level_in);
      loop
         fetch mCur into mRec;
         exit when not found;

         lemail_block := lemail_block ||
                         mRec.n_email || ', ';

      end loop;
      close mCur;

      lemail_block := trim(trailing ', ' from lemail_block);

return lemail_block;
end;

$BODY$;

ALTER FUNCTION infra.get_email(character varying)
    OWNER TO postgres;

CREATE OR REPLACE FUNCTION infra.queue_msg(event_in integer
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      in_event alias for $1;
      
      mCur cursor
      for
      select event_name
           , successful
           , event_timestamp::timestamp(0) as event_timestamp
           , replace(coalesce(event_detail,null,'none'),'''','') as details
           , event_level
        from infra.events
       where event_id = in_event;


      mRec record;

      lmsg_block text;
      lmsg_header text;
      header_len integer := 0;
      block_len integer := 0;

begin

    lmsg_header := 'check                              time                          event';
    lmsg_block := '';
   

    open mCur;
    loop
       fetch mCur into mRec;
       exit when not found;

       
       lmsg_block := lmsg_block ||                       
                     rpad(mRec.event_name,35,' ') || 
                     rpad(mRec.event_timestamp::text,30,' ') || 
                     mRec.details || 
                     mRec.event_level || E'\n';

    end loop;
    close mCur;
 
    block_len := length(lmsg_block);
    lmsg_block := lmsg_header || E'\n' || lmsg_block;
 
return lmsg_block;
end;

$BODY$;

CREATE OR REPLACE FUNCTION infra.save_event(
	in_event text,
	in_success text,
	in_timestamp text,
	in_details text,
	in_level text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
      event_in alias for $1;
      success_in alias for $2;
      time_in alias for $3;
      detail_in alias for $4;
      level_in alias for $5;
      email_addrs text;
      msg text;
      
      e_id integer;
begin
    begin
        insert into infra.events
             ( event_name
             , successful
             , event_timestamp
             , event_detail
             , event_level
             )
        values
             ( event_in
             , success_in::boolean
             , time_in::timestamp with time zone
             , detail_in
             , level_in
             );
             
        select max(event_id)
          into e_id
          from infra.events;
          
    exception
         when others
         then
            raise notice 'error on insert %',sqlerrm;
    end;
    
    email_addrs := infra.get_email(level_in);
    msg := infra.queue_msg(e_id);
    
    insert into infra.email_queue
         ( send_to
         , digest
         , email_timestamp
         )
    values
         ( email_addrs
         , msg
         , now()::timestamp(0)
         );
    
return 0;
end;

$BODY$;

ALTER FUNCTION infra.save_event(text, text, text, text, text)
    OWNER TO postgres;


