create table infra.event_rules 
( rule_id serial
, rule_name character varying
, rule_sql  text
, rule_alert_value integer 
, constraint event_rules_pk primary key ( rule_id )
);

CREATE TABLE infra.email_log
(
    email_id serial,
    send_to character varying COLLATE pg_catalog."default",
    digest text COLLATE pg_catalog."default",
    email_timestamp timestamp with time zone,
    CONSTRAINT email_log_pk PRIMARY KEY (email_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE infra.email_log
    OWNER to postgres;

CREATE TABLE infra.email_queue
(
    email_id serial,
    send_to character varying COLLATE pg_catalog."default",
    digest text COLLATE pg_catalog."default",
    email_timestamp timestamp with time zone,
    CONSTRAINT email_queue_pk PRIMARY KEY (email_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE infra.email_queue
    OWNER to postgres;

CREATE TABLE infra.event_rules
(
    rule_id serial,
    rule_name character varying COLLATE pg_catalog."default",
    rule_sql text COLLATE pg_catalog."default",
    rule_alert_value integer,
    alert_level character varying COLLATE pg_catalog."default" DEFAULT 'primary'::character varying,
    CONSTRAINT event_rules_pk PRIMARY KEY (rule_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE infra.event_rules
    OWNER to postgres;

CREATE TABLE infra.events
(
    event_id serial,
    event_name character varying COLLATE pg_catalog."default",
    successful boolean,
    event_timestamp timestamp with time zone,
    event_detail text COLLATE pg_catalog."default",
    event_level character varying COLLATE pg_catalog."default",
    CONSTRAINT events_pk PRIMARY KEY (event_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE infra.events
    OWNER to postgres;

CREATE TABLE infra.notifies
(
    n_level character varying COLLATE pg_catalog."default" NOT NULL,
    n_email character varying COLLATE pg_catalog."default" NOT NULL,
    n_active boolean NOT NULL DEFAULT true,
    CONSTRAINT notifies_pk PRIMARY KEY (n_level, n_email)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE infra.notifies
    OWNER to postgres;
