create table infra.empty_table ( col_1 character varying );

create table infra.populated_table ( col_1 character varying );

insert into infra.populated_table values ('this table has data');


insert into infra.event_rules
( rule_name 
, rule_sql
, rule_alert_value
)
values
( 'empty_table_check'
, 'select count(*) from infra.empty_table'
, 0
);

insert into infra.event_rules
( rule_name
, rule_sql
, rule_alert_value
)
values
( 'populated_table_check'
, 'select count(*) from infra.populated_table'
, 0
);


