#!/bin/bash -x
#
# 
export ENV_HOME=/home/postgres/ivy20/bin/config
export LOG_HOME=/home/postgres/ivy20/log
export PGAPPNAME=mailer

. $ENV_HOME/5432.env

/bin/date '+Started at %T on %D'

NOW=`date +%m-%d-%Y`

if [ $# -lt 1 ]
then
   echo "Usage: ./mailer.sh -p<port>"
   exit 1
else
   while [ ! -z $1 ]
   do
     case "$1" in
         -p*) PORT=`echo $1 | sed -e 's/\-p//'`
              ;;
         * ) echo "Invalid argument -> $1"
             exit 1
             ;;
     esac
     shift
   done
fi

SLOG=${LOG_HOME}/${PORT}_mailer_${NOW}.log
echo $SLOG

exec &>> $SLOG
. $ENV_HOME/$PORT.env

psql -h $PGHOST -U postgres -d $PG_DB -A -F ' ' -t --quiet -c "select email_id from infra.email_queue" | while read -a Record ; do 
     email_id=${Record[0]} 
      
     send_to=$(psql -h $PGHOST -U postgres -d $PG_DB -A -F ' ' -t --quiet -c "select send_to from infra.email_queue where email_id = $email_id")
     digest=$(psql -h $PGHOST -U postgres -d $PG_DB  -t -c "select digest from infra.email_queue where email_id = $email_id")

     echo "$email_id"
     echo "$send_to"
     echo "$digest" | mailx -s 'Alert' $send_to

     res=$(psql -h $PGHOST -U postgres -d $PG_DB -c "select infra.log_sent($email_id)")

done

/bin/date '+Finished at %T on %D'
exit 0

