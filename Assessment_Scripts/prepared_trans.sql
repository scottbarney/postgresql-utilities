SELECT database, ROUND(EXTRACT(epoch FROM now()-prepared)) AS age, prepared
FROM pg_prepared_xacts
ORDER BY prepared ASC
