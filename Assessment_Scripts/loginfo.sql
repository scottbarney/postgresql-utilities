SELECT name, CASE WHEN length(setting)<1 THEN '?' ELSE setting END AS s
FROM pg_settings
WHERE name IN ('log_destination','log_directory','log_filename','redirect_stderr','syslog_facility')
ORDER BY name
