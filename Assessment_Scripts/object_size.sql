SELECT relname AS "relation"
     , case 
          when(c.relkind = 'i') then 'index'
          when(c.relkind = 'r') then 'table'
          when(c.relkind = 'c') then 'composite type'
          when(c.relkind = 't') then 'TOAST table'
          when(c.relkind = 'v') then 'view'
          when(c.relkind = 'S') then 'sequence'
       end as "type"
     , pg_size_pretty(pg_relation_size(C.oid)) AS "size"
  FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema')
  ORDER BY case 
             when(c.relkind = 'i') then 'index'
             when(c.relkind = 'r') then 'table'
             when(c.relkind = 'c') then 'composite type'
             when(c.relkind = 't') then 'TOAST table'
             when(c.relkind = 'v') then 'view'
             when(c.relkind = 'S') then 'sequence'
          end 
         , relname
         , pg_relation_size(C.oid) DESC
