select a.datname
     , pg_size_pretty(pg_database_size(a.datid)) as size
     , cast(blks_hit/(blks_read+blks_hit+0.000001)*100.0 as numeric(5,2)) as cache_hit_pct
     , cast(xact_commit/(xact_rollback+xact_commit+0.000001)*100.0 as numeric(5,2)) as transact_success_pct 
  from pg_stat_database a order by a.datname
