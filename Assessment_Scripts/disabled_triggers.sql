SELECT tgrelid::regclass AS tname, tgname, tgenabled
FROM pg_trigger
WHERE tgenabled = 'D' ORDER BY tgname
