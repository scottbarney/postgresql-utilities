﻿SELECT DISTINCT ON (nspname, seqname) nspname, seqname,
  quote_ident(nspname) || '.' || quote_ident(seqname) AS safename, typname
  -- sequences by column dependency
FROM (
 SELECT depnsp.nspname, dep.relname as seqname, typname
 FROM pg_depend
 JOIN pg_class on classid = pg_class.oid
 JOIN pg_class dep on dep.oid = objid
 JOIN pg_namespace depnsp on depnsp.oid= dep.relnamespace
 JOIN pg_class refclass on refclass.oid = refclassid
 JOIN pg_class ref on ref.oid = refobjid
 JOIN pg_namespace refnsp on refnsp.oid = ref.relnamespace
 JOIN pg_attribute refattr ON (refobjid, refobjsubid) = (refattr.attrelid, refattr.attnum)
 JOIN pg_type ON refattr.atttypid = pg_type.oid
 WHERE pg_class.relname = 'pg_class'
 AND refclass.relname = 'pg_class'
 AND dep.relkind in ('S')
 AND ref.relkind in ('r')
 AND typname IN ('int2', 'int4', 'int8')
 UNION ALL
 --sequences by parsing DEFAULT constraints
 SELECT nspname, seq.relname, typname
 FROM pg_attrdef
 JOIN pg_attribute ON (attrelid, attnum) = (adrelid, adnum)
 JOIN pg_type on pg_type.oid = atttypid
 JOIN pg_class rel ON rel.oid = attrelid
 JOIN pg_class seq ON seq.relname = regexp_replace(adsrc, $re$^nextval\('(.+?)'::regclass\)$$re$, $$\1$$)
 AND seq.relnamespace = rel.relnamespace
 JOIN pg_namespace nsp ON nsp.oid = seq.relnamespace
 WHERE adsrc ~ 'nextval' AND seq.relkind = 'S' AND typname IN ('int2', 'int4', 'int8')
 UNION ALL
 -- all sequences, to catch those whose associations are not obviously recorded in pg_catalog
 SELECT nspname, relname, CAST('int8' AS TEXT)
 FROM pg_class
 JOIN pg_namespace nsp ON nsp.oid = relnamespace
 WHERE relkind = 'S'
) AS seqs
WHERE nspname !~ '^pg_temp.*'
ORDER BY nspname, seqname, typname