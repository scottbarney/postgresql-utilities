SELECT pg_namespace.nspname,
       pg_size_pretty(sum(pg_relation_size(pg_class.oid))) as size
FROM   pg_class
       INNER JOIN pg_namespace
         ON relnamespace = pg_namespace.oid
GROUP BY pg_namespace.nspname
ORDER  BY pg_namespace.nspname,
          pg_size_pretty(sum(pg_relation_size(pg_class.oid))) DESC;
