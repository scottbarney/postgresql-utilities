SELECT pg_database_size(d.oid) AS dsize,
  pg_size_pretty(pg_database_size(d.oid)) AS pdsize,
  datname,
  usename
FROM pg_database d
LEFT JOIN pg_user u ON (u.usesysid=d.datdba)
order by datname;
