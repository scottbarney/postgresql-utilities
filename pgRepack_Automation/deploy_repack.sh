#!/bin/bash -x
#
# deploy_repack.sh [host] [port]
#
#
/bin/date '+Started at %T on %D'
export ENV_HOME=$HOME/bin/config
export LOG_HOME=$HOME/log
NOW=`date +%m-%d-%Y`

# default values for HOST and PORT if a HOST and PORT are not specified during execution
HOST="localhost"
PORT=5432

HOST=$1
PORT=$2

echo $HOST
echo $PORT

SLOG=${LOG_HOME}/${PORT}_deploy_repack_${NOW}.log
echo $SLOG

for dbname in $(psql -U postgres -p $PORT -h $HOST -t -c "select datname from pg_database where datname not in ('template1','template0') order by datname"); do
  echo $dbname

  psql -U postgres -d ${dbname} -p $PORT -h $HOST -f table_bloat_view.sql
  psql -U postgres -d ${dbname} -p $PORT -h $HOST -c "create extension pg_repack;"

done
/bin/date '+Finished at %T on %D'
exit 0
