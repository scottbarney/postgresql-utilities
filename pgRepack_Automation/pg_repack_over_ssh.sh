#!/bin/bash
#
# pg_repack_over_ssh.sh
#
#
/bin/date '+Started at %T on %D'
export ENV_HOME=$HOME/bin/config
export LOG_HOME=$HOME/log
NOW=`date +%m-%d-%Y`

if [ $# -ne 3 ]
then
   echo "Usage: ./pg_repack_dbs.sh -p<port> -h<host> -j<jobcount>"
   exit 1
else
   while [ ! -z $1 ]
   do
     case "$1" in
         -p*) PORT=`echo $1 | sed -e 's/\-p//'`
              ;;
         -h*) HOST=`echo $1 | sed -e 's/\-h//'`
              ;;
         -j*) JOBCOUNT=`echo $1 | sed -e 's/\-j//'`
         * ) echo "Invalid argument -> $1"
             exit 1
             ;;
     esac
     shift
   done

fi

SLOG=${LOG_HOME}/${PORT}_pg_repack_${NOW}.log
echo $SLOG

exec &>> $SLOG
. $ENV_HOME/$PORT.env

for dbname in $(psql -U postgres -h $HOST -p $PORT -t -c "select datname from pg_database where datname not in ('template1','template0') order by datname"); do
  echo $dbname
  
  for schema_tab in $(psql -U postgres -h $HOST -d ${dbname} -p $PORT -t -c "select schemaname || '.' || tablename from table_bloat where schemaname != 'pg_catalog'"); do
    echo "------------------------"
    echo "${schema_tab}"
    echo "------------------------"
    
    schema_tab_schema=$(echo $schema_tab | cut -f1 -d".")
    schema_tab_tab=$(echo $schema_tab | cut -f2 -d".")
    echo "Table Size: " $(psql -U postgres -h $HOST -d ${dbname} -p $PORT -t -c "select pg_size_pretty(pg_relation_size('${schema_tab}'))")
    echo "Initial Table Bloat (in bytes): " $(psql -U postgres -h $HOST -d ${dbname} -p $PORT -t -c "select wastedsize from table_bloat where schemaname='${schema_tab_schema}' and tablename='${schema_tab_tab}'")
    
    bstart=`date +%s`  
    ssh -q postgres@${HOST} ". ~/.bash_profile; pg_repack -e -U postgres -d ${dbname} -h ${HOST} -j $(($JOBCOUNT)) --table $schema_tab"
    bend=`date +%s`

    dt=$(echo "$bend-$bstart" | bc)
    dd=$(echo "$dt/86400" | bc)
    dt2=$(echo "$dt-86400*$dd" | bc)
    dh=$(echo "$dt2/3600" | bc)
    dt3=$(echo "$dt2-3600*$dh" | bc)
    dm=$(echo "$dt3/60" | bc)
    ds=$(echo "$dt3-60*$dm" | bc)

    printf "============================================\n"
    printf "Repack complete for ${schema_tab} for ${dbname} on port ${PORT}\n"
    echo "Final Table Bloat (in bytes): " $(psql -U postgres -h $HOST -d ${dbname} -p $PORT -t -c "select wastedsize from table_bloat where schemaname='${schema_tab_schema}' and tablename='${schema_tab_tab}'")
    printf "Runtime (days:hh:mm:ss): %d:%02d:%02d:%02d\n" $dd $dh $dm $ds
  done
done
/bin/date '+Finished at %T on %D'
exit 0

