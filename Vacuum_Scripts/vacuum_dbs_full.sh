#!/bin/bash -x
#
# vacuum_dbs.sh
#
#
/bin/date '+Started at %T on %D'
export ENV_HOME=/home/postgres/ivy96/bin/config
export LOG_HOME=/home/postgres/ivy96/log
export SQL_HOME=/home/postgres/ivy96/sql

NOW=`date +%m-%d-%Y`

if [ $# -lt 1 ]
then
   echo "Usage: ./vacuum_dbs_full.sh -p<port>"
   exit 1
else
   while [ ! -z $1 ]
   do
     case "$1" in
         -p*) PORT=`echo $1 | sed -e 's/\-p//'`
              ;;
         * ) echo "Invalid argument -> $1"
             exit 1
             ;;
     esac
     shift
   done
fi

SLOG=${LOG_HOME}/${PORT}_FULL_${NOW}.log
echo $SLOG

exec &>> $SLOG
. $ENV_HOME/$PORT.env

for dbname in $(psql -U postgres -p $PGPORT -h $PGHOST -t -c "select datname from pg_database order by datname"); do
  echo $dbname

  bstart=`date +%s`  
  vacuumdb -U postgres -d $dbname -p $PGPORT -h $PGHOST -f -v -z
  bend=`date +%s`

  dt=$(echo "$bend-$bstart" | bc)
  dd=$(echo "$dt/86400" | bc)
  dt2=$(echo "$dt-86400*$dd" | bc)
  dh=$(echo "$dt2/3600" | bc)
  dt3=$(echo "$dt2-3600*$dh" | bc)
  dm=$(echo "$dt3/60" | bc)
  ds=$(echo "$dt3-60*$dm" | bc)

  printf "============================================\n"
  printf "Vacuum Full complete for ${dbname} on port ${PGPORT}\n"
  printf "Runtime (days:hh:mm:ss): %d:%02d:%02d:%02d\n" $dd $dh $dm $ds

  SHLOG=${LOG_HOME}/${PORT}_${dbname}_${NOW}_short.log
  tail -3 "${SLOG}" > $SHLOG
  IFS=
  #mail -s "Vacuum Full Complete for ${dbname} on port ${PGPORT} at ${HOSTNAME}" "<email>" <<< $(cat "$SHLOG")
done
/bin/date '+Finished at %T on %D'
exit 0
