CREATE TABLE if not exists logger.job_check_config
(
  job_name text NOT NULL,
  warn_threshold interval NOT NULL,
  error_threshold interval NOT NULL,
  active boolean NOT NULL DEFAULT false,
  sensitivity smallint NOT NULL DEFAULT 0,
  escalate integer,
  CONSTRAINT job_check_config_job_name_pkey PRIMARY KEY (job_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE logger.job_check_config
  OWNER TO postgres;

