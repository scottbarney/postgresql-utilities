CREATE TABLE if not exists logger.job_check_log
(
  job_id bigint NOT NULL,
  job_name text NOT NULL,
  alert_code integer NOT NULL DEFAULT 3
)
WITH (
  OIDS=FALSE
);
ALTER TABLE logger.job_check_log
  OWNER TO postgres;

