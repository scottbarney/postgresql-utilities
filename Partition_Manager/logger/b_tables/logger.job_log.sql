CREATE TABLE if not exists logger.job_log
(
  job_id bigint NOT NULL DEFAULT nextval('logger.job_log_job_id_seq'::regclass),
  owner text NOT NULL,
  job_name text NOT NULL,
  start_time timestamp with time zone NOT NULL,
  end_time timestamp with time zone,
  status text,
  pid integer NOT NULL,
  CONSTRAINT job_log_job_id_pkey PRIMARY KEY (job_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE logger.job_log
  OWNER TO postgres;

-- Index: logger.job_log_job_name_idx

-- DROP INDEX logger.job_log_job_name_idx;

CREATE INDEX job_log_job_name_idx
  ON logger.job_log
  USING btree
  (job_name COLLATE pg_catalog."default");

-- Index: logger.job_log_pid_idx

-- DROP INDEX logger.job_log_pid_idx;

CREATE INDEX job_log_pid_idx
  ON logger.job_log
  USING btree
  (pid);

-- Index: logger.job_log_start_time_idx

-- DROP INDEX logger.job_log_start_time_idx;

CREATE INDEX job_log_start_time_idx
  ON logger.job_log
  USING btree
  (start_time);

-- Index: logger.job_log_status_idx

-- DROP INDEX logger.job_log_status_idx;

CREATE INDEX job_log_status_idx
  ON logger.job_log
  USING btree
  (status COLLATE pg_catalog."default");


