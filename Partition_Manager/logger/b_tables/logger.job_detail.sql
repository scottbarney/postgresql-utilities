CREATE TABLE if not exists logger.job_detail
(
  job_id bigint NOT NULL,
  step_id bigint NOT NULL DEFAULT nextval('logger.job_detail_step_id_seq'::regclass),
  action text NOT NULL,
  start_time timestamp with time zone NOT NULL,
  end_time timestamp with time zone,
  elapsed_time real,
  status text,
  message text,
  CONSTRAINT job_detail_step_id_pkey PRIMARY KEY (step_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE logger.job_detail
  OWNER TO postgres;

-- Index: logger.job_detail_job_id_idx

-- DROP INDEX logger.job_detail_job_id_idx;

CREATE INDEX job_detail_job_id_idx
  ON logger.job_detail
  USING btree
  (job_id);
