CREATE TABLE if not exists logger.job_status_text
(
  alert_code integer NOT NULL,
  alert_text text NOT NULL,
  CONSTRAINT job_status_text_alert_code_pkey PRIMARY KEY (alert_code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE logger.job_status_text
  OWNER TO postgres;

