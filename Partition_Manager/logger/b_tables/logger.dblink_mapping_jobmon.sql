CREATE TABLE if not exists logger.dblink_mapping_jobmon
(
  username text,
  port text,
  pwd text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE logger.dblink_mapping_jobmon
  OWNER TO postgres;
