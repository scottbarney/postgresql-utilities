CREATE OR REPLACE FUNCTION logger._autonomous_add_step(
    p_job_id bigint,
    p_action text)
  RETURNS bigint AS
$BODY$
DECLARE
    v_step_id bigint;
BEGIN
    v_step_id := nextval('logger.job_detail_step_id_seq');

    insert into logger.job_detail
         ( job_id
         , step_id
         , action
         , start_time
         )
    values
         ( p_job_id
         , v_step_id
         , p_action
         , current_timestamp
         );

    return v_step_id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger._autonomous_add_step(bigint, text)
  OWNER TO postgres;

