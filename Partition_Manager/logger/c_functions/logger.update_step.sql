CREATE OR REPLACE FUNCTION logger.update_step(
    p_step_id bigint,
    p_status text,
    p_message text)
  RETURNS void AS
$BODY$
DECLARE
    v_remote_query text;
    v_dblink_schema text;
BEGIN
    SELECT nspname INTO v_dblink_schema FROM pg_namespace n, pg_extension e WHERE e.extname = 'dblink' AND e.extnamespace = n.oid;
    
    v_remote_query := 'SELECT logger._autonomous_update_step ('||
    p_step_id || ',' ||
    quote_literal(p_status) || ',' ||
    quote_literal(p_message) || ')';

    EXECUTE 'SELECT devnull FROM ' || v_dblink_schema || '.dblink('||quote_literal(logger.auth())||
        ','|| quote_literal(v_remote_query) || ',TRUE) t (devnull int)';  
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger.update_step(bigint, text, text)
  OWNER TO postgres;

