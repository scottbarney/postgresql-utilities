CREATE OR REPLACE FUNCTION logger.close_job(p_job_id bigint)
  RETURNS void AS
$BODY$
DECLARE
    v_remote_query text;
    v_dblink_schema text;
BEGIN

    SELECT nspname INTO v_dblink_schema FROM pg_namespace n, pg_extension e WHERE e.extname = 'dblink' AND e.extnamespace = n.oid;
    
    v_remote_query := 'SELECT logger._autonomous_close_job('||p_job_id||')'; 

    EXECUTE 'SELECT devnull FROM ' || v_dblink_schema || '.dblink('||quote_literal(logger.auth())||
        ',' || quote_literal(v_remote_query) || ',TRUE) t (devnull int)';  
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger.close_job(bigint)
  OWNER TO postgres;

