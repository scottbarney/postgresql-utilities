CREATE OR REPLACE FUNCTION logger.show_detail(
    p_name text,
    integer DEFAULT 1)
  RETURNS SETOF logger.job_detail AS
$BODY$
DECLARE
    v_job_list      logger.job_log%ROWTYPE;
    v_job_detail     logger.job_detail%ROWTYPE;
BEGIN

    FOR v_job_list IN SELECT job_id, owner, job_name, start_time, end_time, status, pid  
        FROM logger.job_log
        WHERE job_name = upper(p_name)
        ORDER BY job_id DESC
        LIMIT $2
    LOOP
        FOR v_job_detail IN SELECT job_id, step_id, action, start_time, end_time, elapsed_time, status, message
            FROM logger.job_detail
            WHERE job_id = v_job_list.job_id
            ORDER BY step_id ASC
        LOOP
            RETURN NEXT v_job_detail; 
        END LOOP;
    END LOOP;

    RETURN;
END
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION logger.show_detail(text, integer)
  OWNER TO postgres;

