CREATE OR REPLACE FUNCTION logger.show_running(integer DEFAULT 10)
  RETURNS SETOF logger.job_log AS
$BODY$
DECLARE
    v_job_list      logger.job_log%ROWTYPE;
    v_version       int;
BEGIN

SELECT current_setting('server_version_num')::int INTO v_version;

IF v_version >= 90200 THEN
    FOR v_job_list IN SELECT j.job_id, j.owner, j.job_name, j.start_time, j.end_time, j.status, j.pid  
        FROM logger.job_log j
        JOIN pg_stat_activity p ON j.pid = p.pid
        WHERE p.state <> 'idle'
        AND j.status IS NULL
        ORDER BY j.job_id DESC
        LIMIT $1
    LOOP
        RETURN NEXT v_job_list; 
    END LOOP;
ELSE 
    FOR v_job_list IN SELECT j.job_id, j.owner, j.job_name, j.start_time, j.end_time, j.status, j.pid  
        FROM logger.job_log j
        JOIN pg_stat_activity p ON j.pid = p.procpid
        WHERE p.current_query <> '<IDLE>' 
        AND j.status IS NULL
        ORDER BY j.job_id DESC
        LIMIT $1
    LOOP
        RETURN NEXT v_job_list; 
    END LOOP;
END IF;

RETURN;

END
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION logger.show_running(integer)
  OWNER TO postgres;

