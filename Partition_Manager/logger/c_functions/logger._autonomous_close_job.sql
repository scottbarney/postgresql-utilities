CREATE OR REPLACE FUNCTION logger._autonomous_close_job(p_job_id bigint)
  RETURNS integer AS
$BODY$
DECLARE
    v_numrows integer;
    v_status text;
BEGIN   
    execute 'select alert_text from logger.job_status_text where alert_code = 1' into v_status;
    
    update logger.job_log 
       set end_time = current_timestamp
         , status = v_status
     where job_id = p_job_id;
     
    GET DIAGNOSTICS v_numrows = ROW_COUNT;
    
    RETURN v_numrows;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger._autonomous_close_job(bigint)
  OWNER TO postgres;

