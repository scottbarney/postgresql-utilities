CREATE OR REPLACE FUNCTION logger.add_job(p_job_name text)
  RETURNS bigint AS
$BODY$
declare 
    v_job_id bigint;
    v_remote_query text;
    v_dblink_schema text;
begin
    select nspname 
      into v_dblink_schema
      from pg_namespace n
         , pg_extension e
     where e.extname = 'dblink'
       and e.extnamespace = n.oid;
       
    
    v_remote_query := 'SELECT logger._autonomous_add_job (' ||
        quote_literal(current_user) || ',' ||
        quote_literal(p_job_name) || ',' ||
        pg_backend_pid() || ')';

    EXECUTE 'SELECT job_id FROM ' || v_dblink_schema || '.dblink('||quote_literal(logger.auth())||
        ','|| quote_literal(v_remote_query) || ',TRUE) t (job_id int)' INTO v_job_id;      

    IF v_job_id IS NULL THEN
        RAISE EXCEPTION 'Job creation failed';
    END IF;

    RETURN v_job_id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger.add_job(text)
  OWNER TO postgres;

