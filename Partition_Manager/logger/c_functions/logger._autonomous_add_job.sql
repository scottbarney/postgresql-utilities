CREATE OR REPLACE FUNCTION logger._autonomous_add_job(
    p_owner text,
    p_job_name text,
    p_pid integer)
  RETURNS bigint AS
$BODY$
DECLARE
    v_job_id bigint;
BEGIN
    v_job_id := nextval('logger.job_log_job_id_seq');

    insert into logger.job_log 
         ( job_id
         , owner
         , job_name
         , start_time
         , pid 
         )
    values 
         ( v_job_id
         , p_owner
         , lower(p_job_name)
         , current_timestamp
         , p_pid
         ); 

    RETURN v_job_id; 
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger._autonomous_add_job(text, text, integer)
  OWNER TO postgres;

