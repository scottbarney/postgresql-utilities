CREATE OR REPLACE FUNCTION logger._autonomous_update_step(
    p_step_id bigint,
    p_status text,
    p_message text)
  RETURNS integer AS
$BODY$
DECLARE
    v_numrows integer;
BEGIN
    update logger.job_detail
       set end_time = current_timestamp
         , elapsed_time = date_part('epoch',now() - start_time)::real
         , status = p_status
         , message = p_message
     where step_id = p_step_id; 
     
    GET DIAGNOSTICS v_numrows = ROW_COUNT;
    RETURN v_numrows;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger._autonomous_update_step(bigint, text, text)
  OWNER TO postgres;

