CREATE OR REPLACE FUNCTION logger.show_job_status(
    p_status text,
    integer DEFAULT 10)
  RETURNS SETOF logger.job_log AS
$BODY$
DECLARE
    v_job_list      logger.job_log%ROWTYPE;
BEGIN
    FOR v_job_list IN SELECT job_id, owner, job_name, start_time, end_time, status, pid  
        FROM logger.job_log
        WHERE status = p_status
        ORDER BY job_id DESC
        LIMIT $2
    LOOP
        RETURN NEXT v_job_list; 
    END LOOP;

    RETURN;
END
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION logger.show_job_status(text, integer)
  OWNER TO postgres;

