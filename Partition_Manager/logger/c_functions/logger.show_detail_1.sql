CREATE OR REPLACE FUNCTION logger.show_detail(p_id bigint)
  RETURNS SETOF logger.job_detail AS
$BODY$
DECLARE
    v_job_detail     logger.job_detail%ROWTYPE;
BEGIN
    FOR v_job_detail IN SELECT job_id, step_id, action, start_time, end_time, elapsed_time, status, message
        FROM logger.job_detail
        WHERE job_id = p_id
        ORDER BY step_id ASC
    LOOP
        RETURN NEXT v_job_detail; 
    END LOOP;

    RETURN;
END
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION logger.show_detail(bigint)
  OWNER TO postgres;

