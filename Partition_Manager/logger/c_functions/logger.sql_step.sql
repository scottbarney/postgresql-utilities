CREATE OR REPLACE FUNCTION logger.sql_step(
    p_job_id bigint,
    p_action text,
    p_sql text)
  RETURNS boolean AS
$BODY$
DECLARE
    v_step_id   bigint;
    v_numrows   bigint;
BEGIN
    v_step_id := logger.add_step(p_job_id, p_action);
    EXECUTE p_sql;
    GET DIAGNOSTICS v_numrows = ROW_COUNT;
    PERFORM logger.update_step(v_step_id, 'OK', 'Rows affected: ' || v_numrows);
    PERFORM logger.close_job(p_job_id);

    RETURN true;
EXCEPTION
    WHEN OTHERS THEN
        PERFORM logger.update_step(v_step_id, 'CRITICAL', 'ERROR: '||coalesce(SQLERRM,'unknown'));
        RETURN false;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger.sql_step(bigint, text, text)
  OWNER TO postgres;

