alter table logger.job_detail
add   CONSTRAINT job_detail_job_id_fkey FOREIGN KEY (job_id)
      REFERENCES logger.job_log (job_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE;
