CREATE SEQUENCE logger.job_detail_step_id_seq start with 1;

ALTER TABLE logger.job_detail_step_id_seq
  OWNER TO postgres;

