CREATE SEQUENCE logger.job_log_job_id_seq start with 1;

ALTER TABLE logger.job_log_job_id_seq
  OWNER TO postgres;
