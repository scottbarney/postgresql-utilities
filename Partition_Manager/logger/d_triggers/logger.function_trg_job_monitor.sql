CREATE TRIGGER trg_job_monitor
  AFTER UPDATE
  ON logger.job_log
  FOR EACH ROW
  EXECUTE PROCEDURE logger.job_monitor();
