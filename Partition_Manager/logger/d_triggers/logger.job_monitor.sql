CREATE OR REPLACE FUNCTION logger.job_monitor()
  RETURNS trigger AS
$BODY$
DECLARE
    v_alert_code    int;
BEGIN

    SELECT alert_code INTO v_alert_code FROM logger.job_status_text WHERE alert_text = NEW.status;
    IF v_alert_code IS NOT NULL THEN
        IF v_alert_code = 1 THEN
            DELETE FROM logger.job_check_log WHERE job_name = NEW.job_name;
        ELSE
            INSERT INTO logger.job_check_log (job_id, job_name, alert_code) VALUES (NEW.job_id, NEW.job_name, v_alert_code);
        END IF;
    END IF;

    RETURN NULL;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger.job_monitor()
  OWNER TO postgres;

