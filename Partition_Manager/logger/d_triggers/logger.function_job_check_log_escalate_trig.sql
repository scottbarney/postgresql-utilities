CREATE TRIGGER job_check_log_escalate_trig
  BEFORE INSERT
  ON logger.job_check_log
  FOR EACH ROW
  EXECUTE PROCEDURE logger.job_check_log_escalate_trig();
