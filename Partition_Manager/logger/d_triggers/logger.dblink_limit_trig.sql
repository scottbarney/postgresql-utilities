CREATE OR REPLACE FUNCTION logger.dblink_limit_trig()
  RETURNS trigger AS
$BODY$
DECLARE
v_count     smallint;
BEGIN

    EXECUTE 'SELECT count(*) FROM '|| TG_TABLE_SCHEMA ||'.'|| TG_TABLE_NAME INTO v_count;
    IF v_count > 1 THEN
        RAISE EXCEPTION 'Only a single row may exist in this table';
    END IF;

    RETURN NULL;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION logger.dblink_limit_trig()
  OWNER TO postgres;

