CREATE TRIGGER dblink_limit_trig
  AFTER INSERT
  ON logger.dblink_mapping_jobmon
  FOR EACH ROW
  EXECUTE PROCEDURE logger.dblink_limit_trig();

