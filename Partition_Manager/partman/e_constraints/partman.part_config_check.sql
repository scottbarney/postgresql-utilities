alter table partman.part_config
add CONSTRAINT part_config_type_check CHECK (partman.check_partition_type(partition_type));
