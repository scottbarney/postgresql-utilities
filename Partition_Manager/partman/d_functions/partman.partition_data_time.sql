CREATE OR REPLACE FUNCTION partman.partition_data_time(
    p_parent_table text,
    p_batch_count integer DEFAULT 1,
    p_batch_interval interval DEFAULT NULL::interval,
    p_lock_wait numeric DEFAULT 0,
    p_order text DEFAULT 'asC'::text,
    p_analyze boolean DEFAULT true)
  RETURNS bigint AS
$BODY$
declare

v_control                   text;
v_datetime_string           text;
v_current_partition_name    text;
v_epoch                     boolean;
v_last_partition            text;
v_lock_iter                 int := 1;
v_lock_obtained             boolean := FALSE;
v_max_partition_timestamp   timestamptz;
v_min_partition_timestamp   timestamptz;
v_new_search_path           text := 'partman,pg_temp';
v_old_search_path           text;
v_parent_schema             text;
v_parent_tablename          text;
v_partition_expression      text;
v_partition_interval        interval;
v_partition_suffix          text;
v_partition_timestamp       timestamptz[];
v_quarter                   text;
v_rowcount                  bigint;
v_start_control             timestamptz;
v_time_position             int;
v_total_rows                bigint := 0;
v_type                      text;
v_year                      text;

begin

select current_setting('search_path') into v_old_search_path;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

select partition_type
    , partition_interval::interval
    , control
    , datetime_string
    , epoch
into v_type
    , v_partition_interval
    , v_control
    , v_datetime_string
    , v_epoch
from partman.part_config 
where parent_table = p_parent_table
and (partition_type = 'time' OR partition_type = 'time-custom');
if not FOUND then
    execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');
    raise EXCEPTIon 'ERROR: no config found for %', p_parent_table;
end if;

if p_batch_interval is null OR p_batch_interval > v_partition_interval then
    p_batch_interval := v_partition_interval;
end if;

select schemaname, tablename into v_parent_schema, v_parent_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;

select partition_tablename into v_last_partition from partman.show_partitions(p_parent_table, 'DESC') LIMIT 1;

v_partition_expression := case
    when v_epoch = true then format('to_timestamp(%I)', v_control)
    else format('%I', v_control)
end;

for i in 1..p_batch_count loop

    if p_order = 'asC' then
        execute format('select min(%s) from onLY %I.%I', v_partition_expression, v_parent_schema, v_parent_tablename) into v_start_control;
    ELSif p_order = 'DESC' then
        execute format('select max(%s) from onLY %I.%I', v_partition_expression, v_parent_schema, v_parent_tablename) into v_start_control;
    else
        raise EXCEPTIon 'invalid value for p_order. Must be asC or DESC';
    end if;

    if v_start_control is null then
        EXIT;
    end if;

    if v_type = 'time' then
        CasE
            WHEN v_partition_interval = '15 mins' then
                v_min_partition_timestamp := date_trunc('hour', v_start_control) + 
                    '15min'::interval * floor(date_part('minute', v_start_control) / 15.0);
            WHEN v_partition_interval = '30 mins' then
                v_min_partition_timestamp := date_trunc('hour', v_start_control) + 
                    '30min'::interval * floor(date_part('minute', v_start_control) / 30.0);
            WHEN v_partition_interval = '1 hour' then
                v_min_partition_timestamp := date_trunc('hour', v_start_control);
            WHEN v_partition_interval = '1 day' then
                v_min_partition_timestamp := date_trunc('day', v_start_control);
            WHEN v_partition_interval = '1 week' then
                v_min_partition_timestamp := date_trunc('week', v_start_control);
            WHEN v_partition_interval = '1 month' then
                v_min_partition_timestamp := date_trunc('month', v_start_control);
            WHEN v_partition_interval = '3 months' then
                v_min_partition_timestamp := date_trunc('quarter', v_start_control);
            WHEN v_partition_interval = '1 year' then
                v_min_partition_timestamp := date_trunc('year', v_start_control);
        end CasE;
    ELSif v_type = 'time-custom' then
        select child_start_time into v_min_partition_timestamp from partman.show_partition_info(v_parent_schema||'.'||v_last_partition
            , v_partition_interval::text
            , p_parent_table);
        v_max_partition_timestamp := v_min_partition_timestamp + v_partition_interval;
        loop
            if v_start_control >= v_min_partition_timestamp and v_start_control < v_max_partition_timestamp then
                EXIT;
            else
                begin
                    if v_start_control > v_max_partition_timestamp then
                        -- Keep going forward in time, checking if child partition time interval encompasses the current v_start_control value
                        v_min_partition_timestamp := v_max_partition_timestamp;
                        v_max_partition_timestamp := v_max_partition_timestamp + v_partition_interval;

                    else
                        -- Keep going backwards in time, checking if child partition time interval encompasses the current v_start_control value
                        v_max_partition_timestamp := v_min_partition_timestamp;
                        v_min_partition_timestamp := v_min_partition_timestamp - v_partition_interval;
                    end if;
                EXCEPTIon WHEN datetime_field_overflow then
                    raise EXCEPTIon 'Attempted partition time interval is outside PostgreSQL''s supported time range. 
                        Unable to create partition with interval before timestamp % ', v_min_partition_interval;
                end;
            end if;
        end loop;

    end if;

    v_partition_timestamp := array[v_min_partition_timestamp];
    if p_order = 'asC' then
        -- Ensure batch interval given as parameter doesn't cause maximum to overflow the current partition maximum
        if (v_start_control + p_batch_interval) >= (v_min_partition_timestamp + v_partition_interval) then
            v_max_partition_timestamp := v_min_partition_timestamp + v_partition_interval;
        else
            v_max_partition_timestamp := v_start_control + p_batch_interval;
        end if;
    ELSif p_order = 'DESC' then
        -- Must be greater than max value still in parent table since query below grabs < max
        v_max_partition_timestamp := v_min_partition_timestamp + v_partition_interval;
        -- Ensure batch interval given as parameter doesn't cause minimum to underflow current partition minimum
        if (v_start_control - p_batch_interval) >= v_min_partition_timestamp then
            v_min_partition_timestamp = v_start_control - p_batch_interval;
        end if;
    else
        raise EXCEPTIon 'invalid value for p_order. Must be asC or DESC';
    end if;

-- do some locking with timeout, if required
    if p_lock_wait > 0  then
        v_lock_iter := 0;
        WHILE v_lock_iter <= 5 loop
            v_lock_iter := v_lock_iter + 1;
            begin
                execute format('select * from onLY %I.%I where %s >= %L and %3$s < %5$L for UPDATE NOWAIT'
                    , v_parent_schema
                    , v_parent_tablename
                    , v_partition_expression
                    , v_min_partition_timestamp
                    , v_max_partition_timestamp);
                v_lock_obtained := TRUE;
            EXCEPTIon
                WHEN lock_not_available then
                    perform pg_sleep( p_lock_wait / 5.0 );
                    ConTinUE;
            end;
            EXIT WHEN v_lock_obtained;
        end loop;
        if not v_lock_obtained then
           return -1;
        end if;
    end if;

    perform partman.create_partition_time(p_parent_table, v_partition_timestamp, p_analyze);
    -- This suffix generation code is in create_partition_time() as well
    v_partition_suffix := to_char(v_min_partition_timestamp, v_datetime_string);
    v_current_partition_name := partman.check_name_length(v_parent_tablename, v_partition_suffix, TRUE);

    execute format('with partition_data as (
                        DELETE from onLY %I.%I where %s >= %L and %3$s < %5$L returninG *)
                     inSERT into %I.%I select * from partition_data'
                        , v_parent_schema
                        , v_parent_tablename
                        , v_partition_expression
                        , v_min_partition_timestamp
                        , v_max_partition_timestamp
                        , v_parent_schema
                        , v_current_partition_name);
    GET DIAGNOSTICS v_rowcount = ROW_COUNT;
    v_total_rows := v_total_rows + v_rowcount;
    if v_rowcount = 0 then
        EXIT;
    end if;

end loop; 

perform partman.create_function_time(p_parent_table);

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

return v_total_rows;

end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.partition_data_time(text, integer, interval, numeric, text, boolean)
  OWNER TO postgres;

