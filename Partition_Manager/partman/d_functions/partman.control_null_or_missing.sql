CREATE OR REPLACE FUNCTION partman.control_null_or_missing(
    sch_in character varying,
    tab_in character varying,
    col_in character varying)
  RETURNS boolean AS
$BODY$
declare
      in_sch alias for $1;
      in_tab alias for $2;
      in_col alias for $3;

      nnull boolean;
begin
    select attnotnull
      into nnull
      from pg_catalog.pg_attribute a
      join pg_catalog.pg_class c on a.attrelid = c.oid
      join pg_catalog.pg_namespace n on c.relnamespace = n.oid
     where c.relname = in_tab::name
       and n.nspname = in_sch::name
       and a.attname = in_col::name;

    if nnull is false or nnull is null
    then
       return true;
    else 
       return false;
    end if;
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.control_null_or_missing(character varying, character varying, character varying)
  OWNER TO postgres;

