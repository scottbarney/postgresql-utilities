CREATE OR REPLACE FUNCTION partman.clone_archive_table(
    child_schema_in character varying,
    child_table_in character varying,
    parent_schema_in character varying,
    parent_table_in character varying,
    archive_tbs_in character varying,
    job_id_in bigint DEFAULT 0,
    debug_in boolean DEFAULT false)
  RETURNS integer AS
$BODY$
declare
      c_sch alias for $1;
      c_tab alias for $2;
      p_sch alias for $3;
      p_tab alias for $4;
      a_tbs alias for $5;
      in_job alias for $6;
      in_debug alias for $7;
      
      s_stmt text;
      component text := 'partman.clone_archive_table';
      o_search text;
      job_id bigint;
      step_id bigint;
      
      archive_name character varying;
      orig_name character varying;
      noinherit_name character varying;
      parent_name character varying;
begin
    archive_name := c_sch || '.' || c_tab || '_a';
    orig_name := c_sch || '.' || c_tab;
    noinherit_name := c_tab || '_o';
    parent_name := p_sch || '.' || p_tab;    

    if not partman.tablespace_exists(a_tbs)
    then
       perform partman.notice(component, 'tablespace: ' || a_tbs || ' does not exist');
       return 1;
    end if;
    
    -- get/set search path
    o_search := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    if in_job != 0 and in_job is not null
    then
       job_id := in_job;
    else
       job_id := add_job(component || ' for: ' || orig_name );
    end if;
     
    -- create archive partition
    s_stmt := 'create' || format(' table %I (like %I.%I including defaults including constraints including indexes including comments)'
                                , archive_name
                                , p_sch
                                , p_tab );
                                
    s_stmt := s_stmt || ' tablespace ' || a_tbs;
    step_id := add_step(job_id, 'create clone: ' || archive_name );

    s_stmt := replace(s_stmt,'"','');
    
    if in_debug
    then
       perform partman.notice(component, 'create clone: ' || s_stmt);
    end if;

    begin
        perform public.dblink(logger.auth(),s_stmt); 
        perform update_step(step_id,'OK','clone created: ' || archive_name );              
    exception
         when others
         then
            perform partman.exception(job_id,step_id,component,'clone creation: ' || archive_name || ' failed',sqlerrm);
            return 1;
    end;

    -- copy data
    s_stmt := 'insert into ' || 
               archive_name ||
              ' select * from ' || orig_name;

    step_id := add_step(job_id, 'insert clone data: ' || archive_name );

    if in_debug
    then
       perform partman.notice(component, 'insert clone data: ' || s_stmt);
    end if;                

    begin               
        perform public.dblink(logger.auth(),s_stmt);          
        perform update_step(step_id,'OK','inserted clone data: ' || archive_name );              
    exception
         when others
         then
            perform partman.exception(job_id,step_id,component,'insert clone data: ' || archive_name || ' failed',sqlerrm);
            return 1;
    end;
               
    -- disinherit the child
    s_stmt := 'alter table ' || orig_name || 
              ' no inherit ' || parent_name;

    step_id := add_step(job_id, 'disinherit: ' || orig_name );

    if in_debug
    then
       perform partman.notice(component, 'disinherit: ' || s_stmt);
    end if;

    begin               
        perform public.dblink(logger.auth(),s_stmt);          
        perform update_step(step_id,'OK','disinherit ' || orig_name );              
    exception
         when others
         then
            perform partman.exception(job_id,step_id,component,'disinherit: ' || orig_name || ' failed',sqlerrm);
            return 1;
    end;
    
    -- rename the child
    s_stmt := 'alter table ' || orig_name || 
              ' rename to ' || noinherit_name;

    step_id := add_step(job_id, 'alter/rename original: ' || orig_name );

    if in_debug
    then
       perform partman.notice(component, 'rename original child sql: ' || s_stmt);
    end if;

    begin               
        perform public.dblink(logger.auth(),s_stmt);          
        perform update_step(step_id,'OK','rename ' || orig_name );              
    exception
         when others
         then
            perform partman.exception(job_id,step_id,component,'alter/rename: ' || orig_name || ' failed',sqlerrm);
            return 1;
    end;       
                     
    -- rename the sibling
    s_stmt := 'alter table ' || archive_name ||
              ' rename to ' || c_tab;

    step_id := add_step(job_id, 'alter/rename clone: ' || archive_name );

    if in_debug
    then
       perform partman.notice(component, 'rename new child sql: ' || s_stmt);
    end if;

    begin               
        perform public.dblink(logger.auth(),s_stmt);          
        perform update_step(step_id,'OK','rename ' || archive_name );              
    exception
         when others
         then
            perform partman.exception(job_id,step_id,component,'alter/rename: ' || archive_name || ' failed',sqlerrm);
            return 1;
    end;       
    
    -- inherit the sibling
    s_stmt := 'alter table ' || orig_name || 
               ' inherit ' || parent_name;

    step_id := add_step(job_id, 'inherit clone: ' || orig_name );

    if in_debug
    then
       perform partman.notice(component, 'alter table inherit sql: ' || s_stmt);
    end if;

    begin               
        perform public.dblink(logger.auth(),s_stmt);          
        perform update_step(step_id,'OK','inherit ' || orig_name );              
    exception
         when others
         then
            perform partman.exception(job_id,step_id,component,'alter/rename: ' || archive_name || ' failed',sqlerrm);
            return 1;
    end;     

    perform logger.close_job(job_id);  

    return 0;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.clone_archive_table(character varying, character varying, character varying, character varying, character varying, bigint, boolean)
  OWNER TO postgres;

