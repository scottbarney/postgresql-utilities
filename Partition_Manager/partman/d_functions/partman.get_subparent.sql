CREATE OR REPLACE FUNCTION partman.get_subparent(parent_in text)
  RETURNS text AS
$BODY$
declare
      p_in alias for $1;
      s_out text;
begin
    select sub_parent
      into s_out
      from partman.part_config_sub
     where sub_parent = p_in;

    return s_out;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.get_subparent(text)
  OWNER TO postgres;
