CREATE OR REPLACE FUNCTION partman.run_archiver(debug_in boolean DEFAULT false)
  RETURNS integer AS
$BODY$
declare
      in_debug alias for $1;

      tCur cursor
      for
      select parent_table
        from partman.part_config
       where partition_type in ( 'time','time-custom')
         and archive_tablespace is not null;

     tRec record;

     job_id  bigint;
     step_id bigint;     
     o_search text;
     component text := 'partman.run_archiver';
     status integer;
begin
    -- get/set search path
    o_search := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    job_id := add_job(component);

    open tCur;
    loop
       fetch tCur into tRec;
       exit when not found;

       status := partman.archive_table(tRec.parent_table,in_debug);

    end loop;
    close tCur;

    perform partman.set_search(o_search);
    perform logger.close_job(job_id);
    
    return status;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.run_archiver(boolean)
  OWNER TO postgres;
