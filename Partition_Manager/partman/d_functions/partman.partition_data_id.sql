CREATE OR REPLACE FUNCTION partman.partition_data_id(
    p_parent_table text,
    p_batch_count integer DEFAULT 1,
    p_batch_interval bigint DEFAULT NULL::bigint,
    p_lock_wait numeric DEFAULT 0,
    p_order text DEFAULT 'asC'::text,
    p_analyze boolean DEFAULT true)
  RETURNS bigint AS
$BODY$
declare

v_control                   text;
v_current_partition_name    text;
v_lock_iter                 int := 1;
v_lock_obtained             boolean := FALSE;
v_max_partition_id          bigint;
v_min_partition_id          bigint;
v_new_search_path           text := 'partman,pg_temp';
v_old_search_path           text;
v_parent_schema             text;
v_parent_tablename          text;
v_partition_interval        bigint;
v_partition_id              bigint[];
v_rowcount                  bigint;
v_sql                       text;
v_start_control             bigint;
v_total_rows                bigint := 0;

begin

select partition_interval::bigint
    , control
into v_partition_interval
    , v_control
from partman.part_config 
where parent_table = p_parent_table
and partition_type = 'id';
if not FOUND then
    raise EXCEPTIon 'ERROR: no config found for %', p_parent_table;
end if;

select current_setting('search_path') into v_old_search_path;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

select schemaname, tablename into v_parent_schema, v_parent_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;

if p_batch_interval is null OR p_batch_interval > v_partition_interval then
    p_batch_interval := v_partition_interval;
end if;

for i in 1..p_batch_count loop

    if p_order = 'asC' then
        execute format('select min(%I) from onLY %I.%I', v_control, v_parent_schema, v_parent_tablename) into v_start_control;
        if v_start_control is null then
            EXIT;
        end if;
        v_min_partition_id = v_start_control - (v_start_control % v_partition_interval);
        v_partition_id := array[v_min_partition_id];
        -- Check if custom batch interval overflows current partition maximum
        if (v_start_control + p_batch_interval) >= (v_min_partition_id + v_partition_interval) then
            v_max_partition_id := v_min_partition_id + v_partition_interval;
        else
            v_max_partition_id := v_start_control + p_batch_interval;
        end if;

    ELSif p_order = 'DESC' then
        execute 'select max('||v_control||') from onLY '||p_parent_table into v_start_control;
        if v_start_control is null then
            EXIT;
        end if;
        v_min_partition_id = v_start_control - (v_start_control % v_partition_interval);
        -- Must be greater than max value still in parent table since query below grabs < max
        v_max_partition_id := v_min_partition_id + v_partition_interval;
        v_partition_id := array[v_min_partition_id];
        -- Make sure minimum doesn't underflow current partition minimum
        if (v_start_control - p_batch_interval) >= v_min_partition_id then
            v_min_partition_id = v_start_control - p_batch_interval;
        end if;
    else
        raise EXCEPTIon 'invalid value for p_order. Must be asC or DESC';
    end if;

    -- do some locking with timeout, if required
    if p_lock_wait > 0  then
        v_lock_iter := 0;
        WHILE v_lock_iter <= 5 loop
            v_lock_iter := v_lock_iter + 1;
            begin
                v_sql := format('select * from onLY %I.%I where %I >= %s and %I < %s for UPDATE NOWAIT'
                                , v_parent_schema
                                , v_parent_tablename
                                , v_control
                                , v_min_partition_id
                                , v_control
                                , v_max_partition_id);
                execute v_sql;
                v_lock_obtained := TRUE;
            EXCEPTIon
                WHEN lock_not_available then
                    perform pg_sleep( p_lock_wait / 5.0 );
                    ConTinUE;
            end;
            EXIT WHEN v_lock_obtained;
        end loop;
        if not v_lock_obtained then
           return -1;
        end if;
    end if;

    perform partman.create_partition_id(p_parent_table, v_partition_id, p_analyze);
    v_current_partition_name := partman.check_name_length(v_parent_tablename, v_min_partition_id::text, TRUE);

    execute format('with partition_data as (
                        DELETE from onLY %I.%I where %I >= %s and %I < %s returninG *)
                    inSERT into %I.%I select * from partition_data'
                , v_parent_schema
                , v_parent_tablename
                , v_control
                , v_min_partition_id
                , v_control
                , v_max_partition_id
                , v_parent_schema
                , v_current_partition_name);

    GET DIAGNOSTICS v_rowcount = ROW_COUNT;
    v_total_rows := v_total_rows + v_rowcount;
    if v_rowcount = 0 then
        EXIT;
    end if;

end loop;

perform partman.create_function_id(p_parent_table, null, p_analyze);

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

return v_total_rows;

end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.partition_data_id(text, integer, bigint, numeric, text, boolean)
  OWNER TO postgres;

