CREATE OR REPLACE FUNCTION partman.create_trigger(p_parent_table text)
  RETURNS void AS
$BODY$
declare
      v_function_name         text;
      v_new_length            int;
      v_old_search_path       text;
      v_parent_schema         text;
      v_parent_tablename      text;
      v_trig_name             text;
      v_trig_sql              text;
      v_sql text;

     component text := 'partman.create_trigger';
     p_sch character varying := split_part(p_parent_table, '.', 1)::name;
     p_tab character varying := split_part(p_parent_table, '.', 2)::name;
begin
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    v_trig_name := partman.check_name_length(p_object_name := p_tab, p_suffix := '_part_trig'); 
    -- Ensure function name matches the naming pattern
    v_function_name := partman.check_name_length(p_tab, '_part_trig_func', FALSE);

    v_sql := 'drop trigger ' || v_trig_name || ' on ' || p_sch || '.' || p_tab;
    begin
        execute v_sql;
    exception
         when others
         then
            null;
    end;
    v_trig_sql := format('create trigger %I before insert on %I.%I for each row execute procedure %I.%I()'
                        , v_trig_name
                        , p_sch
                        , p_tab
                        , p_sch
                        , v_function_name);

    execute v_trig_sql;

    perform partman.set_search(v_old_search_path);

end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.create_trigger(text)
  OWNER TO postgres;

