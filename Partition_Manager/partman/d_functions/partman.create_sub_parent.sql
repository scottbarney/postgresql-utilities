CREATE OR REPLACE FUNCTION partman.create_sub_parent(
    p_top_parent text,
    p_control text,
    p_type text,
    p_interval text,
    p_constraint_cols text[] DEFAULT NULL::text[],
    p_premake integer DEFAULT 4,
    p_start_partition text DEFAULT NULL::text,
    p_inherit_fk boolean DEFAULT true,
    p_epoch boolean DEFAULT false,
    p_upsert text DEFAULT ''::text,
    p_trigger_return_null boolean DEFAULT true,
    p_jobmon boolean DEFAULT true,
    p_debug boolean DEFAULT false)
  RETURNS boolean AS
$BODY$
declare

v_last_partition    text;
v_new_search_path   text := 'partman,pg_temp';
v_old_search_path   text;
v_parent_interval   text;
v_parent_type       text;
v_row               record;
v_row_last_part     record;
v_run_maint         boolean;
v_sql               text;
v_success           boolean := false;
v_top_type          text;

begin

select use_run_maintenance into v_run_maint from partman.part_config where parent_table = p_top_parent;
if v_run_maint is null then
    raise EXCEPTIon 'Cannot subpartition a table that is not managed by pg_partman already. Given top parent table not found in partman.part_config: %', p_top_parent;
ELSif v_run_maint = false then
    raise EXCEPTIon 'Any parent table that will be part of a sub-partitioned set (on any level) must have use_run_maintenance set to true in part_config table, even for serial partitioning. See documentation for more info.';
end if;

if p_upsert <> '' and partman.check_version('9.5.0') = 'false' then
    raise EXCEPTIon 'inSERT ... on ConFLICT (UPSERT) feature is only supported in PostgreSQL 9.5 and later';
end if;

select current_setting('search_path') into v_old_search_path;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

for v_row in 
    -- loop through all current children to turn them into partitioned tables
    select partition_schemaname||'.'||partition_tablename as child_table from partman.show_partitions(p_top_parent)
loop
    select partition_type, partition_interval into v_parent_type, v_parent_interval from partman.part_config where parent_table = v_row.child_table;

    if v_parent_interval = p_interval then
        execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');
        raise EXCEPTIon 'Sub-partition interval cannot be equal to parent interval';
    end if;

    if (v_parent_type = 'time' OR v_parent_type = 'time-custom') 
       and (p_type = 'time' OR p_type = 'time-custom') 
    then
        if p_interval::interval > v_parent_interval::interval then
            execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');
            raise EXCEPTIon 'Sub-partition interval cannot be greater than the given parent interval';
        end if;
        if p_interval = 'weekly' and v_parent_interval::interval > '1 week'::interval then
            execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');
            raise EXCEPTIon 'Due to conflicting data boundaries between isO weeks and any larger interval of time, pg_partman cannot support a sub-partition interval of weekly';
        end if;
    ELSif v_parent_type = 'id' then
        if p_interval::bigint > v_parent_interval::bigint then
            execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');
            raise EXCEPTIon 'Sub-partition interval cannot be greater than the given parent interval';
        end if;
    end if;
        
    -- Just call existing create_parent() function but add the given parameters to the part_config_sub table as well
    v_sql := format('select partman.create_parent(
             p_parent_table := %L
            , p_control := %L
            , p_type := %L
            , p_interval := %L
            , p_constraint_cols := %L
            , p_premake := %L
            , p_use_run_maintenance := %L
            , p_start_partition := %L
            , p_inherit_fk := %L
            , p_epoch := %L
            , p_upsert := %L
            , p_trigger_return_null := %L
            , p_jobmon := %L
            , p_debug := %L )'
        , v_row.child_table
        , p_control
        , p_type
        , p_interval
        , p_constraint_cols
        , p_premake
        , true
        , p_start_partition
        , p_inherit_fk
        , p_epoch
        , p_upsert
        , p_trigger_return_null
        , p_jobmon
        , p_debug);
    execute v_sql;

end loop;

inSERT into partman.part_config_sub (
    sub_parent
    , sub_control
    , sub_partition_type
    , sub_partition_interval
    , sub_constraint_cols
    , sub_premake
    , sub_inherit_fk
    , sub_use_run_maintenance
    , sub_epoch
    , sub_upsert
    , sub_jobmon
    , sub_trigger_return_null)
VALUES (
    p_top_parent
    , p_control
    , p_type
    , p_interval
    , p_constraint_cols
    , p_premake
    , p_inherit_fk
    , true
    , p_epoch
    , p_upsert
    , p_jobmon
    , p_trigger_return_null);

v_success := true;

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

return v_success;

end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.create_sub_parent(text, text, text, text, text[], integer, text, boolean, boolean, text, boolean, boolean, boolean)
  OWNER TO postgres;

