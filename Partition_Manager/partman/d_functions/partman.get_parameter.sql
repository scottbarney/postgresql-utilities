CREATE OR REPLACE FUNCTION partman.get_parameter(p_name character varying)
  RETURNS character varying AS
$BODY$
declare
      in_name alias for $1;
      out_val character varying;
begin
    select param_value
      into out_val
      from partman.parameters
     where param_name = lower(in_name);

    return out_val;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.get_parameter(character varying)
  OWNER TO postgres;

