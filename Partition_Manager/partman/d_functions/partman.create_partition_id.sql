CREATE OR REPLACE FUNCTION partman.create_partition_id(
    p_parent_table text,
    p_partition_ids bigint[],
    p_analyze boolean DEFAULT true,
    p_debug boolean DEFAULT false)
  RETURNS boolean AS
$BODY$
declare

ex_context              text;
ex_detail               text;
ex_hint                 text;
ex_message              text;
v_all                   text[] := array['select', 'insert', 'update', 'delete', 'truncate', 'references', 'trigger'];
v_analyze               boolean := FALSE;
v_control               text;
v_exists                text;
v_grantees              text[];
v_hasoids               boolean;
v_id                    bigint;
v_inherit_fk            boolean;
v_job_id                bigint;
v_jobmon                boolean;
v_jobmon_schema         text;
v_new_search_path       text := 'partman,pg_temp';
v_old_search_path       text;
v_parent_grant          record;
v_parent_owner          text;
v_parent_schema         text;
v_parent_tablename      text;
v_parent_tablespace     text;
v_partition_interval    bigint;
v_partition_created     boolean := false;
v_partition_name        text;
v_revoke                text;
v_row                   record;
v_sql                   text;
v_step_id               bigint;
v_sub_id_max            bigint;
v_sub_id_min            bigint;
v_unlogged              char;

component text := 'partman.create_partition_id';

begin

select control
    , partition_interval
    , inherit_fk
    , jobmon
into v_control
    , v_partition_interval
    , v_inherit_fk
    , v_jobmon
from partman.part_config
where parent_table = p_parent_table
and partition_type = 'id';

if not FOUND then
    raise EXCEPTIon 'ERROR: no config found for %', p_parent_table;
end if;

select current_setting('search_path') into v_old_search_path;
if v_jobmon then
    select nspname into v_jobmon_schema from pg_catalog.pg_namespace n, pg_catalog.pg_extension e where e.extname = 'pg_jobmon'::name and e.extnamespace = n.oid;
    if v_jobmon_schema is not null then
        v_new_search_path := 'partman,'||v_jobmon_schema||',pg_temp';
    end if;
end if;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

-- Determine if this table is a child of a subpartition parent. if so, get limits of what child tables can be created based on parent suffix
select sub_min::bigint, sub_max::bigint into v_sub_id_min, v_sub_id_max from partman.check_subpartition_limits(p_parent_table, 'id');

select tableowner, schemaname, tablename, tablespace 
into v_parent_owner, v_parent_schema, v_parent_tablename, v_parent_tablespace 
from pg_catalog.pg_tables 
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;

if v_jobmon_schema is not null then
    v_job_id := add_job(format('PARTMAN CREATE TABLE: %s', p_parent_table));
end if;

foreach v_id in array p_partition_ids loop
-- Do not create the child table if it's outside the bounds of the top parent. 
    if v_sub_id_min is not null then
        if v_id < v_sub_id_min OR v_id > v_sub_id_max then
            ConTinUE;
        end if;
    end if;

    v_partition_name := partman.check_name_length(v_parent_tablename, v_id::text, TRUE);
    -- if child table already exists, skip creation
    select tablename into v_exists from pg_catalog.pg_tables where schemaname = v_parent_schema::name and tablename = v_partition_name::name;
    if v_exists is not null then
        ConTinUE;
    end if;

    -- Ensure analyze is run if a new partition is created. Otherwise if one isn't, will be false and analyze will be skipped
    v_analyze := TRUE;

    if v_jobmon_schema is not null then
        v_step_id := add_step(v_job_id, 'Creating new partition '||v_partition_name||' with interval from '||v_id||' to '||(v_id + v_partition_interval)-1);
    end if;

    select relpersistence into v_unlogged 
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on c.relnamespace = n.oid
    where c.relname = v_parent_tablename::name
    and n.nspname = v_parent_schema::name;
    v_sql := 'CREATE';
    if v_unlogged = 'u' then
        v_sql := v_sql || ' UNLOGGED';
    end if;
    v_sql := v_sql || format(' TABLE %I.%I (LIKE %I.%I inCLUDinG defaultS inCLUDinG ConSTRAinTS inCLUDinG inDEXES inCLUDinG STORAGE inCLUDinG COMMENTS)'
            , v_parent_schema
            , v_partition_name
            , v_parent_schema
            , v_parent_tablename);
    select relhasoids into v_hasoids 
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on c.relnamespace = n.oid
    where c.relname = v_parent_tablename::name
    and n.nspname = v_parent_schema::name;
    if v_hasoids is TRUE then
        v_sql := v_sql || ' with (OIDS)';
    end if;
    execute v_sql;
    if v_parent_tablespace is not null then
        execute format('ALTER TABLE %I.%I SET TABLESPACE %I', v_parent_schema, v_partition_name, v_parent_tablespace);
    end if;
    execute format('ALTER TABLE %I.%I ADD ConSTRAinT %I CHECK (%I >= %s and %I < %s )'
        , v_parent_schema
        , v_partition_name
        , v_partition_name||'_partition_check'
        , v_control
        , v_id
        , v_control
        , v_id + v_partition_interval);
    execute format('ALTER TABLE %I.%I inHERIT %I.%I', v_parent_schema, v_partition_name, v_parent_schema, v_parent_tablename);

    perform partman.apply_privileges(v_parent_schema, v_parent_tablename, v_parent_schema, v_partition_name, v_job_id);

    perform partman.apply_cluster(v_parent_schema, v_parent_tablename, v_parent_schema, v_partition_name);

    if v_inherit_fk then
        perform partman.apply_foreign_keys(p_parent_table, v_parent_schema||'.'||v_partition_name, v_job_id);
    end if;

    if v_jobmon_schema is not null then
        perform update_step(v_step_id, 'OK', 'Done');
    end if;

    -- Will only loop once and only if sub_partitioning is actually configured
    -- This seemed easier than assigning a bunch of variables then doing an if condition
    for v_row in 
        select sub_parent
            , sub_partition_type
            , sub_control
            , sub_partition_interval
            , sub_constraint_cols
            , sub_premake
            , sub_optimize_trigger
            , sub_optimize_constraint
            , sub_epoch
            , sub_inherit_fk
            , sub_retention
            , sub_retention_schema
            , sub_retention_keep_table
            , sub_retention_keep_index
            , sub_use_run_maintenance
            , sub_infinite_time_partitions
            , sub_jobmon
            , sub_trigger_exception_handling
        from partman.part_config_sub
        where sub_parent = p_parent_table
    loop
        if v_jobmon_schema is not null then
            v_step_id := add_step(v_job_id, 'Subpartitioning '||v_partition_name);
        end if;
        v_sql := format('select partman.create_parent(
                 p_parent_table := %L
                , p_control := %L
                , p_type := %L
                , p_interval := %L
                , p_constraint_cols := %L
                , p_premake := %L
                , p_use_run_maintenance := %L
                , p_inherit_fk := %L
                , p_epoch := %L
                , p_jobmon := %L )'
            , v_parent_schema||'.'||v_partition_name
            , v_row.sub_control
            , v_row.sub_partition_type
            , v_row.sub_partition_interval
            , v_row.sub_constraint_cols
            , v_row.sub_premake
            , v_row.sub_use_run_maintenance
            , v_row.sub_inherit_fk
            , v_row.sub_epoch
            , v_row.sub_jobmon);
        execute v_sql;

        UPDATE partman.part_config SET 
            retention_schema = v_row.sub_retention_schema
            , retention_keep_table = v_row.sub_retention_keep_table
            , retention_keep_index = v_row.sub_retention_keep_index
            , optimize_trigger = v_row.sub_optimize_trigger
            , optimize_constraint = v_row.sub_optimize_constraint
            , infinite_time_partitions = v_row.sub_infinite_time_partitions
            , trigger_exception_handling = v_row.sub_trigger_exception_handling
        where parent_table = v_parent_schema||'.'||v_partition_name;

        if v_jobmon_schema is not null then
            perform update_step(v_step_id, 'OK', 'Done');
        end if;

    end loop; -- end sub partitioning loop
    
    -- Manage additonal constraints if set
    perform partman.apply_constraints(p_parent_table, p_job_id := v_job_id, p_debug := p_debug);

    v_partition_created := true;

end loop;

-- v_analyze is a local check if a new table is made.
-- p_analyze is a parameter to say whether to run the analyze at all. Used by create_parent() to avoid long exclusive lock or run_maintenence() to avoid long creation runs.
if v_analyze and p_analyze then
    if v_jobmon_schema is not null then
        v_step_id := add_step(v_job_id, format('Analyzing partition set: %s', p_parent_table));
    end if;

    execute format('ANALYZE %I.%I', v_parent_schema, v_parent_tablename);

    if v_jobmon_schema is not null then
        perform update_step(v_step_id, 'OK', 'Done');
    end if;
end if;

if v_jobmon_schema is not null then
    if v_partition_created = false then
        v_step_id := add_step(v_job_id, format('No partitions created for partition set: %s', p_parent_table));
        perform update_step(v_step_id, 'OK', 'Done');
    end if;

    perform close_job(v_job_id);
end if;
 
execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

return v_partition_created;

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.create_partition_id(text, bigint[], boolean, boolean)
  OWNER TO postgres;

