CREATE OR REPLACE FUNCTION partman.reapply_privileges(p_parent_table text)
  RETURNS void AS
$BODY$
declare

ex_context          text;
ex_detail           text;
ex_hint             text;
ex_message          text;
v_job_id            bigint;
v_jobmon            boolean;
v_jobmon_schema     text;
v_new_search_path   text := 'partman,pg_temp';
v_old_search_path   text;
v_parent_schema     text;
v_parent_tablename  text;
v_row               record;
v_step_id           bigint;

component text := 'partman.reapply_privileges';

begin

select jobmon into v_jobmon from partman.part_config where parent_table = p_parent_table;
if v_jobmon is null then
    raise EXCEPTIon 'Given table is not managed by this extention: %', p_parent_table;
end if;

select current_setting('search_path') into v_old_search_path;
if v_jobmon then
    select nspname into v_jobmon_schema from pg_catalog.pg_namespace n, pg_catalog.pg_extension e where e.extname = 'pg_jobmon'::name and e.extnamespace = n.oid;
    if v_jobmon_schema is not null then
        v_new_search_path := 'partman,'||v_jobmon_schema||',pg_temp';
    end if;
end if;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');


select schemaname, tablename into v_parent_schema, v_parent_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;
if v_parent_tablename is null then
    execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');
    raise EXCEPTIon 'Given parent table does not exist: %', p_parent_table;
end if;

if v_jobmon_schema is not null then
    v_job_id := add_job(format('PARTMAN RE-APPLYinG PRIVILEGES TO ALL CHILD TABLES OF: %s', p_parent_table));
end if;

for v_row in 
    select partition_schemaname, partition_tablename from partman.show_partitions(p_parent_table, 'asC')
loop
    perform partman.apply_privileges(v_parent_schema, v_parent_tablename, v_row.partition_schemaname, v_row.partition_tablename, v_job_id);
end loop;

if v_jobmon_schema is not null then
    perform close_job(v_job_id);
end if;

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.reapply_privileges(text)
  OWNER TO postgres;

