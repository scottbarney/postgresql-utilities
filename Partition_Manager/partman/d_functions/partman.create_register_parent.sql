CREATE OR REPLACE FUNCTION partman.create_register_parent(
    p_parent_table text,
    p_control text,
    p_type text,
    p_interval text,
    p_archive_tablespace character varying DEFAULT NULL::character varying,
    p_archive_rotation integer DEFAULT 0,
    p_constraint_cols text[] DEFAULT NULL::text[],
    p_premake integer DEFAULT 4,
    p_use_run_maintenance boolean DEFAULT NULL::boolean,
    p_start_partition text DEFAULT NULL::text,
    p_inherit_fk boolean DEFAULT true,
    p_epoch boolean DEFAULT false,
    p_upsert text DEFAULT ''::text,
    p_trigger_return_null boolean DEFAULT true,
    p_jobmon boolean DEFAULT true,
    p_debug boolean DEFAULT true)
  RETURNS boolean AS
$BODY$
declare

      ex_context                      text;
      ex_detail                       text;
      ex_hint                         text;
      ex_message                      text;
      
      v_base_timestamp                timestamptz;
      v_count                         int := 1;
      v_datetime_string               text;
      v_id_interval                   bigint;
      v_job_id                        bigint;
      v_jobmon_schema                 text;
      v_last_partition_created        boolean;
      v_max                           bigint;
      v_notnull                       boolean;
      
      v_old_search_path               text;
      
      v_parent_partition_id           bigint;
      v_parent_partition_timestamp    timestamptz;
      v_partition_time                timestamptz;
      v_partition_time_array          timestamptz[];
      v_partition_id_array            bigint[];
      
      v_row                           record;
      v_run_maint                     boolean;
      v_sql                           text;
      v_start_time                    timestamptz;
      v_starting_partition_id         bigint;
      v_step_id                       bigint;
      v_step_overflow_id              bigint;
      v_sub_parent                    text;
      v_success                       boolean := false;
      v_time_interval                 interval;
      v_top_datetime_string           text;

      component text := 'partman.create_parent';
      
      p_sch character varying := split_part(p_parent_table,'.',1);
      p_tab character varying:= split_part(p_parent_table,'.',2);
      t_sch character varying;
      t_tab character varying;
      h_sch character varying;
      h_tab character varying;
      
begin
    -- table name must be schema qualified
    if position('.' in p_parent_table) = 0
    then
       perform partman.exception(component, 'Parent table must be schema qualified');
    end if;

    -- check for upsert
    if p_upsert != '' and partman.check_version('9.5.0') = 'false'
    then
       perform partman.exception(component,'insert...on conflict (upsert) feature only supported >= 9.5');
    end if;

   -- make sure parent table exists
    if not partman.table_exists( p_sch, p_tab )
    then
       perform partman.exception( component, 'unable to locate parent table, please create ' || p_parent_table || ' first' );
    end if;

    -- make sure control column is defined as not null
    if partman.control_null_or_missing( p_sch, p_tab, p_control )
    then
       perform partman.exception( component, 'control column: ' || p_control || ' is missing or nullable for ' || p_parent_table );
    end if;

    if p_type = 'id' and p_epoch 
    then
       perform partman.exception( component, 'p_epoch can only be used with time-based partitioning');
    end if;

    if not partman.check_partition_type(p_type)
    then
       perform partman.exception( component, p_type || ' is not a valid partitioning type');
    end if;

    -- get/set search path
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));
        
    v_run_maint := partman.check_run_maintenance(p_use_run_maintenance, p_type);
    perform partman.lock_table( p_sch, p_tab );

    v_job_id := add_job(format('partman setup parent: %s', p_parent_table));
    v_step_id := add_step(v_job_id, format('creating initial partitions on new parent: %s', p_parent_table));

    perform partman.insert_part_config_sub( p_sch, p_tab );
    
    if p_type in ( 'time', 'time-custom' )
    then
       v_time_interval := partman.get_time_interval(p_type, p_interval);
       -- first partition is either the min premake or p_start_partition
       
       v_start_time := coalesce(p_start_partition::timestamptz, current_timestamp - ( v_time_interval * p_premake)); 
       v_datetime_string := partman.get_suffix(v_time_interval,p_interval,v_start_time);
       v_base_timestamp := partman.get_base_timestamp(v_time_interval,p_interval,v_start_time); 
       v_partition_time_array := array_append(v_partition_time_array, v_base_timestamp);
       
       loop
          if (v_base_timestamp + (v_time_interval * v_count)) < (CURRENT_TIMESTAMP + (v_time_interval * p_premake))
          then
             begin
                 v_partition_time := (v_base_timestamp + (v_time_interval * v_count))::timestamptz;
                 v_partition_time_array := array_append(v_partition_time_array, v_partition_time);
             exception
                  when datetime_field_overflow
                  then
                     perform partman.warning( component, 'Attempted partition time interval is outside PostgreSQL''s supported time range. 
                                                          Child partition creation after time ' || v_partition_time || ' skipped');
                     v_step_overflow_id := add_step( v_job_id, 'Attempted partition time interval is outside PostgreSQL''s supported time range.' );
                     perform update_step( v_step_overflow_id, 'CRITICAL', 'Child partition creation after time '||v_partition_time||' skipped' );
                     continue;                                                       
             end;
          else
             exit;
          end if;
          v_count := v_count + 1;
       end loop;

       perform partman.insert_part_config( p_parent_table
                                         , p_type
                                         , v_time_interval
                                         , p_epoch
                                         , p_control
                                         , p_premake
                                         , p_constraint_cols
                                         , v_datetime_string
                                         , v_run_maint
                                         , p_inherit_fk
                                         , p_jobmon
                                         , p_upsert
                                         , p_trigger_return_null 
                                         , p_archive_tablespace
                                         , p_archive_rotation
                                         );
 
       v_last_partition_created := partman.create_partition_time(p_parent_table, v_partition_time_array, false);

       if not v_last_partition_created 
       then 
           -- This can happen with subpartitioning when future or past partitions prevent child creation because they're out of range of the parent
           -- First see if this parent is a subpartition managed by pg_partman
          with top_oid as (
              select i.inhparent as top_parent_oid
                from pg_catalog.pg_inherits i
                join pg_catalog.pg_class c on c.oid = i.inhrelid
                join pg_catalog.pg_namespace n on n.oid = c.relnamespace
               where c.relname = p_tab::name
                 and n.nspname = p_sch::name
              ) select n.nspname
                     , c.relname 
                  into t_sch
                     , t_tab 
                  from pg_catalog.pg_class c
                  join pg_catalog.pg_namespace n on n.oid = c.relnamespace
                  join top_oid t on c.oid = t.top_parent_oid
                  join partman.part_config p on p.parent_table = n.nspname||'.'||c.relname;
        
          if t_tab is not null 
          then
              -- if so create the lowest possible partition that is within the boundary of the parent
              select child_start_time 
                into v_parent_partition_timestamp 
                from partman.show_partition_info(p_parent_table, p_parent_table := t_sch||'.'||t_tab);
              
              if v_base_timestamp >= v_parent_partition_timestamp 
              then
                 while v_base_timestamp >= v_parent_partition_timestamp 
                 loop
                    v_base_timestamp := v_base_timestamp - v_time_interval;
                 end loop;
                 v_base_timestamp := v_base_timestamp + v_time_interval; -- add one back since while loop set it one lower than is needed
              elsif v_base_timestamp < v_parent_partition_timestamp 
              then
                 while v_base_timestamp < v_parent_partition_timestamp 
                 loop
                    v_base_timestamp := v_base_timestamp + v_time_interval;
                 end loop;
                 -- Don't need to remove one since new starting time will fit in top parent interval
              end if;
              v_partition_time_array := null;
              v_partition_time_array := array_append(v_partition_time_array, v_base_timestamp);
              v_last_partition_created := partman.create_partition_time(p_parent_table, v_partition_time_array, false);
          else
             -- Currently unknown edge case if code gets here
             perform partman.notice( component
                                  , 'No child tables created.');
          end if; 
       end if; 
       perform update_step(v_step_id, 'OK', format('Time partitions premade: %s', p_premake));
    end if;

    if p_type = 'id' 
    then
       v_id_interval := p_interval::bigint;
       if v_id_interval < 10 
       then
          perform partman.exception( v_job_id
                                   , v_step_id
                                   , component
                                   ,  'interval for serial partitioning must be greater than or equal to 10');
       end if;

       -- Check if parent table is a subpartition of an already existing id partition set managed by pg_partman. 
       while h_tab is not null 
       loop 
          with top_oid as (
               select i.inhparent as top_parent_oid
                 from pg_catalog.pg_inherits i
                 join pg_catalog.pg_class c on c.oid = i.inhrelid
                 join pg_catalog.pg_namespace n on n.oid = c.relnamespace
                where n.nspname = p_sch::name
                  and c.relname = p_tab::name
              ) select n.nspname, c.relname
                  into h_sch, h_tab
                  from pg_catalog.pg_class c
                  join pg_catalog.pg_namespace n on n.oid = c.relnamespace
                  join top_oid t on c.oid = t.top_parent_oid
                  join partman.part_config p on p.parent_table = n.nspname||'.'||c.relname
                 where p.partition_type = 'id';

          if h_tab is not null 
          then
             t_sch := h_sch;
             t_tab := h_tab;
          end if;
       end loop;

       -- if custom start partition is set, use that.
       -- if custom start is not set and there is already data, start partitioning with the highest current value and ensure it's grabbed from highest top parent table
       if p_start_partition is not null 
       then
           v_max := p_start_partition::bigint;
       else
           v_sql := format('select COALESCE(max(%I)::bigint, 0) from %I.%I LIMIT 1'
                    , p_control
                    , t_sch
                    , t_tab);
           execute v_sql into v_max;
       end if;
       v_starting_partition_id := v_max - (v_max % v_id_interval);

    
       for i in 0..p_premake 
       loop
          -- only make previous partitions if ID value is less than the starting value and positive (and custom start partition wasn't set)
          if p_start_partition is null and 
              (v_starting_partition_id - (v_id_interval*i)) > 0 and 
              (v_starting_partition_id - (v_id_interval*i)) < v_starting_partition_id 
          then
             v_partition_id_array = array_append(v_partition_id_array, (v_starting_partition_id - v_id_interval*i));
          end if; 
          v_partition_id_array = array_append(v_partition_id_array, (v_id_interval*i) + v_starting_partition_id);
       end loop;

       perform partman.insert_part_config( p_parent_table
                                         , p_type
                                         , v_id_interval
                                         , null
                                         , p_control
                                         , p_premake
                                         , p_constraint_cols
                                         , null
                                         , v_run_maint
                                         , p_inherit_fk
                                         , p_jobmon
                                         , p_upsert
                                         , p_trigger_return_null 
                                         , p_archive_tablespace
                                         , p_archive_rotation
                                        );
        
       v_last_partition_created := partman.create_partition_id(p_parent_table, v_partition_id_array, false);

       if v_last_partition_created = false 
       then
          -- This can happen with subpartitioning when future or past partitions prevent child creation because they're out of range of the parent
          -- See if it's actually a subpartition of a parent id partition
          with top_oid as (
               select i.inhparent as top_parent_oid
                 from pg_catalog.pg_inherits i
                 join pg_catalog.pg_class c on c.oid = i.inhrelid
                 join pg_catalog.pg_namespace n on n.oid = c.relnamespace
               where c.relname = p_tab::name
                 and n.nspname = p_sch::name
              ) select n.nspname||'.'||c.relname
                  into t_tab
                  from pg_catalog.pg_class c
                  join pg_catalog.pg_namespace n on n.oid = c.relnamespace
                  join top_oid t on c.oid = t.top_parent_oid
                  join partman.part_config p on p.parent_table = n.nspname||'.'||c.relname
                 where p.partition_type = 'id';
                 
          if t_tab is not null 
          then
             -- Create the lowest possible partition that is within the boundary of the parent
             select child_start_id 
               into v_parent_partition_id 
               from partman.show_partition_info(p_parent_table, p_parent_table := v_top_parent_table);
               
             if v_starting_partition_id >= v_parent_partition_id 
             then
                while v_starting_partition_id >= v_parent_partition_id 
                loop
                    v_starting_partition_id := v_starting_partition_id - v_id_interval;
                end loop;
                v_starting_partition_id := v_starting_partition_id + v_id_interval; -- add one back since while loop set it one lower than is needed
             elsif v_starting_partition_id < v_parent_partition_id 
             then
                while v_starting_partition_id < v_parent_partition_id 
                loop
                    v_starting_partition_id := v_starting_partition_id + v_id_interval;
                end loop;
                -- Don't need to remove one since new starting id will fit in top parent interval
             end if;
             v_partition_id_array = null;
             v_partition_id_array = array_append(v_partition_id_array, v_starting_partition_id);
             v_last_partition_created := partman.create_partition_id(p_parent_table, v_partition_id_array, false);
          else
             -- Currently unknown edge case if code gets here
             perform partman.exception( v_job_id
                                      , v_step_id
                                      , component
                                      ,  'No child tables created. Unexpected edge case encountered. Please report this error to author with conditions that led to it. ');
          end if;        
       end if;
    end if;

    v_step_id := add_step(v_job_id, 'Creating partition function');
    
    if p_type in ('time','time-custom') 
    then
       perform partman.create_function_time(p_parent_table, v_job_id);
       perform update_step(v_step_id, 'OK', 'Time function created');
    elsif p_type = 'id' 
    then
       perform partman.create_function_id(p_parent_table, v_job_id); 
       perform update_step(v_step_id, 'OK', 'ID function created');
    end if;

    v_step_id := add_step(v_job_id, 'Creating partition trigger');
    
    perform partman.create_trigger(p_parent_table);

    perform update_step(v_step_id, 'OK', 'Done');
    if v_step_overflow_id is not null 
    then
        perform fail_job(v_job_id);
    else
        perform close_job(v_job_id);
    end if;

    perform partman.set_search(v_old_search_path);
    v_success := true;

return v_success;

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.create_register_parent(text, text, text, text, character varying, integer, text[], integer, boolean, text, boolean, boolean, text, boolean, boolean, boolean)
  OWNER TO postgres;

