CREATE OR REPLACE FUNCTION partman.parent_exists(
    sch_in character varying,
    tab_in character varying)
  RETURNS boolean AS
$BODY$
declare
      in_sch alias for $1;
      in_tab alias for $2;

      cnt integer;
begin
    select count(*)
      into cnt
      from pg_catalog.pg_tables
     where schemaname = in_sch
       and tablename = in_tab;

    if cnt = 0
    then
       return false;
    else
       return true;
    end if;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.parent_exists(character varying, character varying)
  OWNER TO postgres;

