CREATE OR REPLACE FUNCTION partman.apply_constraints(
    p_parent_table text,
    p_child_table text DEFAULT NULL::text,
    p_analyze boolean DEFAULT false,
    p_job_id bigint DEFAULT NULL::bigint,
    p_debug boolean DEFAULT false)
  RETURNS void AS
$BODY$
declare
      parent_in alias for $1;
      child_in alias for $2;
      analyze_in alias for $3;
      job_in alias for $4;
      debug_in alias for $5;

      ex_context                      text;
      ex_detail                       text;
      ex_hint                         text;
      ex_message                      text;
      v_child_exists                  text;
      v_col                           text;
      v_constraint_cols               text[];
      v_constraint_col_type           text;
      v_constraint_name               text;
      v_constraint_values             record;
      v_control                       text;
      v_datetime_string               text;
      v_existing_constraint_name      text;
      v_job_id                        bigint;
      v_jobmon                        boolean;
      v_last_partition                text;
      v_last_partition_id             bigint; 
      v_last_partition_timestamp      timestamptz;
      v_max_id                        bigint;
      v_max_timestamp                 timestamptz;
      v_old_search_path               text;
      v_optimize_constraint           int;
      v_parent_schema                 text;
     
      v_partition_interval            text;
      v_partition_suffix              text;
      v_premake                       int;
      v_sql                           text;
      v_step_id                       bigint;
      v_suffix_position               int;
      v_type                          text;

      p_sch character varying := split_part(parent_in,'.',1);
      p_tab character varying:= split_part(parent_in,'.',2);
      c_sch character varying := split_part(child_in,'.',1);
      c_tab character varying := split_part(child_in,'.',2);
      
      component text := 'partman.apply_constraints';

begin
    select parent_table
         , partition_type
         , control
         , premake
         , partition_interval
         , optimize_constraint
         , datetime_string
         , constraint_cols
         , jobmon
      into p_tab
         , v_type
         , v_control
         , v_premake
         , v_partition_interval
         , v_optimize_constraint
         , v_datetime_string
         , v_constraint_cols
         , v_jobmon
     from partman.part_config
    where parent_table = parent_in
      and constraint_cols is not null;

    if v_constraint_cols is null 
    then
       if p_debug 
       then
          perform partman.notice(component, 'Given parent table ' || 
                                             parent_in || 
                                             ' is not set up for constraint management (constraint_cols is null)');
       end if;
       -- returns silently to allow this function to be simply called by maintenance processes without having to check if config options are set.
       return;
    end if;

     -- get/set search path
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    if p_job_id is null
    then
       v_job_id := add_job(format('partman create constraint: %s', parent_in));
    else
       v_job_id := p_job_id;
    end if;
    
    -- if p_child_table is null, figure out the partition that is the one right before the optimize_constraint value backwards.
    if p_child_table is null 
    then
       v_step_id := add_step(v_job_id, 'Applying additional constraints: Automatically determining most recent child on which to apply constraints');
    
       select partition_tablename 
         into v_last_partition 
         from partman.show_partitions(p_tab, 'DESC') LIMIT 1;

       if v_type in ('time', 'time-custom') 
       then
          select child_start_time 
            into v_last_partition_timestamp 
            from partman.show_partition_info(p_sch ||'.'|| v_last_partition, v_partition_interval, p_tab);
            
          v_partition_suffix := to_char(v_last_partition_timestamp - (v_partition_interval::interval * (v_optimize_constraint + v_premake + 1) ), v_datetime_string);
       elsif v_type = 'id' 
       then
          select child_start_id 
            into v_last_partition_id 
            from partman.show_partition_info(p_sch ||'.'|| v_last_partition, v_partition_interval, p_tab);
          v_partition_suffix := (v_last_partition_id - (v_partition_interval::int * (v_optimize_constraint + v_premake + 1) ))::text; 
       end if;

       c_tab := partman.check_name_length(p_tab, v_partition_suffix, TRUE);

       if p_debug 
       then
          perform partman.notice(component, 'apply constraint parent : ' || p_tab || 
                                            'suffix: ' || v_partition_suffix );
       end if;

       perform update_step(v_step_id, 'OK', format('Target child table: %s.%s', p_sch, c_tab));
    end if;
    
    v_step_id := add_step(v_job_id, 'Applying additional constraints: Checking if target child table exists');    
    v_child_exists := partman.table_exists( p_sch, c_tab);
    
    if v_child_exists is null 
    then
        perform update_step(v_step_id, 'notice', format('Target child table (%s) does not exist. Skipping constraint creation.', c_tab));
        perform close_job(v_job_id);
    
        if p_debug 
        then
           perform partman.notice(component,'target child ' || v_child_name || ' does not exist, skipping constraints');
        end if;
    
        perform partman.set_search(v_old_search_path);
        return;
    else
       perform update_step(v_step_id, 'OK', 'Done');
    end if;

    foreach v_col in array v_constraint_cols
    loop
       select con.conname
         into v_existing_constraint_name
         from pg_catalog.pg_constraint con
         join pg_class c on c.oid = con.conrelid
         join pg_catalog.pg_namespace n on c.relnamespace = n.oid
         join pg_catalog.pg_attribute a on con.conrelid = a.attrelid 
        where c.relname = c_tab::name
          and n.nspname = p_sch::name
          and con.conname LIKE 'partmanconstr_%'
          and con.contype = 'c' 
          and a.attname = v_col::name
          and array[a.attnum] operator(pg_catalog.<@) con.conkey
          and a.attisdropped = false;

       v_step_id := add_step(v_job_id, format('Applying additional constraints: Applying new constraint on column: %s', v_col));
 
       if v_existing_constraint_name is not null 
       then
          perform update_step(v_step_id, 'notice', format('Partman managed constraint already exists on this table (%s) and column (%s). Skipping creation.', c_tab, v_col));
          if p_debug 
          then
             perform partman.notice(component,'partman managed constraint already exists for ' || v_child_tablename || ' and column ' || v_col );
          end if;
          continue;
       end if;

       -- Ensure column name gets put on end of constraint name to help avoid naming conflicts 
       v_constraint_name := partman.check_name_length('partmanconstr_'||v_child_tablename, p_suffix := '_'||v_col);

       execute format('select min(%I)::text as min, max(%I)::text as max from %I.%I', v_col, v_col, p_sch, c_tab) into v_constraint_values;

       if v_constraint_values is not null 
       then
          v_sql := format('alter table %I.%I add constraint %I check (%I >= %L and %I <= %L)'
                            , p_sch
                            , c_tab
                            , v_constraint_name
                            , v_col
                            , v_constraint_values.min
                            , v_col
                            , v_constraint_values.max);
          if p_debug 
          then
             perform partman.notice(component,'constraint query: ' || v_sql );
          end if;
          execute v_sql;
          perform update_step(v_step_id, 'OK', format('New constraint created: %s', v_sql));
       else
          if p_debug 
          then
             perform partman.notice(component, 'column ' || v_col || ' contains all nulls, constraint ignored ');
          end if;
          perform update_step(v_step_id, 'notice', format('given column (%s) contains all nulls. No constraint created', v_col));
       end if;
    end loop;

    if p_analyze 
    then
       v_step_id := add_step(v_job_id, format('Applying additional constraints: Running analyze on partition set: %s', p_tab));
       if p_debug 
       then
          perform partman.notice(component,'run analyze on partition set ' || p_tab);
       end if;
       execute format('analyze %I.%I', p_sch, p_tab);
       perform update_step(v_step_id, 'OK', 'Done');
    end if;

    perform close_job(v_job_id);
    perform partman.set_search(v_old_search_path);
    
exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.apply_constraints(text, text, boolean, bigint, boolean)
  OWNER TO postgres;

