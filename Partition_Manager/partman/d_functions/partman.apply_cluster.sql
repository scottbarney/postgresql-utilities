CREATE OR REPLACE FUNCTION partman.apply_cluster(
    p_parent_schema text,
    p_parent_tablename text,
    p_child_schema text,
    p_child_tablename text)
  RETURNS void AS
$BODY$
declare
      v_old_search_path   text;
      v_parent_indexdef   text;
      v_row               record;
      v_sql               text;
begin
    -- get/set search path
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    with parent_info as (
         select c.oid as parent_oid 
           from pg_catalog.pg_class c
           join pg_catalog.pg_namespace n on c.relnamespace = n.oid
          where n.nspname = p_parent_schema::name
            and c.relname = p_parent_tablename::name
         )
         
     select substring(pg_get_indexdef(i.indexrelid) from ' using .*$') as index_def
       into v_parent_indexdef
       from pg_catalog.pg_index i
       join pg_catalog.pg_class c on i.indexrelid = c.oid
       join parent_info p on p.parent_oid = indrelid
      where i.indisclustered = true;

     -- loop over all existing indexes in child table to find one with matching definition
    for v_row in
        with child_info as (
             select c.oid as child_oid 
               from pg_catalog.pg_class c
               join pg_catalog.pg_namespace n on c.relnamespace = n.oid
              where n.nspname = p_child_schema::name
                and c.relname = p_child_tablename::name
        )        
        select substring(pg_get_indexdef(i.indexrelid) from ' using .*$') as child_indexdef
             , c.relname as child_indexname
          from pg_catalog.pg_index i
          join pg_catalog.pg_class c on i.indexrelid = c.oid
          join child_info p on p.child_oid = indrelid
    loop
       if v_row.child_indexdef = v_parent_indexdef 
       then
          v_sql = format('alter table %I.%I cluster on %I', p_child_schema, p_child_tablename, v_row.child_indexname);
          raise DEBUG '%', v_sql;
          execute v_sql;
       end if;
    end loop;

    perform partman.set_search(v_old_search_path);

end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.apply_cluster(text, text, text, text)
  OWNER TO postgres;

