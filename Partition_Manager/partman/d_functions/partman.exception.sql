CREATE OR REPLACE FUNCTION partman.exception(
    comp_in text,
    msg_in text)
  RETURNS void AS
$BODY$
declare
      c_in alias for $1;
      m_in alias for $2;
      stmt text;
begin
    raise exception '% -> %: %', current_timestamp, c_in, m_in;
    
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.exception(text, text)
  OWNER TO postgres;

