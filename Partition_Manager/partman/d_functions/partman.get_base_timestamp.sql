CREATE OR REPLACE FUNCTION partman.get_base_timestamp(
    interval_in interval,
    start_in timestamp with time zone)
  RETURNS timestamp with time zone AS
$BODY$
declare
      in_int alias for $1;
      in_st alias for $2;
      out_ts timestamptz;

begin
    if in_int >= '1 year' 
    then
       out_ts := date_trunc('year', in_st);       
       if in_int >= '10 years' 
       then
          out_ts := date_trunc('decade', in_st);
          if in_int >= '100 years'
          then
             out_ts := date_trunc('century', in_st);
             if in_int >= '1000 years'
             then
                out_ts := date_trunc('millennium', in_st);
             end if;
          end if;
       end if;
    end if;

    return out_ts;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.get_base_timestamp(interval, timestamp with time zone)
  OWNER TO postgres;

