CREATE OR REPLACE FUNCTION partman.check_partition_type(p_type text)
  RETURNS boolean AS
$BODY$
declare
v_result    boolean;
begin
    select p_type in ('time', 'time-custom', 'id') into v_result;
    return v_result;
end
$BODY$
  LANGUAGE plpgsql IMMUTABLE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.check_partition_type(text)
  OWNER TO postgres;

