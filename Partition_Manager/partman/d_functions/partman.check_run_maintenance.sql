CREATE OR REPLACE FUNCTION partman.check_run_maintenance(
    run_maint_in boolean,
    p_type_in character varying)
  RETURNS boolean AS
$BODY$
declare
      in_run alias for $1;
      in_type alias for $2;
      component text := 'partman.check_run_maintenance';

      out_run boolean;
begin
    if in_run is not null
    then
       if in_run is false and in_type in ('time','time-custom')
       then
          perform partman.exception( component, 'p_run_maintenance cannot be set to false for time based partitioning' );
       end if;
       out_run := in_run;
    elsif in_type in ('time','time-custom')
    then
       out_run := true;
    elsif in_type = 'id'
    then
       out_run := false;
    else
       perform partman.exception( component, 'use_run_maintenance cannot be null');
    end if;

    return out_run;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.check_run_maintenance(boolean, character varying)
  OWNER TO postgres;

