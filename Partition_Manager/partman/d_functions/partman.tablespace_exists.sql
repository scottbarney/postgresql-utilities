CREATE OR REPLACE FUNCTION partman.tablespace_exists(tablespace_in character varying)
  RETURNS boolean AS
$BODY$
declare 
      tbs_in alias for $1;
      cnt integer;
begin
    select count(*)
      into cnt
      from pg_tablespace
     where spcname = tbs_in;

     if cnt > 0
     then
        return true;
     else
        return false;
     end if;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.tablespace_exists(character varying)
  OWNER TO postgres;

