CREATE OR REPLACE FUNCTION partman.show_partition_info(
    IN p_child_table text,
    IN p_partition_interval text DEFAULT NULL::text,
    IN p_parent_table text DEFAULT NULL::text,
    OUT child_start_time timestamp with time zone,
    OUT child_end_time timestamp with time zone,
    OUT child_start_id bigint,
    OUT child_end_id bigint,
    OUT suffix text)
  RETURNS record AS
$BODY$
declare

      v_child_schema          text;
      v_child_tablename       text;
      v_datetime_string       text;
      v_old_search_path       text;
      v_parent_table          text;
      v_partition_interval    text;
      v_partition_type        text;
      v_quarter               text;
      v_suffix                text;
      v_suffix_position       int;
      v_year                  text;
      v_len                   integer;
      v_work                  text;
      component text := 'partman.show_partition_info';      
      c_sch character varying := split_part(p_child_table,'.',1);
      c_tab character varying:= split_part(p_child_table,'.',2);
      p_sch character varying;
      p_tab character varying;

begin
    -- get/set search path
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    if not partman.table_exists( c_sch, c_tab )
    then
        perform partman.set_search(v_old_search_path);
        perform partman.exception( component, 'child does not exist ' || p_child_table );
    end if;
     
    if p_parent_table is null 
    then
       v_parent_table := partman.get_parent(c_tab);       
    else
       v_parent_table := p_parent_table;
    end if;

    p_sch := split_part(v_parent_table,'.',1);
    p_tab := split_part(v_parent_table,'.',2);
    
    if p_partition_interval is null 
    then
       select partition_interval
            , partition_type
            , substring(datetime_string,2,length(datetime_string)) as datetime_string 
         into v_partition_interval
            , v_partition_type
            , v_datetime_string 
         from partman.part_config 
        where parent_table = v_parent_table;
     else
        v_partition_interval := p_partition_interval;
        select partition_type
             , substring(datetime_string,2,length(datetime_string)) as datetime_string 
          into v_partition_type
             , v_datetime_string 
          from partman.part_config 
         where parent_table = v_parent_table;
     end if;

     --raise notice 'date/time string %', v_datetime_string;
     
     if v_partition_type is null 
     then
        perform partman.exception( component, 'parent table of given child not managed by pg_partman: ' || v_parent_table);
     end if;

     v_len := length(p_tab);    
     v_suffix := substring(c_tab,v_len+2,length(c_tab));

     if v_partition_type in ('time','time-custom')
     then
        if v_partition_interval::interval <> '3 months' OR (v_partition_interval::interval = '3 months' and v_partition_type = 'time-custom') 
        then
           child_start_time := to_timestamp(v_suffix, v_datetime_string);
        else
            -- to_timestamp doesn't recognize 'Q' date string formater. Handle it
            v_year := split_part(v_suffix, 'q', 1);
            v_quarter := split_part(v_suffix, 'q', 2);
            
            case
            when v_quarter = '1' 
            then
               child_start_time := to_timestamp(v_year || '-01-01', 'YYYY-MM-DD');
            when v_quarter = '2' 
            then
               child_start_time := to_timestamp(v_year || '-04-01', 'YYYY-MM-DD');
            when v_quarter = '3' 
            then
               child_start_time := to_timestamp(v_year || '-07-01', 'YYYY-MM-DD');
            when v_quarter = '4' 
            then
               child_start_time := to_timestamp(v_year || '-10-01', 'YYYY-MM-DD');
            end case;
        end if;

        child_end_time := (child_start_time + v_partition_interval::interval) - '1 second'::interval;

    elsif v_partition_type = 'id' 
    then
       child_start_id := v_suffix::bigint;
       child_end_id := (child_start_id + v_partition_interval::bigint) - 1;
    else
       perform partman.exception( component, 'invalid partition type encountered in show_partition_info()');
    end if;

    suffix = v_suffix;
    perform partman.set_search(v_old_search_path);
return;
end
$BODY$
  LANGUAGE plpgsql STABLE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.show_partition_info(text, text, text)
  OWNER TO postgres;
