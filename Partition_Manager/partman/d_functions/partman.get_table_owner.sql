CREATE OR REPLACE FUNCTION partman.get_table_owner(
    schema_in character varying,
    table_in character varying)
  RETURNS character varying AS
$BODY$
declare
      in_sch alias for $1;
      in_tab alias for $2;
      out_owner character varying;
begin
    select tableowner
      into out_owner
      from pg_catalog.pg_tables
     where schemaname = in_sch
       and tablename = in_tab;

    return out_owner;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.get_table_owner(character varying, character varying)
  OWNER TO postgres;
