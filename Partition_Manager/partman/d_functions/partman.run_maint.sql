CREATE OR REPLACE FUNCTION partman.run_maint(
    parent_in text DEFAULT NULL::text,
    analyze_in boolean DEFAULT true,
    logger_in boolean DEFAULT true,
    debug_in boolean DEFAULT true)
  RETURNS void AS
$BODY$
declare
      p_in alias for $1;
      a_in alias for $2;
      l_in alias for $3;
      d_in alias for $4;

      v_old_search_path text;

      ex_context                      text;
      ex_detail                       text;
      ex_hint                         text;
      ex_message                      text;
      
      tab_sql text;
      tab_rec record;

      v_adv_lock                      boolean;
      v_check_subpart                 int;
      v_create_count                  int := 0;
      v_current_partition             text;
      v_current_partition_id          bigint;
      v_current_partition_timestamp   timestamptz;
      v_drop_count                    int := 0;
      v_job_id                        bigint;
      v_last_partition                text;
      v_last_partition_created        boolean;
      v_last_partition_id             bigint;
      v_last_partition_timestamp      timestamptz;
      v_max_id_parent                 bigint;
      v_max_time_parent               timestamptz;
      v_next_partition_id             bigint;
      v_next_partition_timestamp      timestamptz;
      v_parent_schema                 text;
      v_parent_tablename              text;
      v_partition_expression          text;
      v_premade_count                 int;
      v_premake_id_max                bigint;
      v_premake_id_min                bigint;
      v_premake_timestamp_min         timestamptz;
      v_premake_timestamp_max         timestamptz;
      v_row                           record;
      v_row_max_id                    record;
      v_row_max_time                  record;
      v_row_sub                       record;
      v_step_id                       bigint;
      v_step_overflow_id              bigint;
      v_sub_id_max                    bigint;
      v_sub_id_max_suffix             bigint;
      v_sub_id_min                    bigint;
      v_sub_parent                    text;
      v_sub_timestamp_max             timestamptz;
      v_sub_timestamp_max_suffix      timestamptz;
      v_sub_timestamp_min             timestamptz;
      v_tablename                     text;
      v_tables_list_sql               text;
      component text := 'partman.run_maint';      
      p_sch character varying := split_part(p_in,'.',1);
      p_tab character varying:= split_part(p_in,'.',2);
      dt text;

begin
    v_adv_lock := pg_try_advisory_xact_lock(hashtext('pg_partman run_maintenance'));
    
    if v_adv_lock = 'false' 
    then
       raise notice 'partman maintenance already running.';
       return;
    end if;
    -- get/set search path
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    v_job_id := add_job('partition management');
    v_step_id := add_step(v_job_id,'executing maintenance loop');

    tab_sql := 'select parent_table
                     , partition_type
                     , partition_interval
                     , control
                     , premake
                     , undo_in_progress
                     , sub_partition_set_full
                     , epoch
                     , infinite_time_partitions
                     , datetime_string
                  from partman.part_config
                 where sub_partition_set_full is false';

     if d_in is null
     then
        tab_sql := tab_sql || ' and use_run_maintenance is true';  
     else
        tab_sql := tab_sql || format(' and parent_table = %L', p_in);
     end if;

     if d_in
     then
        perform partman.notice(component, tab_sql);
     end if;
     
     for tab_rec in execute tab_sql
     loop
        continue when tab_rec.undo_in_progress;

        -- subparent consistency check
       select sub_parent 
         into v_sub_parent 
         from partman.part_config_sub 
        where sub_parent = tab_rec.parent_table;

       if v_sub_parent is not null 
       then
          select count(*) 
            into v_check_subpart 
            from partman.check_subpart_sameconfig(tab_rec.parent_table);
            
          if v_check_subpart > 1 
          then
            raise exception 'inconsistent data in part_config_sub table. Sub-partition tables that are themselves sub-partitions cannot have differing configuration values among their siblings. 
            Run this query: "select * from partman.check_subpart_sameconfig(''%'');" This should only return a single row or nothing. 
            if multiple rows are returned, results are all children of the given parent. Update the differing values to be consistent for your desired values.', tab_rec.sub_parent;
          end if;
       end if;
       
       v_partition_expression := case
                                 when tab_rec.epoch = true 
                                 then format('to_timestamp(%I)', tab_rec.control)
                                 else format('%I', tab_rec.control)
                                 end;
       if d_in 
       then
          raise notice 'run_maint: v_partition_expression: %', v_partition_expression;
       end if;

       select partition_tablename 
         into v_last_partition 
         from partman.show_partitions(tab_rec.parent_table, 'DESC') 
        limit 1;

       if d_in 
       then
          raise notice 'run_maint: parent_table: %, v_last_partition: %', tab_rec.parent_table, v_last_partition;
       end if;

       if tab_rec.partition_type in ('time','time-custom') 
       then
          dt := substring(tab_rec.datetime_string,2,length(tab_rec.datetime_string));

          raise notice 'p_sch: %', p_sch;
          raise notice 'v_last_part: %', v_last_partition;
          raise notice 'interval: %', tab_rec.partition_interval;
          raise notice 'parent: %', tab_rec.parent_table;
          
          select child_start_time 
            into v_last_partition_timestamp 
            from partman.show_partition_info(p_sch ||'.'|| v_last_partition, tab_rec.partition_interval, tab_rec.parent_table);

          raise notice 'v_last: %', v_last_partition_timestamp;
          
          for v_row_max_time 
          in
          select partition_schemaname
               , partition_tablename 
            from partman.show_partitions(tab_rec.parent_table, 'DESC')
          loop
             execute format('select max(%s)::text from %I.%I'
                           , v_partition_expression
                           , v_row_max_time.partition_schemaname
                           , v_row_max_time.partition_tablename
                           ) 
                into v_current_partition_timestamp;
                         
             if v_current_partition_timestamp is not null 
             then
                  select suffix_timestamp 
                    into v_current_partition_timestamp 
                    from partman.show_partition_name(tab_rec.parent_table, v_current_partition_timestamp::text);
                  exit;
             else
                v_current_partition_timestamp := v_last_partition_timestamp;
             end if;
          end loop;
        
          -- Check for values in the parent table. if they are there and greater than all child values, use that instead
          -- This allows maintenance to continue working properly if there is a large gap in data insertion. Data will remain in parent, but new tables will be created
          raise notice 'v_part_expression %', v_partition_expression;

          
          execute format('select max(%s) from only %I.%I', v_partition_expression, p_sch, p_tab) into v_max_time_parent;

          if v_max_time_parent is null
          then
             v_max_time_parent := v_current_partition_timestamp;
          end if;
          
          if d_in 
          then
             raise notice 'run_maint: v_current_partition_timestamp: %, v_max_time_parent: %', v_current_partition_timestamp, v_max_time_parent;
          end if;

          if v_max_time_parent > v_current_partition_timestamp 
          then
             select suffix_timestamp::timestamp(0) 
               into v_current_partition_timestamp 
               from partman.show_partition_name(tab_rec.parent_table, v_max_time_parent::text);
          end if;
        
          if v_current_partition_timestamp is null 
          then -- Partition set is completely empty
             if tab_rec.infinite_time_partitions  
             then
                -- Set it to now so new partitions continue to be created
                v_current_partition_timestamp := CURRENT_TIMESTAMP;
             else
                -- nothing to do
                continue;
             end if; 
          end if;
          
          -- if this is a subpartition, determine if the last child table has been made. if so, mark it as full so future maintenance runs can skip it
          select sub_min::timestamptz
               , sub_max::timestamptz 
            into v_sub_timestamp_min
               , v_sub_timestamp_max 
            from partman.check_subpartition_limits(tab_rec.parent_table, 'time');

          if v_sub_timestamp_max is not null 
          then
             select suffix_timestamp 
               into v_sub_timestamp_max_suffix 
               from partman.show_partition_name(tab_rec.parent_table, v_sub_timestamp_max::text);
               
             if v_sub_timestamp_max_suffix = v_last_partition_timestamp 
             then
                -- Final partition for this set is created. Set full and skip it
                update partman.part_config 
                   set sub_partition_set_full = true 
                 where parent_table = tab_rec.parent_table;
                continue;
            end if;
        end if;

        -- Check and see how many premade partitions there are.

        raise notice 'v_last_partition_timestamp: %', v_last_partition_timestamp;
        raise notice 'v_current_partition_timestamp: %', v_current_partition_timestamp;
        raise notice 'epoch %', round(extract('epoch' from age(v_last_partition_timestamp,v_current_partition_timestamp)));
        
        v_premade_count = round(EXTRACT('epoch' from age(v_last_partition_timestamp, v_current_partition_timestamp)) / EXTRACT('epoch' from tab_rec.partition_interval::interval));

        v_next_partition_timestamp := v_last_partition_timestamp;
        if d_in 
        then
            raise notice 'run_maint before loop: current_partition_timestamp: %, v_premade_count: %, v_sub_timestamp_min: %, v_sub_timestamp_max: %'
                , v_current_partition_timestamp 
                , v_premade_count
                , v_sub_timestamp_min
                , v_sub_timestamp_max;
        end if;
        -- loop premaking until config setting is met. Allows it to catch up if it fell behind or if premake changed
        while (v_premade_count < tab_rec.premake) 
        loop
            if d_in 
            then
                raise notice 'run_maint: parent_table: %, v_premade_count: %, v_next_partition_timestamp: %', tab_rec.parent_table, v_premade_count, v_next_partition_timestamp;
            end if;
            if v_next_partition_timestamp < v_sub_timestamp_min OR v_next_partition_timestamp > v_sub_timestamp_max 
            then
                -- with subpartitioning, no need to run if the timestamp is not in the parent table's range
                exit;
            end if;
            begin
                v_next_partition_timestamp := v_next_partition_timestamp + tab_rec.partition_interval::interval;
            exception 
                 when datetime_field_overflow 
                 then
                    v_premade_count := tab_rec.premake; -- do this so it can exit the premake check loop and continue in the outer for loop 
                    v_step_overflow_id := add_step(v_job_id, 'Attempted partition time interval is outside PostgreSQL''s supported time range.');
                    perform update_step(v_step_overflow_id, 'CRITICAL', format('Child partition creation skipped for parent table: %s', v_partition_time));
                   raise warning 'Attempted partition time interval is outside PostgreSQL''s supported time range. Child partition creation skipped for parent table %', tab_rec.parent_table;
                   continue;
            end;
            v_last_partition_created := partman.create_partition_time(tab_rec.parent_table, array[v_next_partition_timestamp], a_in); 

            if v_last_partition_created 
            then
               v_create_count := v_create_count + 1;
               perform partman.create_function_time(tab_rec.parent_table, v_job_id);
            end if;

            v_premade_count = round(EXTRACT('epoch' from age(v_next_partition_timestamp, v_current_partition_timestamp)) / EXTRACT('epoch' from tab_rec.partition_interval::interval));
        end loop;
                 
       elsif tab_rec.partition_type = 'id'
       then
          -- loop through child tables starting from highest to get current max value in partition set
          -- Avoids doing a scan on entire partition set and/or getting any values accidentally in parent.
          for v_row_max_id 
          in
          select partition_schemaname, partition_tablename from partman.show_partitions(tab_rec.parent_table, 'DESC')
          loop
             execute format('select max(%I)::text from %I.%I'
                           , tab_rec.control
                           , v_row_max_id.partition_schemaname
                           , v_row_max_id.partition_tablename) into v_current_partition_id;
             if v_current_partition_id is not null 
             then
                select suffix_id 
                  into v_current_partition_id 
                  from partman.show_partition_name(tab_rec.parent_table, v_current_partition_id::text);
                exit;
             end if;
          end loop;
          -- Check for values in the parent table. if they are there and greater than all child values, use that instead
          -- This allows maintenance to continue working properly if there is a large gap in data insertion. Data will remain in parent, but new tables will be created
          execute format('select max(%I) from onLY %I.%I', tab_rec.control, v_parent_schema, v_parent_tablename) into v_max_id_parent;

          if v_max_id_parent > v_current_partition_id 
          then
            select suffix_id 
              into v_current_partition_id 
              from partman.show_partition_name(tab_rec.parent_table, v_max_id_parent::text);
          end if;
          
          if v_current_partition_id is null 
          then
             -- Partition set is completely empty. nothing to do
             continue;
          end if;

          select child_start_id 
            into v_last_partition_id
            from partman.show_partition_info(v_parent_schema||'.'||v_last_partition, tab_rec.partition_interval, tab_rec.parent_table);

          -- Determine if this table is a child of a subpartition parent. if so, get limits to see if run_maintenance even needs to run for it.
          select sub_min::bigint
               , sub_max::bigint 
            into v_sub_id_min
               , v_sub_id_max 
            from partman.check_subpartition_limits(tab_rec.parent_table, 'id');

          if v_sub_id_max is not null 
          then
             select suffix_id 
               into v_sub_id_max_suffix 
               from partman.show_partition_name(tab_rec.parent_table, v_sub_id_max::text);

             if v_sub_id_max_suffix = v_last_partition_id 
             then
                -- Final partition for this set is created. Set full and skip it
                update partman.part_config 
                   set sub_partition_set_full = true where parent_table = tab_rec.parent_table;
                continue;
            end if;
          end if;

          v_next_partition_id := v_last_partition_id;
          v_premade_count := ((v_last_partition_id - v_current_partition_id) / tab_rec.partition_interval::bigint);
          -- loop premaking until config setting is met. Allows it to catch up if it fell behind or if premake changed.
          while (v_premade_count < tab_rec.premake) 
          loop 
             if d_in 
             then
                raise notice 'run_maint: parent_table: %, v_premade_count: %, v_next_partition_id: %', tab_rec.parent_table, v_premade_count, v_next_partition_id;
             end if;
             if v_next_partition_id < v_sub_id_min OR v_next_partition_id > v_sub_id_max 
             then
                -- with subpartitioning, no need to run if the id is not in the parent table's range
                exit;
             end if;
             v_next_partition_id := v_next_partition_id + tab_rec.partition_interval::bigint;
             v_last_partition_created := partman.create_partition_id(tab_rec.parent_table, array[v_next_partition_id], a_in);

             if v_last_partition_created 
             then
                v_create_count := v_create_count + 1;
                perform partman.create_function_id(tab_rec.parent_table, v_job_id);
             end if;
             v_premade_count := ((v_next_partition_id - v_current_partition_id) / tab_rec.partition_interval::bigint);
          end loop;

       end if; -- end main if check for time or id

     end loop; -- end of creation loop

     -- Manage dropping old partitions if retention option is set
     for v_row 
     in 
     select parent_table 
       from partman.part_config 
      where retention is not null 
        and undo_in_progress = false 
        and partition_type in ('time','time-custom')
     loop
        if p_parent_table is null 
        then
           v_drop_count := v_drop_count + partman.drop_partition_time(tab_rec.parent_table);   
        else -- only run retention on table given in parameter
           if p_parent_table <> tab_rec.parent_table 
           then
             continue;
           else
              v_drop_count := v_drop_count + partman.drop_partition_time(tab_rec.parent_table);   
           end if;
        end if;
       
        if v_drop_count > 0 
        then
           perform partman.create_function_time(tab_rec.parent_table, v_job_id);
        end if;
     end loop; 
    
     for v_row 
     in 
     select parent_table 
       from partman.part_config 
      where retention is not null 
        and undo_in_progress = false 
        and partition_type = 'id'
     loop
        if p_parent_table is null 
        then
           v_drop_count := v_drop_count + partman.drop_partition_id(tab_rec.parent_table);
        else -- only run retention on table given in parameter
           if p_parent_table <> tab_rec.parent_table 
           then
             continue;
           else
              v_drop_count := v_drop_count + partman.drop_partition_id(tab_rec.parent_table);
           end if;
        end if;
       
        if v_drop_count > 0 
        then
           perform partman.create_function_id(tab_rec.parent_table, v_job_id);
        end if;
     end loop; 

     perform update_step(v_step_id, 'OK', format('Partition maintenance finished. %s partitons made. %s partitions dropped.', v_create_count, v_drop_count));

     if v_step_overflow_id is not null 
     then
        perform fail_job(v_job_id);
     else
        perform close_job(v_job_id);
     end if;
    
     perform partman.set_search(v_old_search_path);
     return;    
/*exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );     */
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.run_maint(text, boolean, boolean, boolean)
  OWNER TO postgres;
