CREATE OR REPLACE FUNCTION partman.apply_foreign_keys(
    p_parent_table text,
    p_child_table text,
    p_job_id bigint DEFAULT NULL::bigint,
    p_debug boolean DEFAULT false)
  RETURNS void AS
$BODY$
declare
      parent_in alias for $1;
      child_in alias for $2;
      job_in alias for $3;
      debug_in alias for $4;


      ex_context          text;
      ex_detail           text;
      ex_hint             text;
      ex_message          text;
      v_count             int := 0;
      v_job_id            bigint;
      v_jobmon            text;
      v_jobmon_schema     text;
      v_old_search_path   text;

     v_ref_schema        text;
     v_ref_table         text;
     v_row               record;
     v_schemaname        text;
     v_sql               text;
     v_step_id           bigint;
     v_tablename         text;

      p_sch character varying := split_part(parent_in,'.',1);
      p_tab character varying:= split_part(parent_in,'.',2);
      c_sch character varying := split_part(child_in,'.',1);
      c_tab character varying := split_part(child_in,'.',2);
      component text := 'partman.apply_foreign_keys';

      p_exists boolean;
      c_exists boolean;

begin
    -- get/set search path
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path')); 
     
    if p_job_id is null 
    then
        v_job_id := add_job(format('parman apply foreign keys: %s', p_tab));
    else -- Don't create a new job, add steps into given job
        v_job_id := p_job_id;
    end if;

    v_step_id := add_step(v_job_id, format('apply foreign keys to %s if they exist on parent', c_tab ));

    p_exists := partman.table_exists(p_sch,p_tab);
    c_exists := partman.table_exists(c_sch,c_tab);
    

    if not c_exists
    then
       perform update_step(v_step_id, 'CRITICAL', format('target child table (%s) does not exist.', c_tab ));
       perform fail_job(v_job_id);
       perform partman.exception( v_job_id
                                , v_step_id
                                , component
                                , 'target child table ' || c_tab || ' does not exist'
                                , null);
       return;
    end if;

    for v_row in
        select pg_get_constraintdef(con.oid) as constraint_def 
         from pg_catalog.pg_constraint con
         join pg_catalog.pg_class c on con.conrelid = c.oid
         join pg_catalog.pg_namespace n on c.relnamespace = n.oid
        where c.relname = p_tab
          and n.nspname = p_sch
          and contype = 'f'
    loop
       v_sql := format('alter table %I.%I add %s'
                       , c_sch
                       , c_tab
                       , v_row.constraint_def);

       if p_debug 
       then
          raise notice 'constraint create query: %', v_sql;
       end if;

       execute v_sql;
       perform update_step(v_step_id, 'OK', 'FK applied');
       v_count := v_count + 1;

    end loop;

    if v_count = 0 
    then
       perform update_step(v_step_id, 'OK', 'no FKs found on parent');
    end if;
    perform close_job(v_job_id);
    perform partman.set_search(v_old_search_path);

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
