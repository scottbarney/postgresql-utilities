CREATE OR REPLACE FUNCTION partman.show_partitions(
    IN p_parent_table text,
    IN p_order text DEFAULT 'ASC'::text)
  RETURNS TABLE(partition_schemaname text, partition_tablename text) AS
$BODY$
declare

v_datetime_string       text;
v_new_search_path       text := 'partman,pg_temp';
v_old_search_path       text;
v_parent_schema         text;
v_parent_tablename      text;
v_partition_interval    text;
v_quarter               text;
v_type                  text;
v_year                  text;
s_stmt text;

begin

    -- get/set search path
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    if upper(p_order) not in ('ASC', 'DESC') 
    then
       perform partman.set_search(v_old_search_path);
       raise exception 'p_order paramter must be one of the following values: ASC, DESC';
    end if;

    select partition_type
         , partition_interval
         , datetime_string
      into v_type
         , v_partition_interval
         , v_datetime_string
      from partman.part_config
     where parent_table = p_parent_table;

     select schemaname, tablename into v_parent_schema, v_parent_tablename
       from pg_catalog.pg_tables
      where schemaname = split_part(p_parent_table, '.', 1)::name
        and tablename = split_part(p_parent_table, '.', 2)::name;

     if v_type in ('time', 'time-custom') 
     then

    if v_partition_interval::interval <> '3 months' OR (v_partition_interval::interval = '3 months' and v_type = 'time-custom') then
     --   return QUERY execute
     s_stmt := 
        format('select n.nspname::text as partition_schemaname, c.relname::text as partition_name from
        pg_catalog.pg_inherits h
        join pg_catalog.pg_class c on c.oid = h.inhrelid
        join pg_catalog.pg_namespace n on c.relnamespace = n.oid
        where h.inhparent = ''%I.%I''::regclass
        ORDER BY to_timestamp(substring(c.relname from ((length(c.relname) - position(''_'' in reverse(c.relname))) + 1) ), %L) %s'
            , v_parent_schema
            , v_parent_tablename
            , v_datetime_string
            , p_order);

      -- raise notice 'sql %', s_stmt;
       return query execute s_stmt;
    else
        -- for quarterly, to_timestamp() doesn't recognize "Q" in datetime string. 
        -- First order by just the year, then order by the quarter number (should be last character in table name)
        --return QUERY execute 
        s_stmt :=
        format('select n.nspname::text as partition_schemaname, c.relname::text as partition_name from
        pg_catalog.pg_inherits h
        join pg_catalog.pg_class c on c.oid = h.inhrelid
        join pg_catalog.pg_namespace n on c.relnamespace = n.oid
        where h.inhparent = ''%I.%I''::regclass
        ORDER BY to_timestamp(substring(c.relname from ((length(c.relname) - position(''_'' in reverse(c.relname))) + 2) for 4), ''YYYY'') %s
            , substring(reverse(c.relname) from 1 for 1) %s'
            , v_parent_schema
            , v_parent_tablename
            , p_order
            , p_order);

       -- raise notice 'sql %', s_stmt;
        return query execute s_stmt;
    end if;

ELSif v_type = 'id' then
    return QUERY execute 
    format('select n.nspname::text as partition_schemaname, c.relname::text as partition_name from
    pg_catalog.pg_inherits h
    join pg_catalog.pg_class c on c.oid = h.inhrelid
    join pg_catalog.pg_namespace n on c.relnamespace = n.oid
    where h.inhparent = ''%I.%I''::regclass
    ORDER BY substring(c.relname from ((length(c.relname) - position(''_'' in reverse(c.relname))) + 1) )::bigint %s'
        , v_parent_schema
        , v_parent_tablename
        , p_order);

end if;

perform partman.set_search(v_old_search_path);
end
$BODY$
  LANGUAGE plpgsql STABLE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION partman.show_partitions(text, text)
  OWNER TO postgres;
