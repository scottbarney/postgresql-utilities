CREATE OR REPLACE FUNCTION partman.drop_constraints(
    p_parent_table text,
    p_child_table text,
    p_debug boolean DEFAULT false)
  RETURNS void AS
$BODY$
declare

ex_context                      text;
ex_detail                       text;
ex_hint                         text;
ex_message                      text;
v_child_schemaname              text;
v_child_tablename               text;
v_col                           text;
v_constraint_cols               text[]; 
v_existing_constraint_name      text;
v_exists                        boolean := FALSE;
v_job_id                        bigint;
v_jobmon                        boolean;
v_jobmon_schema                 text;
v_old_search_path               text;
v_sql                           text;
v_step_id                       bigint;

component text := 'partman.drop_constraints';

begin

select constraint_cols 
    , jobmon
into v_constraint_cols 
    , v_jobmon
from partman.part_config 
where parent_table = p_parent_table;

if v_constraint_cols is null then
    raise EXCEPTIon 'Given parent table (%) not set up for constraint management (constraint_cols is null)', p_parent_table;
end if;

if v_jobmon then 
    select nspname into v_jobmon_schema from pg_catalog.pg_namespace n, pg_catalog.pg_extension e where e.extname = 'pg_jobmon' and e.extnamespace = n.oid;
    if v_jobmon_schema is not null then
        select current_setting('search_path') into v_old_search_path;
        execute format('select set_config(%L, %L, %L)', 'search_path', 'partman,'||v_jobmon_schema, 'false');
    end if;
end if;

select schemaname, tablename into v_child_schemaname, v_child_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_child_table, '.', 1)::name
and tablename = split_part(p_child_table, '.', 2)::name;
if v_child_tablename is null then
    raise EXCEPTIon 'Unable to find given child table in system catalogs: %', p_child_table;
end if;

if v_jobmon_schema is not null then
    v_job_id := add_job(format('PARTMAN DROP ConSTRAinT: %s', p_parent_table));
    v_step_id := add_step(v_job_id, 'Entering constraint drop loop');
    perform update_step(v_step_id, 'OK', 'Done');
end if;


foreach v_col in array v_constraint_cols
loop
    select con.conname
    into v_existing_constraint_name
    from pg_catalog.pg_constraint con
    join pg_class c on c.oid = con.conrelid
    join pg_catalog.pg_namespace n on c.relnamespace = n.oid
    join pg_catalog.pg_attribute a on con.conrelid = a.attrelid 
    where c.relname = v_child_tablename
        and n.nspname = v_child_schemaname
        and con.conname LIKE 'partmanconstr_%'
        and con.contype = 'c' 
        and a.attname = v_col
        and array[a.attnum] operator(pg_catalog.<@) con.conkey
        and a.attisdropped = false;

    if v_existing_constraint_name is not null then
        v_exists := TRUE;
        if v_jobmon_schema is not null then
            v_step_id := add_step(v_job_id, format('Dropping constraint on column: %s', v_col));
        end if;
        v_sql := format('ALTER TABLE %I.%I DROP ConSTRAinT %I', v_child_schemaname, v_child_tablename, v_existing_constraint_name);
        if p_debug then
            raise notice 'Constraint drop query: %', v_sql;
        end if;
        execute v_sql;
        if v_jobmon_schema is not null then
            perform update_step(v_step_id, 'OK', format('Drop constraint query: %s', v_sql));
        end if;
    end if;

end loop;

if v_jobmon_schema is not null and v_exists is FALSE then
    v_step_id := add_step(v_job_id, format('No constraints found to drop on child table: %s', p_child_table));
    perform update_step(v_step_id, 'OK', 'Done');
end if;

if v_jobmon_schema is not null then
    perform close_job(v_job_id);
    execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');
end if;

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.drop_constraints(text, text, boolean)
  OWNER TO postgres;

