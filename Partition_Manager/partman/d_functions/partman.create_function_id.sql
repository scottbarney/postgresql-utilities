CREATE OR REPLACE FUNCTION partman.create_function_id(
    p_parent_table text,
    p_job_id bigint DEFAULT NULL::bigint,
    p_analyze boolean DEFAULT true)
  RETURNS void AS
$BODY$
declare

ex_context                      text;
ex_detail                       text;
ex_hint                         text;
ex_message                      text;
v_control                       text;
v_count                         int;
v_current_partition_name        text;
v_current_partition_id          bigint;
v_datetime_string               text;
v_final_partition_id            bigint;
v_function_name                 text;
v_higher_parent_schema          text := split_part(p_parent_table, '.', 1);
v_higher_parent_table           text := split_part(p_parent_table, '.', 2);
v_id_position                   int;
v_job_id                        bigint;
v_jobmon                        text;
v_jobmon_schema                 text;
v_last_partition                text;
v_max                           bigint;
v_next_partition_id             bigint;
v_next_partition_name           text;
v_new_search_path               text := 'partman,pg_temp';
v_old_search_path               text;
v_optimize_trigger              int;
v_parent_schema                 text;
v_parent_tablename              text;
v_partition_interval            bigint;
v_premake                       int;
v_prev_partition_id             bigint;
v_prev_partition_name           text;
v_row_max_id                    record;
v_run_maint                     boolean;
v_step_id                       bigint;
v_top_parent                    text := p_parent_table;
v_trig_func                     text;
v_trigger_exception_handling    boolean;
v_trigger_return_null           boolean;
v_upsert                        text;

begin

select partition_interval::bigint
    , control
    , premake
    , optimize_trigger
    , use_run_maintenance
    , jobmon
    , trigger_exception_handling
    , upsert
    , trigger_return_null
into v_partition_interval
    , v_control
    , v_premake
    , v_optimize_trigger
    , v_run_maint
    , v_jobmon
    , v_trigger_exception_handling
    , v_upsert
    , v_trigger_return_null
from partman.part_config 
where parent_table = p_parent_table
and partition_type = 'id';

if not FOUND then
    raise EXCEPTIon 'ERROR: no config found for %', p_parent_table;
end if;

select current_setting('search_path') into v_old_search_path;
if v_jobmon then
    select nspname into v_jobmon_schema from pg_catalog.pg_namespace n, pg_catalog.pg_extension e where e.extname = 'pg_jobmon'::name and e.extnamespace = n.oid;
    if v_jobmon_schema is not null then
        v_new_search_path := 'partman,'||v_jobmon_schema||',pg_temp';
    end if;
end if;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

select partition_tablename into v_last_partition from partman.show_partitions(p_parent_table, 'DESC') LIMIT 1;

if v_jobmon_schema is not null then
    if p_job_id is null then
        v_job_id := add_job(format('PARTMAN CREATE FUNCTIon: %s', p_parent_table));
    else
        v_job_id = p_job_id;
    end if;
    v_step_id := add_step(v_job_id, format('Creating partition function for table %s', p_parent_table));
end if;

select schemaname, tablename into v_parent_schema, v_parent_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;
v_function_name := partman.check_name_length(v_parent_tablename, '_part_trig_func', FALSE);

-- Get the highest level top parent if multi-level partitioned in order to get proper max() value below
WHILE v_higher_parent_table is not null loop -- initially set in declare
    with top_oid as (
        select i.inhparent as top_parent_oid
        from pg_catalog.pg_inherits i
        join pg_catalog.pg_class c on c.oid = i.inhrelid
        join pg_catalog.pg_namespace n on n.oid = c.relnamespace
        where n.nspname = v_higher_parent_schema::name
        and c.relname = v_higher_parent_table::name
    ) select n.nspname, c.relname
    into v_higher_parent_schema, v_higher_parent_table
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n.oid = c.relnamespace
    join top_oid t on c.oid = t.top_parent_oid
    join partman.part_config p on p.parent_table = n.nspname||'.'||c.relname
    where p.partition_type = 'id';

    if v_higher_parent_table is not null then
        -- initially set in declare
        v_top_parent := v_higher_parent_schema||'.'||v_higher_parent_table;
    end if;

end loop;

-- loop through child tables starting from highest to get current max value in partition set
-- Avoids doing a scan on entire partition set and/or getting any values accidentally in parent.
for v_row_max_id in
    select partition_schemaname, partition_tablename from partman.show_partitions(v_top_parent, 'DESC')
loop
        execute format('select max(%I) from %I.%I', v_control, v_row_max_id.partition_schemaname, v_row_max_id.partition_tablename) into v_max;
        if v_max is not null then
            EXIT;
        end if;
end loop;
if v_max is null then
    v_max := 0;
end if;
v_current_partition_id = v_max - (v_max % v_partition_interval);
v_next_partition_id := v_current_partition_id + v_partition_interval;
v_current_partition_name := partman.check_name_length(v_parent_tablename, v_current_partition_id::text, TRUE);

v_trig_func := format('CREATE OR REPLACE FUNCTIon %I.%I() returns trigger language plpgsql as $t$ 
    declare
        v_count                     int;
        v_current_partition_id      bigint;
        v_current_partition_name    text;
        v_id_position               int;
        v_last_partition            text := %L;
        v_next_partition_id         bigint;
        v_next_partition_name       text;
        v_partition_created         boolean;
    begin
    if TG_OP = ''inSERT'' then 
        if NEW.%I >= %s and NEW.%I < %s then '
            , v_parent_schema
            , v_function_name
            , v_last_partition
            , v_control
            , v_current_partition_id
            , v_control
            , v_next_partition_id
        );
        select count(*) into v_count from pg_catalog.pg_tables where schemaname = v_parent_schema::name and tablename = v_current_partition_name::name;
        if v_count > 0 then
            v_trig_func := v_trig_func || format(' 
            inSERT into %I.%I VALUES (NEW.*) %s; ', v_parent_schema, v_current_partition_name, v_upsert);
        else
            v_trig_func := v_trig_func || '
            -- Child table for current values does not exist in this partition set, so write to parent
            return NEW;';
        end if;

    for i in 1..v_optimize_trigger loop
        v_prev_partition_id := v_current_partition_id - (v_partition_interval * i);
        v_next_partition_id := v_current_partition_id + (v_partition_interval * i);
        v_final_partition_id := v_next_partition_id + v_partition_interval;
        v_prev_partition_name := partman.check_name_length(v_parent_tablename, v_prev_partition_id::text, TRUE);
        v_next_partition_name := partman.check_name_length(v_parent_tablename, v_next_partition_id::text, TRUE);

        -- Check that child table exist before making a rule to insert to them.
        -- Handles optimize_trigger being larger than premake (to go back in time further) and edge case of changing optimize_trigger immediately after running create_parent().
        select count(*) into v_count from pg_catalog.pg_tables where schemaname = v_parent_schema::name and tablename = v_prev_partition_name::name;
        if v_count > 0 then
            -- only handle previous partitions if they're starting above zero
            if v_prev_partition_id >= 0 then
                v_trig_func := v_trig_func ||format('
        ELSif NEW.%I >= %s and NEW.%I < %s then 
            inSERT into %I.%I VALUES (NEW.*) %s; '
                , v_control
                , v_prev_partition_id
                , v_control
                , v_prev_partition_id + v_partition_interval
                , v_parent_schema
                , v_prev_partition_name
                , v_upsert
            );
            end if;
        end if;

        select count(*) into v_count from pg_catalog.pg_tables where schemaname = v_parent_schema::name and tablename = v_next_partition_name::name;
        if v_count > 0 then
            v_trig_func := v_trig_func ||format('
        ELSif NEW.%I >= %s and NEW.%I < %s then 
            inSERT into %I.%I VALUES (NEW.*) %s;'
                , v_control
                , v_next_partition_id
                , v_control
                , v_final_partition_id
                , v_parent_schema
                , v_next_partition_name
                , v_upsert
            );
        end if;
    end loop;
    v_trig_func := v_trig_func ||format('
        else
            v_current_partition_id := NEW.%I - (NEW.%I %% %s);
            v_current_partition_name := partman.check_name_length(%L, v_current_partition_id::text, TRUE);
            select count(*) into v_count from pg_catalog.pg_tables where schemaname = %L::name and tablename = v_current_partition_name::name;
            if v_count > 0 then 
                execute format(''inSERT into %%I.%%I VALUES($1.*) %s'', %L, v_current_partition_name) USinG NEW;
            else
                return NEW;
            end if;
        end if;'
            , v_control
            , v_control
            , v_partition_interval
            , v_parent_tablename
            , v_parent_schema
            , v_upsert
            , v_parent_schema
        );

    if v_run_maint is FALSE then
        v_trig_func := v_trig_func ||format('
        v_current_partition_id := NEW.%I - (NEW.%I %% %s);
        if (NEW.%I %% %s) > (%s / 2) then
            v_id_position := (length(v_last_partition) - position(''p_'' in reverse(v_last_partition))) + 2;
            v_next_partition_id := (substring(v_last_partition from v_id_position)::bigint) + %s;
            WHILE ((v_next_partition_id - v_current_partition_id) / %s) <= %s loop 
                v_partition_created := partman.create_partition_id(%L, array[v_next_partition_id], p_analyze := %L);
                if v_partition_created then
                    perform partman.create_function_id(%L, p_analyze := %L);
                    perform partman.apply_constraints(%L);
                end if;
                v_next_partition_id := v_next_partition_id + %s;
            end loop;
        end if;'
            , v_control
            , v_control
            , v_partition_interval
            , v_control
            , v_partition_interval
            , v_partition_interval
            , v_partition_interval
            , v_partition_interval
            , v_premake
            , p_parent_table
            , p_analyze
            , p_parent_table
            , p_analyze
            , p_parent_table
            , v_partition_interval
        );
    end if;

    v_trig_func := v_trig_func ||'
    end if;';

    if v_trigger_return_null is TRUE then
        v_trig_func := v_trig_func ||'
    return null;';
    else
        v_trig_func := v_trig_func ||'
    return NEW;';
    end if;

    if v_trigger_exception_handling then 
        v_trig_func := v_trig_func ||'
    EXCEPTIon WHEN OTHERS then
        raise WARNinG ''pg_partman insert into child table failed, row inserted into parent (%.%). ERROR: %'', TG_TABLE_SCHEMA, TG_TABLE_NAME, COALESCE(SQLERRM, ''unknown'');
        return NEW;';
    end if;

    v_trig_func := v_trig_func ||'
    end $t$;';

execute v_trig_func;

if v_jobmon_schema is not null then
    perform update_step(v_step_id, 'OK', format('Added function for current id interval: %s to %s', v_current_partition_id, v_final_partition_id-1));
end if;

if v_jobmon_schema is not null then
    perform close_job(v_job_id);
end if;

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

EXCEPTIon
    WHEN OTHERS then
        GET STACKED DIAGNOSTICS ex_message = MESSAGE_TEXT,
                                ex_context = PG_EXCEPTIon_ConTEXT,
                                ex_detail = PG_EXCEPTIon_DETAIL,
                                ex_hint = PG_EXCEPTIon_HinT;
        if v_jobmon_schema is not null then
            if v_job_id is null then
                execute format('select %I.add_job(''PARTMAN CREATE FUNCTIon: %s'')', v_jobmon_schema, p_parent_table) into v_job_id;
                execute format('select %I.add_step(%s, ''Partition function maintenance for table %s failed'')', v_jobmon_schema, v_job_id, p_parent_table) into v_step_id;
            ELSif v_step_id is null then
                execute format('select %I.add_step(%s, ''EXCEPTIon before first step logged'')', v_jobmon_schema, v_job_id) into v_step_id;
            end if;
            execute format('select %I.update_step(%s, ''CRITICAL'', %L)', v_jobmon_schema, v_step_id, 'ERROR: '||coalesce(SQLERRM,'unknown'));
            execute format('select %I.fail_job(%s)', v_jobmon_schema, v_job_id);
        end if;
        raise EXCEPTIon '%
ConTEXT: %
DETAIL: %
HinT: %', ex_message, ex_context, ex_detail, ex_hint;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.create_function_id(text, bigint, boolean)
  OWNER TO postgres;

