CREATE OR REPLACE FUNCTION partman.undo_partition_time(
    p_parent_table text,
    p_batch_count integer DEFAULT 1,
    p_batch_interval interval DEFAULT NULL::interval,
    p_keep_table boolean DEFAULT true,
    p_lock_wait numeric DEFAULT 0)
  RETURNS bigint AS
$BODY$
declare

ex_context              text;
ex_detail               text;
ex_hint                 text;
ex_message              text;
v_adv_lock              boolean;
v_batch_loop_count      int := 0;
v_child_loop_total      bigint := 0;
v_child_min             timestamptz;
v_child_table           text;
v_control               text;
v_epoch                 boolean;
v_function_name         text;
v_inner_loop_count      int;
v_lock_iter             int := 1;
v_lock_obtained         boolean := FALSE;
v_job_id                bigint;
v_jobmon                boolean;
v_jobmon_schema         text;
v_new_search_path       text := 'partman,pg_temp';
v_old_search_path       text;
v_parent_schema         text;
v_parent_tablename      text;
v_partition_expression  text;
v_partition_interval    interval;
v_row                   record;
v_rowcount              bigint;
v_step_id               bigint;
v_sub_count             int;
v_total                 bigint := 0;
v_trig_name             text;
v_type                  text;
v_undo_count            int := 0;

component text := 'partman.undo_partition_time';
begin

v_adv_lock := pg_try_advisory_xact_lock(hashtext('pg_partman undo_partition_time'));
if v_adv_lock = 'false' then
    raise notice 'undo_partition_time already running.';
    return 0;
end if;

select partition_type
    , partition_interval::interval
    , control
    , jobmon
    , epoch
into v_type
    , v_partition_interval
    , v_control
    , v_jobmon
    , v_epoch
from partman.part_config 
where parent_table = p_parent_table 
and (partition_type = 'time' OR partition_type = 'time-custom');

if v_partition_interval is null then
    raise EXCEPTIon 'Configuration for given parent table not found: %', p_parent_table;
end if;

select current_setting('search_path') into v_old_search_path;
if v_jobmon then
    select nspname into v_jobmon_schema from pg_catalog.pg_namespace n, pg_catalog.pg_extension e where e.extname = 'pg_jobmon'::name and e.extnamespace = n.oid;
    if v_jobmon_schema is not null then
        v_new_search_path := 'partman,'||v_jobmon_schema||',pg_temp';
    end if;
end if;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

-- Check if any child tables are themselves partitioned or part of an inheritance tree. Prevent undo at this level if so.
-- Need to either lock child tables at all levels or handle the proper removal of triggers on all child tables first 
--  before multi-level undo can be performed safely.
for v_row in 
    select partition_schemaname, partition_tablename from partman.show_partitions(p_parent_table)
loop
    select count(*) into v_sub_count
    from pg_catalog.pg_inherits i
    join pg_catalog.pg_class c on i.inhparent = c.oid
    join pg_catalog.pg_namespace n on c.relnamespace = n.oid
    where c.relname = v_row.partition_tablename::name
    and n.nspname = v_row.partition_schemaname::name;
    if v_sub_count > 0 then
        raise EXCEPTIon 'Child table for this parent has child table(s) itself (%). Run undo partitioning on this table or remove inheritance first to ensure all data is properly moved to parent', v_row.partition_schemaname||'.'||v_row.partition_tablename;
    end if;
end loop;

if v_jobmon_schema is not null then
    v_job_id := add_job(format('PARTMAN UNDO PARTITIoninG: %s', p_parent_table));
    v_step_id := add_step(v_job_id, format('Undoing partitioning for table %s', p_parent_table));
end if;

if p_batch_interval is null then
    p_batch_interval := v_partition_interval;
end if;

select schemaname, tablename 
into v_parent_schema, v_parent_tablename 
from pg_catalog.pg_tables 
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;

v_partition_expression := case
    when v_epoch = true then format('to_timestamp(%I)', v_control)
    else format('%I', v_control)
end;

-- Stops new time partitons from being made as well as stopping child tables from being dropped if they were configured with a retention period.
UPDATE partman.part_config SET undo_in_progress = true where parent_table = p_parent_table;
-- Stop data going into child tables.
v_trig_name := partman.check_name_length(p_object_name := v_parent_tablename, p_suffix := '_part_trig'); 
v_function_name := partman.check_name_length(v_parent_tablename, '_part_trig_func', FALSE);

select tgname into v_trig_name 
from pg_catalog.pg_trigger t
join pg_catalog.pg_class c on t.tgrelid = c.oid
where tgname = v_trig_name::name 
and c.relname = v_parent_tablename::name;

select proname into v_function_name from pg_catalog.pg_proc p join pg_catalog.pg_namespace n on p.pronamespace = n.oid where n.nspname = v_parent_schema::name and proname = v_function_name::name;

if v_trig_name is not null then
    -- lockwait for trigger drop
    if p_lock_wait > 0  then
        v_lock_iter := 0;
        WHILE v_lock_iter <= 5 loop
            v_lock_iter := v_lock_iter + 1;
            begin
                execute format('LOCK TABLE onLY %I.%I in ACCESS EXCLUSIVE MODE NOWAIT', v_parent_schema, v_parent_tablename);
                v_lock_obtained := TRUE;
            EXCEPTIon
                WHEN lock_not_available then
                    perform pg_sleep( p_lock_wait / 5.0 );
                    ConTinUE;
            end;
            EXIT WHEN v_lock_obtained;
        end loop;
        if not v_lock_obtained then
            raise notice 'Unable to obtain lock on parent table to remove trigger';
            return -1;
        end if;
    end if; -- end p_lock_wait if
    execute format('DROP TRIGGER if EXisTS %I on %I.%I', v_trig_name, v_parent_schema, v_parent_tablename);
end if; -- end trigger if
v_lock_obtained := FALSE; -- reset for reuse later

if v_function_name is not null then
    execute format('DROP FUNCTIon if EXisTS %I.%I()', v_parent_schema, v_function_name);
end if;

if v_jobmon_schema is not null then
    if (v_trig_name is not null OR v_function_name is not null) then
        perform update_step(v_step_id, 'OK', 'Stopped partition creation process. Removed trigger & trigger function');
    else
        perform update_step(v_step_id, 'OK', 'Stopped partition creation process.');
    end if;
end if;

<<outer_child_loop>>
loop
    -- Get ordered list of child table in set. Store in variable one at a time per loop until none are left or batch count is reached.
    -- This easily allows it to loop over same child table until empty or move onto next child table after it's dropped
    select partition_tablename into v_child_table from partman.show_partitions(p_parent_table, 'asC') LIMIT 1;

    EXIT outer_child_loop WHEN v_child_table is null;

    if v_jobmon_schema is not null then
        v_step_id := add_step(v_job_id, format('Removing child partition: %s.%s', v_parent_schema, v_child_table));
    end if;

    execute format('select min(%s) from %I.%I', v_partition_expression, v_parent_schema, v_child_table) into v_child_min;
    if v_child_min is null then
        -- No rows left in this child table. Remove from partition set.

        -- lockwait timeout for table drop
        if p_lock_wait > 0  then
            v_lock_iter := 0;
            WHILE v_lock_iter <= 5 loop
                v_lock_iter := v_lock_iter + 1;
                begin
                    execute format('LOCK TABLE onLY %I.%I in ACCESS EXCLUSIVE MODE NOWAIT', v_parent_schema, v_child_table);
                    v_lock_obtained := TRUE;
                EXCEPTIon
                    WHEN lock_not_available then
                        perform pg_sleep( p_lock_wait / 5.0 );
                        ConTinUE;
                end;
                EXIT WHEN v_lock_obtained;
            end loop;
            if not v_lock_obtained then
                raise notice 'Unable to obtain lock on child table for removal from partition set';
                return -1;
            end if;
        end if; -- end p_lock_wait if
        v_lock_obtained := FALSE; -- reset for reuse later

        execute format('ALTER TABLE %I.%I NO inHERIT %I.%I'
                        , v_parent_schema
                        , v_child_table
                        , v_parent_schema
                        , v_parent_tablename);
        if p_keep_table = false then
            execute format('DROP TABLE %I.%I', v_parent_schema, v_child_table);
            if v_jobmon_schema is not null then
                perform update_step(v_step_id, 'OK', format('Child table DROPPED. Moved %s rows to parent', v_child_loop_total));
            end if;
        else
            if v_jobmon_schema is not null then
                perform update_step(v_step_id, 'OK', format('Child table UNinHERITED, not DROPPED. Moved %s rows to parent', v_child_loop_total));
            end if;
        end if;
        if v_type = 'time-custom' then
            DELETE from partman.custom_time_partitions where parent_table = p_parent_table and child_table = v_parent_schema||'.'||v_child_table;
        end if;
        v_undo_count := v_undo_count + 1;
        EXIT outer_child_loop WHEN v_batch_loop_count >= p_batch_count; -- Exit outer for loop if p_batch_count is reached
        ConTinUE outer_child_loop; -- skip data moving steps below
    end if;
    v_inner_loop_count := 1;
    v_child_loop_total := 0;
    <<inner_child_loop>>
    loop
        -- do some locking with timeout, if required
        if p_lock_wait > 0  then
            v_lock_iter := 0;
            WHILE v_lock_iter <= 5 loop
                v_lock_iter := v_lock_iter + 1;
                begin
                    execute format('select * from %I.%I where %I <= %L for UPDATE NOWAIT'
                        , v_parent_schema
                        , v_child_table
                        , v_control
                        , v_child_min + (p_batch_interval * v_inner_loop_count));
                   v_lock_obtained := TRUE;
                EXCEPTIon
                    WHEN lock_not_available then
                        perform pg_sleep( p_lock_wait / 5.0 );
                        ConTinUE;
                end;
                EXIT WHEN v_lock_obtained;
            end loop;
            if not v_lock_obtained then
               raise notice 'Unable to obtain lock on batch of rows to move';
               return -1;
            end if;
        end if;

        -- Get everything from the current child minimum up to the multiples of the given interval
        execute format('with move_data as (
                                DELETE from %I.%I where %s <= %L returninG *)
                              inSERT into %I.%I select * from move_data'
            , v_parent_schema
            , v_child_table
            , v_partition_expression
            , v_child_min + (p_batch_interval * v_inner_loop_count)
            , v_parent_schema
            , v_parent_tablename);
        GET DIAGNOSTICS v_rowcount = ROW_COUNT;
        v_total := v_total + v_rowcount;
        v_child_loop_total := v_child_loop_total + v_rowcount;
        if v_jobmon_schema is not null then
            perform update_step(v_step_id, 'OK', format('Moved %s rows to parent.', v_child_loop_total));
        end if;
        EXIT inner_child_loop WHEN v_rowcount = 0; -- exit before loop incr if table is empty
        v_inner_loop_count := v_inner_loop_count + 1;
        v_batch_loop_count := v_batch_loop_count + 1;

        -- Check again if table is empty and go to outer loop again to drop it if so
        execute format('select min(%s) from %I.%I', v_partition_expression, v_parent_schema, v_child_table) into v_child_min;
        ConTinUE outer_child_loop WHEN v_child_min is null;

        EXIT outer_child_loop WHEN v_batch_loop_count >= p_batch_count; -- Exit outer for loop if p_batch_count is reached
    end loop inner_child_loop;
end loop outer_child_loop;

select partition_tablename into v_child_table from partman.show_partitions(p_parent_table, 'asC') LIMIT 1;
if v_child_table is null then
    DELETE from partman.part_config where parent_table = p_parent_table;
    if v_jobmon_schema is not null then
        v_step_id := add_step(v_job_id, 'Removing config from pg_partman');
        perform update_step(v_step_id, 'OK', 'Done');
    end if;
end if;

raise notice 'Copied % row(s) to the parent. Removed % partitions.', v_total, v_undo_count;
if v_jobmon_schema is not null then
    v_step_id := add_step(v_job_id, 'Final stats');
    perform update_step(v_step_id, 'OK', format('Copied %s row(s) to the parent. Removed %s partitions.', v_total, v_undo_count));
end if;

if v_jobmon_schema is not null then
    perform close_job(v_job_id);
end if;

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

return v_total;

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.undo_partition_time(text, integer, interval, boolean, numeric)
  OWNER TO postgres;

