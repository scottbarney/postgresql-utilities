CREATE OR REPLACE FUNCTION partman.exception(
    job_in bigint,
    step_in bigint,
    comp_in text,
    msg_in text,
    err_in text DEFAULT NULL::text)
  RETURNS void AS
$BODY$
declare
      j_in alias for $1;
      s_in alias for $2;
      c_in alias for $3;
      m_in alias for $4;
      e_in alias for $5;
      stmt text;
begin
    perform logger.update_step(s_in,'critical','error: ' || coalesce(e_in,m_in));
    perform logger.fail_job(j_in);
    raise exception '% -> %: %', current_timestamp, c_in, m_in;
    
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.exception(bigint, bigint, text, text, text)
  OWNER TO postgres;

