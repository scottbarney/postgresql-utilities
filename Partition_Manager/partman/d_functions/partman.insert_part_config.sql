CREATE OR REPLACE FUNCTION partman.insert_part_config(
    tab_in text,
    type_in text,
    int_in interval,
    epoch_in boolean,
    control_in text,
    premake_in integer,
    constraints_in text[],
    dt_in text,
    run_maint_in boolean,
    fks_in boolean,
    job_in boolean,
    up_in text,
    trig_null_in boolean,
    archive_tbs text,
    archive_rot integer)
  RETURNS integer AS
$BODY$
declare
      in_tab alias for $1;
      in_type alias for $2;
      in_int alias for $3;
      in_epoch alias for $4;
      in_control alias for $5;
      in_premake alias for $6;
      in_constraint alias for $7;
      in_dt alias for $8;
      in_run_maint alias for $9;
      in_fks alias for $10;
      in_job alias for $11;
      in_up alias for $12;
      in_trig_null alias for $13;
      in_archive_tbs alias for $14;
      in_archive_rotation alias for $15;

      component text := 'partman.insert_part_config';
begin
    begin
        insert into partman.part_config
             ( parent_table
             , partition_type
             , partition_interval
             , epoch
             , control
             , premake
             , constraint_cols
             , datetime_string
             , use_run_maintenance
             , inherit_fk
             , jobmon
             , upsert
             , trigger_return_null
             , archive_tablespace
             , archive_rotation
             )
        values
             ( in_tab
             , in_type
             , in_int
             , in_epoch
             , in_control
             , in_premake
             , in_constraint
             , in_dt
             , in_run_maint
             , in_fks
             , in_job
             , in_up
             , in_trig_null
             , in_archive_tbs
             , in_archive_rotation
             );

    exception
         when others
         then
            perform partman.notice( component, sqlerrm );
    end;         
    return 0;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.insert_part_config(text, text, interval, boolean, text, integer, text[], text, boolean, boolean, boolean, text, boolean, text, integer)
  OWNER TO postgres;

