CREATE OR REPLACE FUNCTION partman.check_name_length(
    p_object_name text,
    p_suffix text DEFAULT NULL::text,
    p_table_partition boolean DEFAULT false)
  RETURNS text AS
$BODY$
declare
    v_new_length    int;
    v_new_name      text;
begin

if p_table_partition is TRUE and (p_suffix is null) then
    raise EXCEPTIon 'Table partition name requires a suffix value';
end if;

if p_table_partition then  -- 61 characters to account for _p in partition name
    if char_length(p_object_name) + char_length(p_suffix) >= 61 then
        v_new_length := 61 - char_length(p_suffix);
        v_new_name := substring(p_object_name from 1 for v_new_length) ||  p_suffix; 
    else
        v_new_name := p_object_name||p_suffix;
    end if;
else
    if char_length(p_object_name) + char_length(COALESCE(p_suffix, '')) >= 63 then
        v_new_length := 63 - char_length(COALESCE(p_suffix, ''));
        v_new_name := substring(p_object_name from 1 for v_new_length) || COALESCE(p_suffix, ''); 
    else
        v_new_name := p_object_name||COALESCE(p_suffix, '');
    end if;
end if;

return v_new_name;

end
$BODY$
  LANGUAGE plpgsql IMMUTABLE
  COST 100;
ALTER FUNCTION partman.check_name_length(text, text, boolean)
  OWNER TO postgres;

