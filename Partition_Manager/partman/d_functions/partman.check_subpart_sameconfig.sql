CREATE OR REPLACE FUNCTION partman.check_subpart_sameconfig(IN p_parent_table text)
  RETURNS TABLE(sub_partition_type text, sub_control text, sub_partition_interval text, sub_constraint_cols text[], sub_premake integer, sub_inherit_fk boolean, sub_retention text, sub_retention_schema text, sub_retention_keep_table boolean, sub_retention_keep_index boolean, sub_use_run_maintenance boolean, sub_epoch boolean, sub_optimize_trigger integer, sub_optimize_constraint integer, sub_infinite_time_partitions boolean, sub_jobmon boolean, sub_trigger_exception_handling boolean, sub_upsert text, sub_trigger_return_null boolean) AS
$BODY$

    with parent_info as (
        select c1.oid
        from pg_catalog.pg_class c1 
        join pg_catalog.pg_namespace n1 on c1.relnamespace = n1.oid
        where n1.nspname = split_part(p_parent_table, '.', 1)::name
        and c1.relname = split_part(p_parent_table, '.', 2)::name
    )
    , child_tables as (
        select n.nspname||'.'||c.relname as tablename
        from pg_catalog.pg_inherits h
        join pg_catalog.pg_class c on c.oid = h.inhrelid
        join pg_catalog.pg_namespace n on c.relnamespace = n.oid
        join parent_info pi on h.inhparent = pi.oid
    )
    -- Column order here must match the returns TABLE definition
    select DisTinCT a.sub_partition_type
        , a.sub_control
        , a.sub_partition_interval
        , a.sub_constraint_cols
        , a.sub_premake
        , a.sub_inherit_fk
        , a.sub_retention
        , a.sub_retention_schema
        , a.sub_retention_keep_table
        , a.sub_retention_keep_index
        , a.sub_use_run_maintenance
        , a.sub_epoch
        , a.sub_optimize_trigger
        , a.sub_optimize_constraint
        , a.sub_infinite_time_partitions
        , a.sub_jobmon
        , a.sub_trigger_exception_handling
        , a.sub_upsert
        , a.sub_trigger_return_null
    from partman.part_config_sub a
    join child_tables b on a.sub_parent = b.tablename;
$BODY$
  LANGUAGE sql STABLE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION partman.check_subpart_sameconfig(text) SET search_path=partman, pg_temp;

ALTER FUNCTION partman.check_subpart_sameconfig(text)
  OWNER TO postgres;

