CREATE OR REPLACE FUNCTION partman.show_partition_name(
    IN p_parent_table text,
    IN p_value text,
    OUT partition_table text,
    OUT suffix_timestamp timestamp with time zone,
    OUT suffix_id bigint,
    OUT table_exists boolean)
  RETURNS record AS
$BODY$
declare

v_child_exists          text;
v_datetime_string       text;
v_max_range             timestamptz;
v_min_range             timestamptz;
v_parent_schema         text;
v_parent_tablename      text;
v_partition_interval    text;
v_type                  text;

begin

select partition_type 
    , partition_interval
    , substring(datetime_string,2,length(datetime_string)) as datetime_string 
into v_type
    , v_partition_interval
    , v_datetime_string 
from partman.part_config 
where parent_table = p_parent_table;

if v_type is null then
    raise EXCEPTIon 'Parent table given is not managed by pg_partman (%)', p_parent_table;
end if;

select schemaname, tablename into v_parent_schema, v_parent_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;
if v_parent_tablename is null then
    raise EXCEPTIon 'Parent table given does not exist (%)', p_parent_table;
end if;

if v_type = 'time' then
    CasE
        WHEN v_partition_interval::interval = '15 mins' then
            suffix_timestamp := date_trunc('hour', p_value::timestamptz) + 
                '15min'::interval * floor(date_part('minute', p_value::timestamptz) / 15.0);
        WHEN v_partition_interval::interval = '30 mins' then
            suffix_timestamp := date_trunc('hour', p_value::timestamptz) + 
                '30min'::interval * floor(date_part('minute', p_value::timestamptz) / 30.0);
        WHEN v_partition_interval::interval = '1 hour' then
            suffix_timestamp := date_trunc('hour', p_value::timestamptz);
        WHEN v_partition_interval::interval = '1 day' then
            suffix_timestamp := date_trunc('day', p_value::timestamptz);
        WHEN v_partition_interval::interval = '1 week' then
            suffix_timestamp := date_trunc('week', p_value::timestamptz);
        WHEN v_partition_interval::interval = '1 month' then
            suffix_timestamp := date_trunc('month', p_value::timestamptz);
        WHEN v_partition_interval::interval = '3 months' then
            suffix_timestamp := date_trunc('quarter', p_value::timestamptz);
        WHEN v_partition_interval::interval = '1 year' then
            suffix_timestamp := date_trunc('year', p_value::timestamptz);
    end CasE;
    partition_table := v_parent_schema||'.'||partman.check_name_length(v_parent_tablename, to_char(suffix_timestamp, v_datetime_string), TRUE);
ELSif v_type = 'id' then
    suffix_id := (p_value::bigint - (p_value::bigint % v_partition_interval::bigint));
    partition_table := v_parent_schema||'.'||partman.check_name_length(v_parent_tablename, suffix_id::text, TRUE);
ELSif v_type = 'time-custom' then

    select child_table, lower(partition_range) into partition_table, suffix_timestamp from partman.custom_time_partitions 
        where parent_table = p_parent_table and partition_range @> p_value::timestamptz;

    if partition_table is null then
        select max(upper(partition_range)) into v_max_range from partman.custom_time_partitions where parent_table = p_parent_table;
        select min(lower(partition_range)) into v_min_range from partman.custom_time_partitions where parent_table = p_parent_table;
        if p_value::timestamptz >= v_max_range then
            suffix_timestamp := v_max_range;
            loop
                -- Keep incrementing higher until given value is below the upper range
                suffix_timestamp := suffix_timestamp + v_partition_interval::interval;
                if p_value::timestamptz < suffix_timestamp then
                    -- Have to subtract one interval because the value would actually be in the partition previous 
                    --      to this partition timestamp since the partition names contain the lower boundary
                    suffix_timestamp := suffix_timestamp - v_partition_interval::interval;
                    EXIT;
                end if;
            end loop;
        ELSif p_value::timestamptz < v_min_range then
            suffix_timestamp := v_min_range;
            loop
                -- Keep decrementing lower until given value is below or equal to the lower range
                suffix_timestamp := suffix_timestamp - v_partition_interval::interval;
                if p_value::timestamptz >= suffix_timestamp then
                    EXIT;
                end if;
            end loop;
        else
            raise EXCEPTIon 'Unable to determine a valid child table for the given parent table and value';
        end if;

        partition_table := v_parent_schema||'.'||partman.check_name_length(v_parent_tablename, to_char(suffix_timestamp, v_datetime_string), TRUE);
    end if;
end if;

select tablename into v_child_exists
from pg_catalog.pg_tables
where schemaname = split_part(partition_table, '.', 1)::name
and tablename = split_part(partition_table, '.', 2)::name;

if v_child_exists is not null then
    table_exists := true;
else
    table_exists := false;
end if;

return;

end
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
ALTER FUNCTION partman.show_partition_name(text, text)
  OWNER TO postgres;
