CREATE OR REPLACE FUNCTION partman.get_base_timestamp(
    time_interval_in interval,
    req_interval_in text,
    start_in timestamp with time zone)
  RETURNS timestamp with time zone AS
$BODY$
declare
      t_in alias for $1;
      r_in alias for $2;
      s_in alias for $3;
      out_ts timestamptz;

begin
    
    if t_in < '1 year'
    then
       if r_in = 'quarterly'
       then
          out_ts := date_trunc('quarter',s_in);
     --     out_suffix := 'YYYY"q"Q';
       else
          out_ts := date_trunc('month',s_in);
     --     out_suffix := out_suffix || 'MM';
       end if;
       if t_in < '1 month'
       then 
          if r_in = 'weekly'
          then
             raise notice 'weekly';
             out_ts := date_trunc('week',s_in);
      --       out_suffix := 'IYYY"w"IW';
          else
             out_ts := date_trunc('day',s_in);
      --       out_suffix := out_suffix || 'DD';
             
          end if;
          if t_in < '1 day'
          then
             out_ts := date_trunc('hour',s_in);
      --       out_suffix := out_suffix || '_HH24MI';
             if t_in < '1 minute'
             then
                out_ts := date_trunc('minute',s_in);
      --          out_suffix := out_suffix || 'SS';
             end if;
          end if;
       end if;
    end if;
             raise notice 'out_ts: %', out_ts;
      --       raise notice 'out_suffix: %', out_suffix;
   return out_ts;   
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.get_base_timestamp(interval, text, timestamp with time zone)
  OWNER TO postgres;

