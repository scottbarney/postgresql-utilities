CREATE OR REPLACE FUNCTION partman.archive_table(
    parent_in text DEFAULT NULL::text,
    debug_in boolean DEFAULT false)
  RETURNS integer AS
$BODY$
declare
      in_parent alias for $1;
      in_debug alias for $2;
      
      s_stmt text;
      component text := 'partman.archive_table';
      o_search text;

      ex_context                      text;
      ex_detail                       text;
      ex_hint                         text;
      ex_message                      text;

      v_adv_lock                      boolean;
      job_id   bigint;
      step_id bigint;
      
      child_range timestamptz;
      child_work  text;
      len integer;
      current_range timestamptz;
      archive_before timestamptz;
      dt text;
      online_interval interval;
      auth text;
      status integer;

      aCur cursor( tab_in character varying )
      for
      select split_part(parent_table, '.', 1) as schema_name
           , split_part(parent_table, '.', 2) as table_name
           , parent_table
           , control
           , partition_interval
           , datetime_string
           , archive_tablespace
           , archive_rotation
        from partman.part_config
       where partition_type in ( 'time','time-custom')
         and parent_table = tab_in
         and archive_tablespace is not null;

     cCur cursor( parent_in character varying )
     for
     select n.nspname::text as child_schema
          , c.relname::text as child_table
       from pg_catalog.pg_inherits h
       join pg_catalog.pg_class c on c.oid = h.inhrelid
       join pg_catalog.pg_namespace n on c.relnamespace = n.oid
      where h.inhparent = parent_in::regclass;
      
     aRec record; 
     cRec record;
begin
    v_adv_lock := pg_try_advisory_xact_lock(hashtext(component));
    
    if v_adv_lock = 'false' 
    then
       perform partman.notice(component, 'already running');
    end if;
    
    -- get/set search path
    o_search := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    if in_parent is not null
    then
       job_id := add_job('partition archiving for: ' || in_parent);
       step_id := add_step(job_id,'executing archiving for: ' || in_parent );
    else

    end if;
    
    open aCur(in_parent);
    loop
       fetch aCur into aRec;
       exit when not found;

       dt := substring(aRec.datetime_string,2,length(aRec.datetime_string));
       online_interval := aRec.partition_interval::interval * aRec.archive_rotation;
       archive_before := now() - online_interval::interval;

       if in_debug
       then
          perform partman.notice(component, 'dt: ' || dt);
          perform partman.notice(component, 'online interval: ' || online_interval);
          perform partman.notice(component, 'archive before: ' || archive_before);
       end if;

       len := length(aRec.table_name);
          
       open cCur( aRec.parent_table);
       loop
          fetch cCur into cRec;
          exit when not found;
             
          -- decide if the partion is ready to be archived based on time
          child_work := substring(cRec.child_table,len+2,length(cRec.child_table));
          child_range := to_date(child_work, dt);

          if in_debug
          then
             perform partman.notice(component, 'child ts: ' || child_work);
             perform partman.notice(component, 'child_range: ' || child_range);
          end if;

          if child_range < archive_before
          then
             if partman.tablespace_exists(aRec.archive_tablespace)
             then
                status := partman.clone_archive_table( cRec.child_schema
                                                     , cRec.child_table
                                                     , aRec.schema_name
                                                     , aRec.table_name
                                                     , aRec.archive_tablespace
                                                     , job_id
                                                     , in_debug
                                                     );

                if status = 0
                then
                   -- validate result before cleanup
                   status := partman.validate_clone( cRec.child_schema
                                                   , cRec.child_table
                                                   , job_id
                                                   , in_debug
                                                   );
                   if status = 0
                   then
                      status := partman.archive_cleanup( cRec.child_schema
                                                       , cRec.child_table
                                                       , job_id
                                                       , in_debug
                                                       );
                   else
                      perform fail_job(job_id);
                   end if;
                else
                   perform fail_job(job_id);
                end if;
             else
                perform partman.notice(component, 'tablespace: ' || aRec.archive_tablespace || ' does not exist');
                perform partman.notice(component, 'skipping: ' || cRec.child_schema || '.' || cRec.child_table);
                perform partman.exception(job_id,step_id,component,'tablespace does not exist: ' || aRec.archive_tablespace,null);
             end if;
          end if;   
       end loop;
       close cCur;
                  
    end loop;
    close aCur;
    perform update_step(step_id,'OK','archive complete: ' || in_parent);
    
    perform logger.close_job(job_id);
    perform partman.set_search(o_search);
    return status;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.archive_table(text, boolean)
  OWNER TO postgres;

