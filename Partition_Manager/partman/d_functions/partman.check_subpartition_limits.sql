CREATE OR REPLACE FUNCTION partman.check_subpartition_limits(
    IN p_parent_table text,
    IN p_type text,
    OUT sub_min text,
    OUT sub_max text)
  RETURNS record AS
$BODY$
declare

v_datetime_string       text;
v_id_position           int;
v_parent_schema         text;
v_parent_tablename      text;
v_partition_interval    interval;
v_quarter               text;
v_sub_id_max            bigint;
v_sub_id_min            bigint;
v_sub_timestamp_max     timestamptz;
v_sub_timestamp_min     timestamptz;
v_time_position         int;
v_top_datetime_string   text;
v_top_interval          text;
v_top_parent            text;
v_top_type              text;
v_year                  text;

begin

select schemaname, tablename into v_parent_schema, v_parent_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;

-- CTE query is done individually for each type (time, id) because it should return null if the top parent is not the same type in a subpartition set (id->time or time->id)

if p_type = 'id' then

    with top_oid as (
        select i.inhparent as top_parent_oid
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits i on c.oid = i.inhrelid
        join pg_catalog.pg_namespace n on n.oid = c.relnamespace
        where n.nspname = v_parent_schema
        and c.relname = v_parent_tablename
    ) select n.nspname||'.'||c.relname, p.datetime_string, p.partition_interval, p.partition_type
      into v_top_parent, v_top_datetime_string, v_top_interval, v_top_type
      from pg_catalog.pg_class c
      join top_oid t on c.oid = t.top_parent_oid
      join pg_catalog.pg_namespace n on n.oid = c.relnamespace
      join partman.part_config p on p.parent_table = n.nspname||'.'||c.relname
      where c.oid = t.top_parent_oid
      and p.partition_type = 'id';

    if v_top_parent is not null then 
        select child_start_id::text, child_end_id::text 
        into sub_min, sub_max
        from partman.show_partition_info(p_parent_table, v_top_interval, v_top_parent);
    end if;

ELSif p_type = 'time' then

    with top_oid as (
        select i.inhparent as top_parent_oid
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits i on c.oid = i.inhrelid
        join pg_catalog.pg_namespace n on n.oid = c.relnamespace
        where n.nspname = v_parent_schema
        and c.relname = v_parent_tablename
    ) select n.nspname||'.'||c.relname, p.datetime_string, p.partition_interval, p.partition_type
      into v_top_parent, v_top_datetime_string, v_top_interval, v_top_type
      from pg_catalog.pg_class c
      join top_oid t on c.oid = t.top_parent_oid
      join pg_catalog.pg_namespace n on n.oid = c.relnamespace
      join partman.part_config p on p.parent_table = n.nspname||'.'||c.relname
      where c.oid = t.top_parent_oid
      and p.partition_type = 'time' OR p.partition_type = 'time-custom';

    if v_top_parent is not null then 
        select child_start_time::text, child_end_time::text 
        into sub_min, sub_max
        from partman.show_partition_info(p_parent_table, v_top_interval, v_top_parent);
    end if;

else
    raise EXCEPTIon 'invalid type given as parameter to check_subpartition_limits()';
end if;

return;

end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.check_subpartition_limits(text, text)
  OWNER TO postgres;

