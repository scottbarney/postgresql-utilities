CREATE OR REPLACE FUNCTION partman.lock_table(
    schema_in character varying,
    table_in character varying)
  RETURNS void AS
$BODY$
declare
      in_sch alias for $1;
      in_tab alias for $2;
begin
    execute format( 'lock table %I.%I in access exclusive mode', in_sch, in_tab );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.lock_table(character varying, character varying)
  OWNER TO postgres;

