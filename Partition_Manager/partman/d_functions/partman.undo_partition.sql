CREATE OR REPLACE FUNCTION partman.undo_partition(
    p_parent_table text,
    p_batch_count integer DEFAULT 1,
    p_keep_table boolean DEFAULT true,
    p_jobmon boolean DEFAULT true,
    p_lock_wait numeric DEFAULT 0)
  RETURNS bigint AS
$BODY$
declare

ex_context              text;
ex_detail               text;
ex_hint                 text;
ex_message              text;
v_adv_lock              boolean;
v_batch_loop_count      bigint := 0;
v_child_count           bigint;
v_child_table           text;
v_copy_sql              text;
v_function_name         text;
v_job_id                bigint;
v_jobmon_schema         text;
v_lock_iter             int := 1;
v_lock_obtained         boolean := FALSE;
v_new_search_path       text := 'partman,pg_temp';
v_old_search_path       text;
v_parent_schema         text;
v_parent_tablename      text;
v_partition_interval    interval;
v_rowcount              bigint;
v_step_id               bigint;
v_total                 bigint := 0;
v_trig_name             text;
v_type                  text;
v_undo_count            int := 0;

component text := 'partman.undo_partition';

begin

v_adv_lock := pg_try_advisory_xact_lock(hashtext('pg_partman undo_partition'));
if v_adv_lock = 'false' then
    raise notice 'undo_partition already running.';
    return 0;
end if;

select current_setting('search_path') into v_old_search_path;
if p_jobmon then
    select nspname into v_jobmon_schema from pg_catalog.pg_namespace n, pg_catalog.pg_extension e where e.extname = 'pg_jobmon'::name and e.extnamespace = n.oid;
    if v_jobmon_schema is not null then
        v_new_search_path := 'partman,'||v_jobmon_schema||',pg_temp';
    end if;
end if;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

if v_jobmon_schema is not null then
    v_job_id := add_job(format('PARTMAN UNDO PARTITIoninG: %s', p_parent_table));
    v_step_id := add_step(v_job_id, format('Undoing partitioning for table %s', p_parent_table));
end if;

select schemaname, tablename into v_parent_schema, v_parent_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;

-- Stops new time partitons from being made as well as stopping child tables from being dropped if they were configured with a retention period.
UPDATE partman.part_config SET undo_in_progress = true where parent_table = p_parent_table;

-- Stop data going into child tables and stop new id partitions from being made.
v_trig_name := partman.check_name_length(p_object_name := v_parent_tablename, p_suffix := '_part_trig'); 
v_function_name := partman.check_name_length(v_parent_tablename, '_part_trig_func', FALSE);

select tgname into v_trig_name 
from pg_catalog.pg_trigger t
join pg_catalog.pg_class c on t.tgrelid = c.oid
where tgname = v_trig_name::name
and c.relname = v_parent_tablename::name;

select proname into v_function_name from pg_catalog.pg_proc p join pg_catalog.pg_namespace n on p.pronamespace = n.oid where n.nspname = v_parent_schema::name and proname = v_function_name::name;

if v_trig_name is not null then
    -- lockwait for trigger drop
    if p_lock_wait > 0  then
        v_lock_iter := 0;
        WHILE v_lock_iter <= 5 loop
            v_lock_iter := v_lock_iter + 1;
            begin
                execute format('LOCK TABLE onLY %I.%I in ACCESS EXCLUSIVE MODE NOWAIT', v_parent_schema, v_parent_tablename);
                v_lock_obtained := TRUE;
            EXCEPTIon
                WHEN lock_not_available then
                    perform pg_sleep( p_lock_wait / 5.0 );
                    ConTinUE;
            end;
            EXIT WHEN v_lock_obtained;
        end loop;
        if not v_lock_obtained then
            raise notice 'Unable to obtain lock on parent table to remove trigger';
            return -1;
        end if;
    end if; -- end p_lock_wait if
    execute format('DROP TRIGGER if EXisTS %I on %I.%I', v_trig_name, v_parent_schema, v_parent_tablename);
end if; -- end trigger if
v_lock_obtained := FALSE; -- reset for reuse later

if v_function_name is not null then
    execute format('DROP FUNCTIon if EXisTS %I.%I()', v_parent_schema, v_function_name);
end if;

if v_jobmon_schema is not null then
    if (v_trig_name is not null OR v_function_name is not null) then
        perform update_step(v_step_id, 'OK', 'Stopped partition creation process. Removed trigger & trigger function');
    else
        perform update_step(v_step_id, 'OK', 'Stopped partition creation process.');
    end if;
end if;

WHILE v_batch_loop_count < p_batch_count loop 
    -- Get ordered list of child table in set. Store in variable one at a time per loop until none are left.
    -- not using show_partitions() so it can work on non-pg_partman partition sets
    with parent_info as (
        select c1.oid 
        from pg_catalog.pg_class c1 
        join pg_catalog.pg_namespace n1 on c1.relnamespace = n1.oid
        where c1.relname = v_parent_tablename::name
        and n1.nspname = v_parent_schema::name
    )
    select c.relname into v_child_table
    from pg_catalog.pg_inherits i
    join pg_catalog.pg_class c on i.inhrelid = c.oid
    join parent_info p on i.inhparent = p.oid
    ORDER BY i.inhrelid asC;

    EXIT WHEN v_child_table is null;

    if v_jobmon_schema is not null then
        v_step_id := add_step(v_job_id, format('Removing child partition: %s.%s', v_parent_schema, v_child_table));
    end if;

    -- lockwait timeout for table drop
    v_lock_obtained := FALSE; -- reset for reuse in loop
    if p_lock_wait > 0  then
        v_lock_iter := 0;
        WHILE v_lock_iter <= 5 loop
            v_lock_iter := v_lock_iter + 1;
            begin
                execute format('LOCK TABLE onLY %I.%I in ACCESS EXCLUSIVE MODE NOWAIT', v_parent_schema, v_child_table);
                v_lock_obtained := TRUE;
            EXCEPTIon
                WHEN lock_not_available then
                    perform pg_sleep( p_lock_wait / 5.0 );
                    ConTinUE;
            end;
            EXIT WHEN v_lock_obtained;
        end loop;
        if not v_lock_obtained then
            raise notice 'Unable to obtain lock on child table for removal from partition set';
            return -1;
        end if;
    end if; -- end p_lock_wait if

    v_copy_sql := format('inSERT into %I.%I select * from %I.%I'
                            , v_parent_schema
                            , v_parent_tablename
                            , v_parent_schema
                            , v_child_table);
    execute v_copy_sql;
    GET DIAGNOSTICS v_rowcount = ROW_COUNT;
    v_total := v_total + v_rowcount;

    execute format('ALTER TABLE %I.%I NO inHERIT %I.%I'
                    , v_parent_schema
                    , v_child_table
                    , v_parent_schema
                    , v_parent_tablename);
    if p_keep_table = false then
        execute format('DROP TABLE %I.%I', v_parent_schema, v_child_table);
        if v_jobmon_schema is not null then
            perform update_step(v_step_id, 'OK', format('Child table DROPPED. Moved %s rows to parent', v_rowcount));
        end if;
    else
        if v_jobmon_schema is not null then
            perform update_step(v_step_id, 'OK', format('Child table UNinHERITED, not DROPPED. Copied %s rows to parent', v_rowcount));
        end if;
    end if;

    select partition_type into v_type from partman.part_config where parent_table = p_parent_table;
    if v_type = 'time-custom' then
        DELETE from partman.custom_time_partitions where parent_table = p_parent_table and child_table = v_parent_schema||'.'||v_child_table;
    end if;

    v_batch_loop_count := v_batch_loop_count + 1;
    v_undo_count := v_undo_count + 1;
end loop; -- v_batch_loop_count

if v_undo_count = 0 then
    -- for loop never ran, so there's no child tables left.
    DELETE from partman.part_config where parent_table = p_parent_table;
    if v_jobmon_schema is not null then
        v_step_id := add_step(v_job_id, 'Removing config from pg_partman (if it existed)');
        perform update_step(v_step_id, 'OK', 'Done');
    end if;
end if;

raise notice 'Copied % row(s) from % child table(s) to the parent: %', v_total, v_undo_count, p_parent_table;
if v_jobmon_schema is not null then
    v_step_id := add_step(v_job_id, 'Final stats');
    perform update_step(v_step_id, 'OK', format('Copied %s row(s) from %s child table(s) to the parent', v_total, v_undo_count));
end if;

if v_jobmon_schema is not null then
    perform close_job(v_job_id);
end if;

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

return v_total;

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.undo_partition(text, integer, boolean, boolean, numeric)
  OWNER TO postgres;

