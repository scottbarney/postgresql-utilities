CREATE OR REPLACE FUNCTION partman.insert_part_config_sub(
    schema_in character varying,
    table_in character varying)
  RETURNS void AS
$BODY$
declare
      in_sch alias for $1;
      in_tab alias for $2;

      pRec record;
begin
    for pRec in
        with parent_table as ( select h.inhparent as parent_oid
                                 from pg_catalog.pg_inherits h
                                 join pg_catalog.pg_class c on h.inhrelid = c.oid
                                 join pg_catalog.pg_namespace n on c.relnamespace = n.oid
                                where c.relname = in_tab::name
                                  and n.nspname = in_sch::name
                             )
           , sibling_children as ( select i.inhrelid::regclass::text as tablename
                                     from pg_inherits i
                                     join parent_table p on i.inhparent = p.parent_oid
                                 )
                             select distinct sub_partition_type
                                  , sub_control
                                  , sub_partition_interval
                                  , sub_constraint_cols
                                  , sub_premake
                                  , sub_inherit_fk
                                  , sub_retention
                                  , sub_retention_schema
                                  , sub_retention_keep_table
                                  , sub_retention_keep_index
                                  , sub_use_run_maintenance
                                  , sub_epoch
                                  , sub_optimize_trigger
                                  , sub_infinite_time_partitions
                                  , sub_jobmon
                                  , sub_trigger_exception_handling
                                  , sub_upsert
                                  , sub_trigger_return_null
                               from partman.part_config_sub a
                               join sibling_children b on a.sub_parent = b.tablename limit 1
    loop
       insert into partman.part_config_sub
            ( sub_parent
            , sub_partition_type
            , sub_control
            , sub_partition_interval
            , sub_constraint_cols
            , sub_premake
            , sub_inherit_fk
            , sub_retention
            , sub_retention_schema
            , sub_retention_keep_table
            , sub_retention_keep_index
            , sub_use_run_maintenance
            , sub_epoch
            , sub_optimize_trigger
            , sub_optimize_constraint
            , sub_infinite_time_partitions
            , sub_jobmon
            , sub_trigger_exception_handling
            , sub_upsert
            , sub_trigger_return_null
            )
       values
            ( in_sch || '.' || in_tab
            , pRec.sub_partition_type
            , pRec.sub_control
            , pRec.sub_partition_interval
            , pRec.sub_constraint_cols
            , pRec.sub_premake
            , pRec.sub_inherit_fk
            , pRec.sub_retention
            , pRec.sub_retention_schema
            , pRec.sub_retention_keep_table
            , pRec.sub_retention_keep_index
            , pRec.sub_use_run_maintenance
            , pRec.sub_epoch
            , pRec.sub_optimize_trigger
            , pRec.sub_optimize_constraint
            , pRec.sub_infinite_time_partitions
            , pRec.sub_jobmon
            , pRec.sub_trigger_exception_handling
            , pRec.sub_upsert
            , pRec.sub_trigger_return_null
            );
    end loop;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.insert_part_config_sub(character varying, character varying)
  OWNER TO postgres;

