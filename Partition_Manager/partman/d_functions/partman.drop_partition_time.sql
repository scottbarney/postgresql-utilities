CREATE OR REPLACE FUNCTION partman.drop_partition_time(
    p_parent_table text,
    p_retention interval DEFAULT NULL::interval,
    p_keep_table boolean DEFAULT NULL::boolean,
    p_keep_index boolean DEFAULT NULL::boolean,
    p_retention_schema text DEFAULT NULL::text)
  RETURNS integer AS
$BODY$
declare

ex_context                  text;
ex_detail                   text;
ex_hint                     text;
ex_message                  text;
v_adv_lock                  boolean;
v_datetime_string           text;
v_drop_count                int := 0;
v_index                     record;
v_job_id                    bigint;
v_jobmon                    boolean;
v_jobmon_schema             text;
v_new_search_path           text := 'partman,pg_temp';
v_old_search_path           text;
v_parent_schema             text;
v_parent_tablename          text;
v_partition_interval        interval;
v_partition_timestamp       timestamptz;
v_retention                 interval;
v_retention_keep_index      boolean;
v_retention_keep_table      boolean;
v_retention_schema          text;
v_row                       record;
v_step_id                   bigint;
v_type                      text;

component text := 'partman.drop_partition_time';

begin

v_adv_lock := pg_try_advisory_xact_lock(hashtext('pg_partman drop_partition_time'));
if v_adv_lock = 'false' then
    raise notice 'drop_partition_time already running.';
    return 0;
end if;

-- Allow override of configuration options
if p_retention is null then
    select  
        partition_type
        , partition_interval::interval
        , retention::interval
        , retention_keep_table
        , retention_keep_index
        , datetime_string
        , retention_schema
        , jobmon
    into
        v_type
        , v_partition_interval
        , v_retention
        , v_retention_keep_table
        , v_retention_keep_index
        , v_datetime_string
        , v_retention_schema
        , v_jobmon
    from partman.part_config 
    where parent_table = p_parent_table 
    and (partition_type = 'time' OR partition_type = 'time-custom')
    and retention is not null;

    if v_partition_interval is null then
        raise EXCEPTIon 'Configuration for given parent table with a retention period not found: %', p_parent_table;
    end if;
else
    select  
        partition_type
        , partition_interval::interval
        , retention_keep_table
        , retention_keep_index
        , datetime_string
        , retention_schema
        , jobmon
    into
        v_type
        , v_partition_interval
        , v_retention_keep_table
        , v_retention_keep_index
        , v_datetime_string
        , v_retention_schema
        , v_jobmon
    from partman.part_config 
    where parent_table = p_parent_table 
    and (partition_type = 'time' OR partition_type = 'time-custom'); 
    v_retention := p_retention;

    if v_partition_interval is null then
        raise EXCEPTIon 'Configuration for given parent table not found: %', p_parent_table;
    end if;
end if;

select current_setting('search_path') into v_old_search_path;
if v_jobmon then
    select nspname into v_jobmon_schema from pg_catalog.pg_namespace n, pg_catalog.pg_extension e where e.extname = 'pg_jobmon'::name and e.extnamespace = n.oid;
    if v_jobmon_schema is not null then
        v_new_search_path := 'partman,'||v_jobmon_schema||',pg_temp';
    end if;
end if;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

if p_keep_table is not null then
    v_retention_keep_table = p_keep_table;
end if;
if p_keep_index is not null then
    v_retention_keep_index = p_keep_index;
end if;
if p_retention_schema is not null then
    v_retention_schema = p_retention_schema;
end if;

select schemaname, tablename into v_parent_schema, v_parent_tablename
from pg_catalog.pg_tables
where schemaname = split_part(p_parent_table, '.', 1)::name
and tablename = split_part(p_parent_table, '.', 2)::name;

-- loop through child tables of the given parent
for v_row in 
    select partition_schemaname, partition_tablename from partman.show_partitions(p_parent_table, 'DESC')
loop
    -- pull out datetime portion of partition's tablename to make the next one
     select child_start_time into v_partition_timestamp from partman.show_partition_info(v_row.partition_schemaname||'.'||v_row.partition_tablename
        , v_partition_interval::text
        , p_parent_table);

    -- Add one interval since partition names contain the start of the constraint period
    if v_retention < (CURRENT_TIMESTAMP - (v_partition_timestamp + v_partition_interval)) then
        -- only create a jobmon entry if there's actual retention work done
        if v_jobmon_schema is not null and v_job_id is null then
            v_job_id := add_job(format('PARTMAN DROP TIME PARTITIon: %s', p_parent_table));
        end if;

        if v_jobmon_schema is not null then
            v_step_id := add_step(v_job_id, format('Uninherit table %s.%s from %s'
                                                , v_row.partition_schemaname
                                                , v_row.partition_tablename
                                                , p_parent_table));
        end if;
        execute format('ALTER TABLE %I.%I NO inHERIT %I.%I'
                , v_row.partition_schemaname
                , v_row.partition_tablename
                , v_parent_schema
                , v_parent_tablename);
        if v_type = 'time-custom' then
            DELETE from partman.custom_time_partitions where parent_table = p_parent_table and child_table = v_row.partition_schemaname||'.'||v_row.partition_tablename;
        end if;
        if v_jobmon_schema is not null then
            perform update_step(v_step_id, 'OK', 'Done');
        end if;
        if v_retention_schema is null then
            if v_retention_keep_table = false then
                if v_jobmon_schema is not null then
                    v_step_id := add_step(v_job_id, format('Drop table %s.%s', v_row.partition_schemaname, v_row.partition_tablename));
                end if;
                execute format('DROP TABLE %I.%I CasCADE', v_row.partition_schemaname, v_row.partition_tablename);
                if v_jobmon_schema is not null then
                    perform update_step(v_step_id, 'OK', 'Done');
                end if;
            ELSif v_retention_keep_index = false then
                for v_index in 
                    with child_info as (
                        select c1.oid
                        from pg_catalog.pg_class c1
                        join pg_catalog.pg_namespace n1 on c1.relnamespace = n1.oid
                        where c1.relname = v_row.partition_tablename::name
                        and n1.nspname = v_row.partition_schemaname::name
                    )
                    select c.relname as name
                        , con.conname
                    from pg_catalog.pg_index i
                    join pg_catalog.pg_class c on i.indexrelid = c.oid
                    LEFT join pg_catalog.pg_constraint con on i.indexrelid = con.conindid
                    join child_info on i.indrelid = child_info.oid
                loop
                    if v_jobmon_schema is not null then
                        v_step_id := add_step(v_job_id, format('Drop index %s from %s.%s'
                                                            , v_index.name
                                                            , v_row.partition_schemaname
                                                            , v_row.partition_tablename));
                    end if;
                    if v_index.conname is not null then
                        execute format('ALTER TABLE %I.%I DROP ConSTRAinT %I'
                                        , v_row.partition_schemaname
                                        , v_row.partition_tablename
                                        , v_index.conname);
                    else
                        execute format('DROP inDEX %I.%I', v_parent_schema, v_index.name);
                    end if;
                    if v_jobmon_schema is not null then
                        perform update_step(v_step_id, 'OK', 'Done');
                    end if;
                end loop;
            end if;
        else -- Move to new schema
            if v_jobmon_schema is not null then
                v_step_id := add_step(v_job_id, format('Moving table %s.%s to schema %s'
                                                , v_row.partition_schemaname
                                                , v_row.partition_tablename
                                                , v_retention_schema));
            end if;

            execute format('ALTER TABLE %I.%I SET SCHEMA %I', v_row.partition_schemaname, v_row.partition_tablename, v_retention_schema);


            if v_jobmon_schema is not null then
                perform update_step(v_step_id, 'OK', 'Done');
            end if;
        end if; -- end retention schema if

        -- if child table is a subpartition, remove it from part_config & part_config_sub (should cascade due to FK)
        DELETE from partman.part_config where parent_table = v_row.partition_schemaname||'.'||v_row.partition_tablename;

        v_drop_count := v_drop_count + 1;
    end if; -- end retention check if

end loop; -- end child table loop

if v_jobmon_schema is not null then
    if v_job_id is not null then
        v_step_id := add_step(v_job_id, 'Finished partition drop maintenance');
        perform update_step(v_step_id, 'OK', format('%s partitions dropped.', v_drop_count));
        perform close_job(v_job_id);
    end if;
end if;

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

return v_drop_count;

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.drop_partition_time(text, interval, boolean, boolean, text)
  OWNER TO postgres;

