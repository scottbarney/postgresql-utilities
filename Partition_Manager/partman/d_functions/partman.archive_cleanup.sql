CREATE OR REPLACE FUNCTION partman.archive_cleanup(
    child_schema_in character varying,
    child_table_in character varying,
    job_id_in bigint DEFAULT 0,
    debug_in boolean DEFAULT false)
  RETURNS integer AS
$BODY$
declare
      c_sch alias for $1;
      c_tab alias for $2;
      in_job alias for $3;
      in_debug alias for $4;
      
      s_stmt text;
      component text := 'partman.archive_cleanup';
      o_search text;
      job_id bigint;
      step_id bigint;
      source_cnt bigint;
      clone_cnt bigint;
      
      archive_name character varying;
      orig_name character varying;
      noinherit_name character varying;
      parent_name character varying;
begin
    noinherit_name := c_sch || '.' || c_tab || '_o';

    if in_job != 0 and in_job is not null
    then
       job_id := in_job;
    else
       job_id := add_job(component || ' for: ' || orig_name);
    end if;

    step_id := add_step(job_id,component || ' cleaning: ' || noinherit_name);
       
    s_stmt := 'drop table ' || noinherit_name;

    if in_debug
    then
       perform partman.notice(component, 'cleanup sql: ' || s_stmt);
    end if;

    begin
        execute s_stmt;
        perform update_step(step_id,'OK','cleanup complete: ' || noinherit_name);
        
    exception
         when others
         then
            perform partman.exception(job_id, step_id, component,'cleanup failed: ' || noinherit_name, sqlerrm);
            return 1;
    end;
    return 0;
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.archive_cleanup(character varying, character varying, bigint, boolean)
  OWNER TO postgres;

