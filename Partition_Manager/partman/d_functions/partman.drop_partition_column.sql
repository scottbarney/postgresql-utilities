CREATE OR REPLACE FUNCTION partman.drop_partition_column(
    p_parent_table text,
    p_column text)
  RETURNS void AS
$BODY$
declare

v_child_oid         oid;
v_parent_oid        oid;
v_parent_schema     text;
v_parent_tablename  text;
v_row               record;

begin

select c.oid, n.nspname, c.relname into v_parent_oid, v_parent_schema, v_parent_tablename
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n.oid
where n.nspname = split_part(p_parent_table, '.', 1)::name
and c.relname = split_part(p_parent_table, '.', 2)::name;

if v_parent_oid is null then
    raise EXCEPTIon 'Given parent table does not exist: %', p_parent_table;
end if;

execute format('ALTER TABLE %I.%I DROP COLUMN if EXisTS %I', v_parent_schema, v_parent_tablename, p_column);

for v_row in 
    select n.nspname as child_schema, c.relname as child_table
    from pg_catalog.pg_inherits h
    join pg_catalog.pg_class c on h.inhrelid = c.oid
    join pg_catalog.pg_namespace n on c.relnamespace = n.oid
    where inhparent = v_parent_oid 
loop

    execute format('ALTER TABLE %I.%I DROP COLUMN if EXisTS %I', v_row.child_schema, v_row.child_table, p_column);

end loop;

end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.drop_partition_column(text, text)
  OWNER TO postgres;

