CREATE OR REPLACE FUNCTION partman.get_time_interval(
    type_in character varying,
    interval_in character varying)
  RETURNS text AS
$BODY$
declare
      in_typ alias for $1;
      in_int alias for $2;
      out_int text;
      component character varying := 'partman.get_time_interval';
begin
    case when in_int = 'yearly' then out_int := '1 year';
         when in_int = 'quarterly' then out_int := '3 months';
         when in_int = 'monthly' then out_int := '1 month';
         when in_int = 'weekly' then out_int := '1 week';
         when in_int = 'daily' then out_int := '1 day';
         when in_int = 'hourly' then out_int := '1 hour';
         when in_int = 'half-hour' then out_int := '30 mins';
         when in_int = 'quarter-hour' then out_int := '15 mins';
         else
            if in_type != 'time-custom'
            then
               perform partman.exception( component, 'must use a predefined time interval if not using "time-custom". see documentation');
            end if;
                       
            out_int := in_int::interval;
            
            if in_int < '1 second'::interval
            then
               perform partman.exception( component, 'interval must be >= 1 second');
            end if;
    end case;

    return out_int;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.get_time_interval(character varying, character varying)
  OWNER TO postgres;

