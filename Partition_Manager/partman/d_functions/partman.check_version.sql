CREATE OR REPLACE FUNCTION partman.check_version(p_check_version text)
  RETURNS boolean AS
$BODY$
declare

v_check_version     text[];
v_current_version   text[] := string_to_array(current_setting('server_version'), '.');
 
begin

v_check_version := string_to_array(p_check_version, '.');

if v_current_version[1]::int > v_check_version[1]::int then
    return true;
end if;
if v_current_version[1]::int = v_check_version[1]::int then
    if substring(v_current_version[2] from 'beta') is not null 
        OR substring(v_current_version[2] from 'alpha') is not null 
        OR substring(v_current_version[2] from 'rc') is not null 
    then
        -- You're running a test version. You're on your own if things fail.
        return true;
    end if;
    if v_current_version[2]::int > v_check_version[2]::int then
        return true;
    end if;
    if v_current_version[2]::int = v_check_version[2]::int then
        if v_current_version[3]::int >= v_check_version[3]::int then
            return true;
        end if; -- 0.0.x
    end if; -- 0.x.0
end if; -- x.0.0

return false;

end
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
ALTER FUNCTION partman.check_version(text)
  OWNER TO postgres;

