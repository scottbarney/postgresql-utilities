CREATE OR REPLACE FUNCTION partman.check_parent(p_exact_count boolean DEFAULT true)
  RETURNS SETOF partman.check_parent_table AS
$BODY$
declare

v_count             bigint = 0;
v_new_search_path   text := 'partman,pg_temp';
v_old_search_path   text;
v_row               record;
v_schemaname        text;
v_tablename         text;
v_sql               text;
v_trouble           partman.check_parent_table%rowtype;

begin

select current_setting('search_path') into v_old_search_path;
execute format('select set_config(%L, %L, %L)', 'search_path', v_new_search_path, 'false');

for v_row in 
    select parent_table from partman.part_config
loop
    select schemaname, tablename 
    into v_schemaname, v_tablename
    from pg_catalog.pg_tables 
    where schemaname = split_part(v_row.parent_table, '.', 1)::name
    and tablename = split_part(v_row.parent_table, '.', 2)::name;

    if p_exact_count then
        v_sql := format('select count(1) as n from onLY %I.%I', v_schemaname, v_tablename);
    else
        v_sql := format('select count(1) as n from (select 1 from onLY %I.%I LIMIT 1) x', v_schemaname, v_tablename);
    end if;
    execute v_sql into v_count;

    if v_count > 0 then 
        v_trouble.parent_table := v_schemaname ||'.'|| v_tablename;
        v_trouble.count := v_count;
        return NEXT v_trouble;
    end if;

    v_count := 0;

end loop;

execute format('select set_config(%L, %L, %L)', 'search_path', v_old_search_path, 'false');

return;

end
$BODY$
  LANGUAGE plpgsql STABLE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION partman.check_parent(boolean)
  OWNER TO postgres;

