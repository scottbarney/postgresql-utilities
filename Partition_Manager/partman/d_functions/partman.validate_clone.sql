CREATE OR REPLACE FUNCTION partman.validate_clone(
    child_schema_in character varying,
    child_table_in character varying,
    job_id_in bigint DEFAULT 0,
    debug_in boolean DEFAULT false)
  RETURNS integer AS
$BODY$
declare
      c_sch alias for $1;
      c_tab alias for $2;
      in_job alias for $3;
      in_debug alias for $4;
      
      s_stmt text;
      component text := 'partman.validate_clone';
      o_search text;
      job_id bigint;
      step_id bigint;
      source_cnt bigint;
      clone_cnt bigint;
      
      archive_name character varying;
      orig_name character varying;
      noinherit_name character varying;
      parent_name character varying;
begin
    orig_name := c_sch || '.' || c_tab;
    noinherit_name := c_sch || '.' || c_tab || '_o';

    if in_job != 0 and in_job is not null
    then
       job_id := in_job;
    else
       job_id := add_job(component || ' for: ' || orig_name);
    end if;

    step_id := add_step(job_id,component || ' clone count: ' || orig_name);
       
    s_stmt := 'select count(*) from ' || orig_name;

    if in_debug
    then
       perform partman.notice(component, 'clone count sql: ' || s_stmt);
    end if;

    begin
        execute s_stmt into clone_cnt;
        perform update_step(step_id,'OK','clone count for: ' || orig_name || ' ' || clone_cnt);
        
    exception
         when others
         then
            perform partman.exception(job_id, step_id, component,'clone count failed: ' || orig_name, sqlerrm);
            return 1;
    end;
    

    step_id := add_step(job_id,component || ' source count: ' || noinherit_name);
    
    s_stmt := 'select count(*) from ' || noinherit_name;

    if in_debug
    then
       perform partman.notice(component, 'source count sql: ' || s_stmt);
    end if;

    begin
        execute s_stmt into source_cnt;
        perform update_step(step_id,'OK','source count for: ' || noinherit_name || ' ' || source_cnt);
        
    exception
         when others
         then
            perform partman.exception(job_id, step_id, component,'clone count failed: ' || orig_name, sqlerrm);
            return 1;
    end;
    
    if clone_cnt = source_cnt
    then
       if in_job != 0 and in_job is not null
       then
          perform logger.close_job(job_id);
       end if;
       return 0;
    else
       if in_job != 0 and in_job is not null
       then
          perform logger.fail_job(job_id);
       end if;       
       return 1;
    end if;
    
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.validate_clone(character varying, character varying, bigint, boolean)
  OWNER TO postgres;

