CREATE OR REPLACE FUNCTION partman.apply_privilegesx(
    p_parent_schema text,
    p_parent_tablename text,
    p_child_schema text,
    p_child_tablename text,
    p_job_id bigint DEFAULT NULL::bigint)
  RETURNS void AS
$BODY$
declare

      ex_context          text;
      ex_detail           text;
      ex_hint             text;
      ex_message          text;
      v_all               text[] := array['select', 'insert', 'update', 'delete', 'truncate', 'references', 'trigger'];
      v_child_grant       record;
      v_child_owner       text;
      v_grantees          text[];
      v_job_id            bigint;
      v_jobmon            boolean;
      v_jobmon_schema     text;
      v_match             boolean;
      v_parent_grant      record;
      v_parent_owner      text;
      v_revoke            text;
      v_row_revoke        record;
      v_sql               text;
      v_step_id           bigint;

      component text := 'partman.apply_privileges';
begin
    if not partman.is_partman_managed( p_parent_schema, p_parent_tablename )
    then
       perform partman.exception( component, 
                                 'Given table is not managed by this extention: ' || p_parent_schema || '.' || p_parent_tablename );
     end if;

     v_parent_owner := partman.get_table_owner(p_parent_schema, p_parent_tablename);
     v_child_owner := partman.get_table_owner(p_child_schema, p_child_tablename);
     
     if v_parent_owner is null 
     then
        perform partman.exception( component, 
                                   'parent does not exist: ' || v_parent_schema || '.' || v_parent_tablename );
     end if;

     if v_child_owner is null
     then
        perform partman.exception( component, 
                                   'child does not exist: ' || v_child_schema || '.' || v_child_tablename ); 
     end if;

    if p_job_id is null 
    then
       v_job_id := partman.add_job( 'partman applying privs to child: ' || p_child_schema || '.' || p_child_tablename );
    else
        v_job_id := p_job_id;
    end if;
    v_step_id := partman.add_step( 'partman setting child privs: ' || p_child_schema || '.' || p_child_tablename );

    perform partman.update_step(v_step_id, 'pending', 'partman setting child privs: ' || p_child_schema || '.' || p_child_tablename ); 

    for v_parent_grant in 
        select array_agg(distinct privilege_type::text order by privilege_type::text) as types
             , grantee
          from partman.table_privs
         where table_schema = p_parent_schema::name and table_name = p_parent_tablename::name
         group by grantee 
    loop
       -- Compare parent & child grants. Don't re-apply if it already exists
       v_match := false;
       v_sql := null;
       for v_child_grant in 
           select array_agg(distinct privilege_type::text order by privilege_type::text) as types
                , grantee
             from partman.table_privs 
            where table_schema = p_child_schema::name and table_name = p_child_tablename::name
            group by grantee 
       loop
          if v_parent_grant.types = v_child_grant.types and v_parent_grant.grantee = v_child_grant.grantee 
          then
             v_match := true;
          end if;
       end loop;

       if v_match = false 
       then
          if v_parent_grant.grantee = 'PUBLIC' 
          then
             v_sql := 'grant %s on %I.%I TO %s';
          else
             v_sql := 'grant %s on %I.%I TO %I';
          end if;
        
          execute format(v_sql
                       , array_to_string(v_parent_grant.types, ',')
                       , p_child_schema
                       , p_child_tablename
                       , v_parent_grant.grantee);
          v_sql := null;
          select string_agg(r, ',') 
            into v_revoke 
            from (select unnest(v_all) as r 
          except
          select unnest(v_parent_grant.types)) x;
        
          if v_revoke is not null 
          then
              if v_parent_grant.grantee = 'PUBLIC' 
              then
                 v_sql := 'revoke %s on %I.%I from %s cascade';
              else
                 v_sql := 'revoke %s on %I.%I from %I cascade';
              end if;
            
              execute format(v_sql
                           , v_revoke
                           , p_child_schema
                           , p_child_tablename
                           , v_parent_grant.grantee);
              v_sql := null;
          end if;
       end if;

       v_grantees := array_append(v_grantees, v_parent_grant.grantee::text);

    end loop;

    -- Revoke all privileges from roles that have none on the parent
    if v_grantees is not null 
    then
       for v_row_revoke in 
           select role from (
              select distinct grantee::text as role from partman.table_privs where table_schema = p_child_schema::name and table_name = p_child_tablename::name
              except
              select unnest(v_grantees)) x
        loop
           if v_row_revoke.role is not null 
           then
              if v_row_revoke.role = 'PUBLIC' 
              then
                 v_sql := 'revoke all on %I.%I from %s';
              else
                 v_sql := 'revoke all on %I.%I from %I';
              end if;
              execute format(v_sql
                           , p_child_schema
                           , p_child_tablename
                           , v_row_revoke.role);
           end if;
        end loop;
    end if;

    if v_parent_owner != v_child_owner 
    then
       execute format('alter table %I.%I owner to %I'
                     , p_child_schema
                     , p_child_tablename
                     , v_parent_owner);
    end if;

    perform partman.update_step(v_step_id,'OK','Done');
    
    if p_job_id is null
    then 
       perform partman.close_job(v_job_id);
    end if;

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.apply_privilegesx(text, text, text, text, bigint)
  OWNER TO postgres;

