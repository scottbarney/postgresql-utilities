CREATE OR REPLACE FUNCTION partman.get_parent(child_in character varying)
  RETURNS text AS
$BODY$
declare
      in_c alias for $1;
      out_p character varying;
begin
    select n.nspname || '.' || c.relname
      into out_p
      from pg_catalog.pg_inherits h
      join pg_catalog.pg_class c on c.oid = h.inhparent
      join pg_catalog.pg_namespace n on c.relnamespace = n.oid
     where h.inhrelid::regclass = in_c::regclass;

     return out_p;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.get_parent(character varying)
  OWNER TO postgres;
