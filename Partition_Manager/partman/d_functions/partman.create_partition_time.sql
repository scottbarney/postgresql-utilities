CREATE OR REPLACE FUNCTION partman.create_partition_time(
    p_parent_table text,
    p_partition_times timestamp with time zone[],
    p_analyze boolean DEFAULT true,
    p_debug boolean DEFAULT false)
  RETURNS boolean AS
$BODY$
declare
      ex_context                      text;
      ex_detail                       text;
      ex_hint                         text;
      ex_message                      text;
      v_all                           text[] := array['select', 'insert', 'update', 'delete', 'truncate', 'references', 'trigger'];
      v_analyze                       boolean := true;
      v_control                       text;
      v_datetime_string               text;
      v_exists                        boolean;
      v_epoch                         boolean;
      v_grantees                      text[];
      v_hasoids                       boolean;
      v_inherit_fk                    boolean;
      v_job_id                        bigint;
      v_jobmon                        boolean;
      v_jobmon_schema                 text;
      v_old_search_path               text;
      v_parent_grant                  record;
      v_parent_owner                  text;
      v_partition_created             boolean := false;
      v_partition_name                text;
      v_partition_suffix              text;
      p_tablespace             text;
      v_partition_expression          text;
      v_partition_interval            interval;
      v_partition_timestamp_end       timestamptz;
      v_partition_timestamp_start     timestamptz;
      v_quarter                       text;
      v_revoke                        text;
      v_row                           record;
      v_sql                           text;
      v_step_id                       bigint;
      v_step_overflow_id              bigint;
      v_sub_timestamp_max             timestamptz;
      v_sub_timestamp_min             timestamptz;
      v_trunc_value                   text;
      v_time                          timestamptz;
      v_type                          text;
      v_unlogged                      char;
      v_year                          text;

     component text := 'partman.create_partition_time';
     p_sch character varying := split_part(p_parent_table, '.', 1)::name;
     p_tab character varying := split_part(p_parent_table, '.', 2)::name;
begin
    select partition_type
         , control
         , partition_interval
         , epoch
         , inherit_fk
         , jobmon
         , datetime_string
      into v_type
         , v_control
         , v_partition_interval
         , v_epoch
         , v_inherit_fk
         , v_jobmon
         , v_datetime_string
      from partman.part_config
     where parent_table = p_parent_table
       and partition_type in ( 'time','time-custom');

    if not found 
    then
       raise exception 'ERROR: no config found for %', p_parent_table;
    end if;

    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));


    -- Determine if this table is a child of a subpartition parent. if so, get limits of what child tables can be created based on parent suffix
    select sub_min::timestamptz
         , sub_max::timestamptz 
      into v_sub_timestamp_min
         , v_sub_timestamp_max 
     from partman.check_subpartition_limits(p_parent_table, 'time');

   select tableowner
        , schemaname
        , tablename
        , tablespace 
     into v_parent_owner
        , p_sch
        , p_tab
        , p_tablespace 
     from pg_catalog.pg_tables 
    where schemaname = p_sch::name
      and tablename = p_tab::name;

    v_job_id := add_job(format('partman create table: %s', p_parent_table));

    v_partition_expression := case
                              when v_epoch = true then format('to_timestamp(%I)', v_control)
                              else format('%I', v_control)
                              end;
    if p_debug 
    then
       raise notice 'create_partition_time: v_partition_expression: %', v_partition_expression;
    end if;

    foreach v_time in array p_partition_times 
    loop    
       v_partition_timestamp_start := v_time;
       begin
           v_partition_timestamp_end := v_time + v_partition_interval;
        
       exception 
            when datetime_field_overflow 
            then
               raise warning 'Attempted partition time interval is outside PostgreSQL''s supported time range. 
                             Child partition creation after time % skipped', v_time;
               v_step_overflow_id := add_step(v_job_id, 'Attempted partition time interval is outside PostgreSQL''s supported time range.');
               perform update_step(v_step_overflow_id, 'CRITICAL', 'Child partition creation after time '||v_time||' skipped');
               continue;
       end;

       -- Do not create the child table if it's outside the bounds of the top parent. 
       if v_sub_timestamp_min is not null 
       then
          if v_time < v_sub_timestamp_min OR v_time > v_sub_timestamp_max 
          then
             continue;
          end if;
       end if;
       
       -- This suffix generation code is in partition_data_time() as well
       v_partition_suffix := to_char(v_time, v_datetime_string);
       v_partition_name := partman.check_name_length(p_tab, v_partition_suffix, TRUE);
            
       if partman.table_exists( p_sch, v_partition_name )
       then
          continue;
       end if;

       v_step_id := add_step(v_job_id, format('Creating new partition %s.%s with interval from %s to %s'
                                             , p_sch
                                             , v_partition_name
                                             , v_partition_timestamp_start
                                             , v_partition_timestamp_end-'1sec'::interval));

       select relpersistence 
         into v_unlogged 
         from pg_catalog.pg_class c
         join pg_catalog.pg_namespace n on c.relnamespace = n.oid
        where c.relname = p_tab::name
          and n.nspname = p_sch::name;
          
       v_sql := 'CREATE';
       
       if v_unlogged = 'u' 
       then
          v_sql := v_sql || ' UNLOGGED';
       end if;
       v_sql := v_sql || format(' table %I.%I (like %I.%I including defaults including constraints including indexes including storage including comments)'
                                , p_sch
                                , v_partition_name
                                , p_sch
                                , p_tab);
                                
       select relhasoids 
         into v_hasoids 
         from pg_catalog.pg_class c
         join pg_catalog.pg_namespace n on c.relnamespace = n.oid
        where c.relname = p_tab::name
          and n.nspname = p_sch::name;
          
      if v_hasoids
      then
         v_sql := v_sql || ' with (OIDS)';
      end if;
      execute v_sql;
      
      if p_tablespace is not null 
      then
         execute format('alter table %I.%I set tablespace %I', p_sch, v_partition_name, p_tablespace);
      end if;
      
      execute format('alter table %I.%I add constraint %I check (%s >= %L and %4$s < %6$L)'
                    , p_sch
                    , v_partition_name
                    , v_partition_name||'_partition_check'
                    , v_partition_expression
                    , v_partition_timestamp_start
                    , v_partition_timestamp_end);
        
      if v_epoch
      then
         execute format('alter table %I.%I add constraint %I check (%I >= %L and %I < %L)'
                        , p_sch
                        , v_partition_name
                        , v_partition_name||'_partition_int_check'
                        , v_control
                        , EXTRACT('epoch' from v_partition_timestamp_start)
                        , v_control
                        , EXTRACT('epoch' from v_partition_timestamp_end) );
      end if;

      execute format('alter table %I.%I inherit %I.%I'
                    , p_sch
                    , v_partition_name
                    , p_sch
                    , p_tab);

      -- if custom time, set extra config options.
      if v_type = 'time-custom' 
      then
         insert into partman.custom_time_partitions (parent_table, child_table, partition_range)
         values ( p_parent_table, p_sch||'.'||v_partition_name, tstzrange(v_partition_timestamp_start, v_partition_timestamp_end, '[)') );
      end if;

      perform partman.apply_privileges(p_sch, p_tab, p_sch, v_partition_name, v_job_id);

      perform partman.apply_cluster(p_sch, p_tab, p_sch, v_partition_name);

      if v_inherit_fk 
      then
         perform partman.apply_foreign_keys(p_parent_table, p_sch||'.'||v_partition_name, v_job_id);
      end if;

      perform update_step(v_step_id, 'OK', 'Done');
 
      -- Will only loop once and only if sub_partitioning is actually configured
      -- This seemed easier than assigning a bunch of variables then doing an if condition
      for v_row in 
          select sub_parent
               , sub_partition_type
               , sub_control
               , sub_partition_interval
               , sub_constraint_cols
               , sub_premake
               , sub_optimize_trigger
               , sub_optimize_constraint
               , sub_epoch
               , sub_inherit_fk
               , sub_retention
               , sub_retention_schema
               , sub_retention_keep_table
               , sub_retention_keep_index
               , sub_use_run_maintenance
               , sub_infinite_time_partitions
               , sub_jobmon
               , sub_trigger_exception_handling
            from partman.part_config_sub
           where sub_parent = p_parent_table
      loop
         v_step_id := add_step(v_job_id, format('Subpartitioning %s.%s', p_sch, v_partition_name));
         v_sql := format('select partman.create_parent(
                          p_parent_table := %L
                        , p_control := %L
                        , p_type := %L
                        , p_interval := %L
                        , p_constraint_cols := %L
                        , p_premake := %L
                        , p_use_run_maintenance := %L
                        , p_inherit_fk := %L
                        , p_epoch := %L
                        , p_jobmon := %L )'
                        , p_sch||'.'||v_partition_name
                        , v_row.sub_control
                        , v_row.sub_partition_type
                        , v_row.sub_partition_interval
                        , v_row.sub_constraint_cols
                        , v_row.sub_premake
                        , v_row.sub_use_run_maintenance
                        , v_row.sub_inherit_fk
                        , v_row.sub_epoch
                        , v_row.sub_jobmon);
         execute v_sql;

         update partman.part_config 
            set retention_schema = v_row.sub_retention_schema
              , retention_keep_table = v_row.sub_retention_keep_table
              , retention_keep_index = v_row.sub_retention_keep_index
              , optimize_trigger = v_row.sub_optimize_trigger
              , optimize_constraint = v_row.sub_optimize_constraint
              , infinite_time_partitions = v_row.sub_infinite_time_partitions
              , trigger_exception_handling = v_row.sub_trigger_exception_handling
          where parent_table = p_sch||'.'||v_partition_name;

      end loop; -- end sub partitioning loop

      -- Manage additonal constraints if set
      perform partman.apply_constraints(p_parent_table, p_job_id := v_job_id, p_debug := p_debug);
      v_partition_created := true;

    end loop;

    -- v_analyze is a local check if a new table is made.
    -- p_analyze is a parameter to say whether to run the analyze at all. Used by create_parent() to avoid long exclusive lock or run_maintenence() to avoid long creation runs.
    if v_analyze and p_analyze 
    then
        v_step_id := add_step(v_job_id, format('analyzing partition set: %s', p_parent_table));
        execute format('analyze %I.%I', p_sch, p_tab);
        perform update_step(v_step_id, 'OK', 'Done');
    end if;

    if not v_partition_created
    then
       v_step_id := add_step(v_job_id, format('No partitions created for partition set: %s. Attempted intervals: %s', p_parent_table, p_partition_times));
       perform update_step(v_step_id, 'OK', 'Done');
    end if;

    if v_step_overflow_id is not null 
    then
       perform fail_job(v_job_id);
    else
       perform close_job(v_job_id);
    end if;

    perform partman.set_search(v_old_search_path);
    return v_partition_created;


exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.create_partition_time(text, timestamp with time zone[], boolean, boolean)
  OWNER TO postgres;

