CREATE OR REPLACE FUNCTION partman.notice(
    comp_in text,
    msg_in text)
  RETURNS void AS
$BODY$
declare
      c_in alias for $1;
      m_in alias for $2;
begin
    raise notice '% -> %', current_timestamp, c_in;
    raise notice '---';
    raise notice '%', m_in;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.notice(text, text)
  OWNER TO postgres;

