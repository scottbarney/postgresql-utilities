CREATE OR REPLACE FUNCTION partman.set_search(search_path_in character varying)
  RETURNS void AS
$BODY$
declare
      in_search alias for $1;
begin
    execute format('select set_config(%L, %L, %L)', 'search_path', in_search, 'false');
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.set_search(character varying)
  OWNER TO postgres;

