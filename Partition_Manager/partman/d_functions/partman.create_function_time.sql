CREATE OR REPLACE FUNCTION partman.create_function_time(
    p_parent_table text,
    p_job_id bigint DEFAULT NULL::bigint)
  RETURNS void AS
$BODY$
declare

ex_context                      text;
ex_detail                       text;
ex_hint                         text;
ex_message                      text;
v_control                       text;
v_count                         int;
v_current_partition_name        text;
v_current_partition_timestamp   timestamptz;
v_datetime_string               text;
v_epoch                         boolean;
v_final_partition_timestamp     timestamptz;
v_function_name                 text;
v_job_id                        bigint;
v_jobmon                        boolean;
v_jobmon_schema                 text;
v_old_search_path               text;
v_new_length                    int;
v_next_partition_name           text;
v_next_partition_timestamp      timestamptz;
v_parent_schema                 text;
v_parent_tablename              text;
v_partition_expression          text;
v_partition_interval            interval;
v_prev_partition_name           text;
v_prev_partition_timestamp      timestamptz;
v_step_id                       bigint;
v_trig_func                     text;
v_optimize_trigger              int;
v_trigger_exception_handling    boolean;
v_trigger_return_null           boolean;
v_type                          text;
v_upsert                        text;
component text := 'partman.create_function_time';

begin

     select partition_type
          , partition_interval::interval
          , epoch
          , control
          , optimize_trigger
          , datetime_string
          , jobmon
          , trigger_exception_handling
          , upsert
          , trigger_return_null
       into v_type
          , v_partition_interval
          , v_epoch
          , v_control
          , v_optimize_trigger
          , v_datetime_string
          , v_jobmon
          , v_trigger_exception_handling
          , v_upsert
          , v_trigger_return_null
       from partman.part_config 
      where parent_table = p_parent_table
        and (partition_type in ('time','time-custom'));

     if not found 
     then
        raise exception 'ERROR: no config found for %', p_parent_table;
     end if;

    -- get/set search path
    v_old_search_path := partman.get_search();
    perform partman.set_search(partman.get_parameter('search_path'));

    if p_job_id is null 
    then
        v_job_id := add_job(format('partman create function: %s', p_parent_table));
    else
        v_job_id = p_job_id;
    end if;
    v_step_id := add_step(v_job_id, format('Creating partition function for table %s', p_parent_table));


    select schemaname
         , tablename 
      into v_parent_schema
         , v_parent_tablename
      from pg_catalog.pg_tables
     where schemaname = split_part(p_parent_table, '.', 1)::name
       and tablename = split_part(p_parent_table, '.', 2)::name;

    v_function_name := partman.check_name_length(v_parent_tablename, '_part_trig_func', FALSE);

    v_partition_expression := 
    case
       when v_epoch = true then format('to_timestamp(NEW.%I)', v_control)
       else format('NEW.%I', v_control)
    end;

    if v_type = 'time' 
    then
       v_trig_func := format('create or replace function %I.%I() returns trigger language plpgsql as $t$
            declare
            v_count                 int;
            v_partition_name        text;
            v_partition_timestamp   timestamptz;
        begin 
        if TG_OP = ''INSERT'' then 
            '
       , v_parent_schema
       , v_function_name);

       case
          when v_partition_interval = '15 mins' 
          then
             v_trig_func := v_trig_func||format('v_partition_timestamp := date_trunc(''hour'', %s) +
                  ''15min''::interval * floor(date_part(''minute'', %1$s) / 15.0);' , v_partition_expression);
             v_current_partition_timestamp := date_trunc('hour', CURRENT_TIMESTAMP) +
                  '15min'::interval * floor(date_part('minute', CURRENT_TIMESTAMP) / 15.0);
           when v_partition_interval = '30 mins' 
           then
              v_trig_func := v_trig_func||format('v_partition_timestamp := date_trunc(''hour'', %s) +
                  ''30min''::interval * floor(date_part(''minute'', %1$s) / 30.0);' , v_partition_expression);
              v_current_partition_timestamp := date_trunc('hour', CURRENT_TIMESTAMP) +
                  '30min'::interval * floor(date_part('minute', CURRENT_TIMESTAMP) / 30.0);
           when v_partition_interval = '1 hour' 
           then
              v_trig_func := v_trig_func||format('v_partition_timestamp := date_trunc(''hour'', %s);', v_partition_expression);
              v_current_partition_timestamp := date_trunc('hour', CURRENT_TIMESTAMP);
           when v_partition_interval = '1 day' 
           then
              v_trig_func := v_trig_func||format('v_partition_timestamp := date_trunc(''day'', %s);', v_partition_expression);
              v_current_partition_timestamp := date_trunc('day', CURRENT_TIMESTAMP);
           when v_partition_interval = '1 week' 
           then
              v_trig_func := v_trig_func||format('v_partition_timestamp := date_trunc(''week'', %s);', v_partition_expression);
              v_current_partition_timestamp := date_trunc('week', CURRENT_TIMESTAMP);
           when v_partition_interval = '1 month' 
           then
              v_trig_func := v_trig_func||format('v_partition_timestamp := date_trunc(''month'', %s);', v_partition_expression);
              v_current_partition_timestamp := date_trunc('month', CURRENT_TIMESTAMP);
           when v_partition_interval = '3 months' 
           then
              v_trig_func := v_trig_func||format('v_partition_timestamp := date_trunc(''quarter'', %s);', v_partition_expression);
              v_current_partition_timestamp := date_trunc('quarter', CURRENT_TIMESTAMP);
           when v_partition_interval = '1 year' 
           then
              v_trig_func := v_trig_func||format('v_partition_timestamp := date_trunc(''year'', %s);', v_partition_expression);
              v_current_partition_timestamp := date_trunc('year', CURRENT_TIMESTAMP);
       end case;

       v_current_partition_name := partman.check_name_length(v_parent_tablename, to_char(v_current_partition_timestamp, v_datetime_string), TRUE); 
       v_next_partition_timestamp := v_current_partition_timestamp + v_partition_interval::interval;

       v_trig_func := v_trig_func ||format('
             if %s >= %L and %1$s < %3$L then '
            , v_partition_expression
            , v_current_partition_timestamp::timestamp(0)
            , v_next_partition_timestamp::timestamp(0));

       select count(*) into v_count from pg_catalog.pg_tables where schemaname = v_parent_schema::name and tablename = v_current_partition_name::name;
       if v_count > 0 
       then
          v_trig_func := v_trig_func || format('
                 INSERT into %I.%I VALUES (NEW.*) %s; ', v_parent_schema, v_current_partition_name, v_upsert);
       else
          v_trig_func := v_trig_func || '
            -- Child table for current values does not exist in this partition set, so write to parent
            return NEW;';
       end if;

       for i in 1..v_optimize_trigger 
       loop
          v_prev_partition_timestamp := v_current_partition_timestamp - (v_partition_interval::interval * i);
          v_next_partition_timestamp := v_current_partition_timestamp + (v_partition_interval::interval * i);
          v_final_partition_timestamp := v_next_partition_timestamp + (v_partition_interval::interval);
          v_prev_partition_name := partman.check_name_length(v_parent_tablename, to_char(v_prev_partition_timestamp, v_datetime_string), TRUE);
          v_next_partition_name := partman.check_name_length(v_parent_tablename, to_char(v_next_partition_timestamp, v_datetime_string), TRUE);

          -- Check that child table exist before making a rule to insert to them.
          -- Handles optimize_trigger being larger than premake (to go back in time further) and edge case of changing optimize_trigger immediately after running create_parent().
          select count(*) into v_count from pg_catalog.pg_tables where schemaname = v_parent_schema::name and tablename = v_prev_partition_name::name;
          if v_count > 0 
          then
             v_trig_func := v_trig_func ||format('
             elsif %s >= %L and %1$s < %3$L then 
               INSERT into %I.%I VALUES (NEW.*) %s;'
                , v_partition_expression
                , v_prev_partition_timestamp::timestamp(0)
                , v_prev_partition_timestamp::timestamp(0) + v_partition_interval::interval
                , v_parent_schema
                , v_prev_partition_name
                , v_upsert);
          end if;
          select count(*) into v_count from pg_catalog.pg_tables where schemaname = v_parent_schema::name and tablename = v_next_partition_name::name;
          if v_count > 0 
          then
             v_trig_func := v_trig_func ||format(' 
             elsif %s >= %L and %1$s < %3$L then 
                INSERT into %I.%I VALUES (NEW.*) %s;'
                , v_partition_expression
                , v_next_partition_timestamp::timestamp(0)
                , v_final_partition_timestamp::timestamp(0)
                , v_parent_schema
                , v_next_partition_name
                , v_upsert);
          end if;

      end loop;

      v_trig_func := v_trig_func||format('
            else
                v_partition_name := partman.check_name_length(%L, to_char(v_partition_timestamp, %L), TRUE);
                select count(*) into v_count from pg_catalog.pg_tables where schemaname = %L::name and tablename = v_partition_name::name;
                if v_count > 0 then 
                    execute format(''INSERT into %%I.%%I VALUES($1.*) %s'', %L, v_partition_name) USING NEW;
                else
                    return NEW;
                end if;
            end if;'
            , v_parent_tablename
            , v_datetime_string
            , v_parent_schema
            , v_upsert
            , v_parent_schema);

      v_trig_func := v_trig_func ||'
          end if;';

      if v_trigger_return_null is TRUE 
      then
         v_trig_func := v_trig_func ||'
         return null;';
      else
         v_trig_func := v_trig_func ||'
         return NEW;';
      end if;

      if v_trigger_exception_handling 
      then 
         v_trig_func := v_trig_func ||'
         EXCEPTION WHEN OTHERS then
             raise warning ''pg_partman insert into child table failed, row inserted into parent (%.%). ERROR: %'', TG_TABLE_SCHEMA, TG_TABLE_NAME, COALESCE(SQLERRM, ''unknown'');
             return NEW;';
      end if;
      v_trig_func := v_trig_func ||'
          end $t$;';

      execute v_trig_func;

      perform update_step(v_step_id, 'OK', format('Added function for current time interval: %s to %s' 
                                                 , v_current_partition_timestamp
                                                 , v_final_partition_timestamp-'1sec'::interval));

    elsif v_type = 'time-custom' 
    then

       v_trig_func := format('CREATE OR REPLACE FUNCTION %I.%I() returns trigger language plpgsql as $t$ 
           declare
               v_child_schemaname  text;
               v_child_table       text;
               v_child_tablename   text;
               v_upsert            text;
           begin
               '
           , v_parent_schema
           , v_function_name);

       v_trig_func := v_trig_func || format(' 
           select c.child_table, p.upsert into v_child_table, v_upsert
           from partman.custom_time_partitions c
           join partman.part_config p on c.parent_table = p.parent_table
           where c.partition_range @> %s 
            and c.parent_table = %L;'
           , v_partition_expression
           , v_parent_schema||'.'||v_parent_tablename);

       v_trig_func := v_trig_func || '
           select schemaname, tablename into v_child_schemaname, v_child_tablename 
           from pg_catalog.pg_tables 
           where schemaname = split_part(v_child_table, ''.'', 1)::name
           and tablename = split_part(v_child_table, ''.'', 2)::name;
           if v_child_schemaname is not null and v_child_tablename is not null then
               execute format(''INSERT into %I.%I VALUES ($1.*) %s'', v_child_schemaname, v_child_tablename, v_upsert) USinG NEW;
           else
              return NEW;
           end if;';

       if v_trigger_return_null is TRUE 
       then
          v_trig_func := v_trig_func ||'
          return null;';
       else
          v_trig_func := v_trig_func ||'
          return NEW;';
       end if;

       if v_trigger_exception_handling 
       then 
          v_trig_func := v_trig_func ||'
          exception when others then
            raise WARNING ''pg_partman insert into child table failed, row inserted into parent (%.%). ERROR: %'', TG_TABLE_SCHEMA, TG_TABLE_NAME, COALESCE(SQLERRM, ''unknown'');
            return NEW;';
       end if;
       v_trig_func := v_trig_func ||'
           end $t$;';

       execute v_trig_func;

       perform update_step(v_step_id, 'OK', format('Added function for custom time table: %s', p_parent_table));

   else
      raise exception 'ERROR: invalid time partitioning type given: %', v_type;
   end if;

    perform close_job(v_job_id);

    perform partman.set_search(v_old_search_path);

exception
     when others 
     then
        get stacked diagnostics ex_message = message_text,
                                ex_context = pg_exception_context,
                                ex_detail = pg_exception_detail,
                                ex_hint = pg_exception_hint;
  
        perform partman.exception( v_job_id
                                 , v_step_id
                                 , component
                                 ,  'message: ' || ex_message ||
                                    'context: ' || ex_context ||
                                    'detail: '  || ex_detail  ||
                                    'hint: '    || ex_hint 
                                 , sqlerrm
                                 );
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION partman.create_function_time(text, bigint)
  OWNER TO postgres;
