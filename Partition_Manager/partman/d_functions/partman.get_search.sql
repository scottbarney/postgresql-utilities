CREATE OR REPLACE FUNCTION partman.get_search()
  RETURNS text AS
$BODY$
declare
      out_search text;
begin
    out_search := current_setting('search_path');
    return out_search;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.get_search()
  OWNER TO postgres;

