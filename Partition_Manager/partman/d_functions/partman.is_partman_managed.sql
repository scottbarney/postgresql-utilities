CREATE OR REPLACE FUNCTION partman.is_partman_managed(
    schema_in character varying,
    table_in character varying)
  RETURNS boolean AS
$BODY$
declare
      in_sch alias for $1;
      in_tab alias for $2;
      cnt integer;
 begin
     select count(*)
       into cnt
       from partman.part_config
      where parent_table = in_sch || '.' || in_tab;

     if cnt > 0
     then
        return true;
     else
        return false;
     end if;
 end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION partman.is_partman_managed(character varying, character varying)
  OWNER TO postgres;

