CREATE TABLE if not exists partman.part_config
(
  parent_table text NOT NULL,
  control text NOT NULL,
  partition_type text NOT NULL,
  partition_interval text NOT NULL,
  constraint_cols text[],
  premake integer NOT NULL DEFAULT 4,
  optimize_trigger integer NOT NULL DEFAULT 4,
  optimize_constraint integer NOT NULL DEFAULT 30,
  epoch boolean NOT NULL DEFAULT false,
  inherit_fk boolean NOT NULL DEFAULT true,
  retention text,
  retention_schema text,
  retention_keep_table boolean NOT NULL DEFAULT true,
  retention_keep_index boolean NOT NULL DEFAULT true,
  infinite_time_partitions boolean NOT NULL DEFAULT false,
  datetime_string text,
  use_run_maintenance boolean NOT NULL DEFAULT true,
  jobmon boolean NOT NULL DEFAULT true,
  sub_partition_set_full boolean NOT NULL DEFAULT false,
  undo_in_progress boolean NOT NULL DEFAULT false,
  trigger_exception_handling boolean DEFAULT false,
  upsert text NOT NULL DEFAULT ''::text,
  trigger_return_null boolean NOT NULL DEFAULT true,
  archive_tablespace text,
  archive_rotation integer,
  CONSTRAINT part_config_parent_table_pkey PRIMARY KEY (parent_table),
  CONSTRAINT control_constraint_col_chk CHECK ((constraint_cols @> ARRAY[control]) <> true),
  CONSTRAINT positive_premake_check CHECK (premake > 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE partman.part_config
  OWNER TO postgres;

-- Index: partman.part_config_type_idx

-- DROP INDEX partman.part_config_type_idx;

CREATE INDEX part_config_type_idx
  ON partman.part_config
  USING btree
  (partition_type COLLATE pg_catalog."default");


