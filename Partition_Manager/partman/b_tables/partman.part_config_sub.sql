CREATE TABLE if not exists partman.part_config_sub
(
  sub_parent text NOT NULL,
  sub_partition_type text NOT NULL,
  sub_control text NOT NULL,
  sub_partition_interval text NOT NULL,
  sub_constraint_cols text[],
  sub_premake integer NOT NULL DEFAULT 4,
  sub_optimize_trigger integer NOT NULL DEFAULT 4,
  sub_optimize_constraint integer NOT NULL DEFAULT 30,
  sub_epoch boolean NOT NULL DEFAULT false,
  sub_inherit_fk boolean NOT NULL DEFAULT true,
  sub_retention text,
  sub_retention_schema text,
  sub_retention_keep_table boolean NOT NULL DEFAULT true,
  sub_retention_keep_index boolean NOT NULL DEFAULT true,
  sub_infinite_time_partitions boolean NOT NULL DEFAULT false,
  sub_use_run_maintenance boolean NOT NULL DEFAULT true,
  sub_jobmon boolean NOT NULL DEFAULT true,
  sub_trigger_exception_handling boolean DEFAULT false,
  sub_upsert text NOT NULL DEFAULT ''::text,
  sub_trigger_return_null boolean NOT NULL DEFAULT true,
  CONSTRAINT part_config_sub_pkey PRIMARY KEY (sub_parent),
  CONSTRAINT part_config_sub_sub_parent_fkey FOREIGN KEY (sub_parent)
      REFERENCES partman.part_config (parent_table) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT control_constraint_col_chk CHECK ((sub_constraint_cols @> ARRAY[sub_control]) <> true),
  CONSTRAINT positive_premake_check CHECK (sub_premake > 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE partman.part_config_sub
  OWNER TO postgres;

