CREATE TABLE if not exists partman.custom_time_partitions
(
  parent_table text NOT NULL,
  child_table text NOT NULL,
  partition_range tstzrange NOT NULL,
  CONSTRAINT custom_time_partitions_pkey PRIMARY KEY (parent_table, child_table)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE partman.custom_time_partitions
  OWNER TO postgres;

-- Index: partman.custom_time_partitions_partition_range_idx

-- DROP INDEX partman.custom_time_partitions_partition_range_idx;

CREATE INDEX custom_time_partitions_partition_range_idx
  ON partman.custom_time_partitions
  USING gist
  (partition_range);

