CREATE TABLE if not exists partman.parameters
(
  param_name character varying NOT NULL,
  param_value character varying,
  CONSTRAINT parameters_pk PRIMARY KEY (param_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE partman.parameters
  OWNER TO postgres;

