CREATE TYPE partman.check_parent_table AS
   (parent_table text,
    count bigint);
ALTER TYPE partman.check_parent_table
  OWNER TO postgres;

