# Partition Management and Logger

    Partition Management Utility
    Copyright (C) 2019  Scott Barney

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


  * configure deploy.conf so it contains the database connect info and appropriate directory for the source
  * run python deploy.py -c deploy.conf to create the schemas and underlying objects
  * will need to seed the logger for dblink creds afterword

Refactored from pg_partman and pg_jobmon is the Partition Management Code in support of programmatic partitioning of PostgreSQL tables
create schema logger and partman
create all objects in logger/partman
seed logger.dblink_mapping_jobmon table with creds using for dblink calls
seed partman.parameters with param_name "search_path" and param_value "partman,logger,pg_temp,public"
to create and register:

create an empty parent table or
pass existing parent in with the following:
select partman.create_register_parent('schema.table','control','type','granularity','archive_tbs','archive_rotation');

where:

 * schema.table is the schema name and table name of the parent.
 * control is the column you're partitioning by.
 * type is the type of partition ( id, time, time-custom).
 * granularity is daily,weekly,monthly,quarterly,yearly.
 * archive_tbs is the archive location for the archiver to move archived data to.
 * archive_rotation is the number of partitions to keep on fast storage:
    if set to 2 for example:
      anything older than 2 cycles of the granularity will be archived ( 2 days,weeks,quarters,etc )
run maintenance

executed at the parent level, by executing

select partman.run_maint('schema.table');

optional toggles are analyze, logger, debug - all default to true

archiving

executed globally or at the parent level

select partman.run_archiver() - global execution
select partman.archive_table('schema.table') where schema.table is the schema/table name of the parent
archiver will check for the existence of the archive_tablespace and fail if it does not exist
archiver serial operation:
* clones existing child table to the archive tablespace that meets the threshold 
  for archive rotation with a _a suffixed to the table name
* validates the clone row count to make sure it matches
* dis-inherits the original child and renames, adding a _o suffix to the name
* changes the name of the clone to the original child name
* inherits the new child to the parent
* drops the original child ( with the _o suffix )
Operations are logged to logger.job_log and logger.job_log_detail autonomously.
The archiver performs most of its tasks over dblink:
to keep transaction size to a minimum when moving data.
to atomize operations to single steps.
Known issues:
naming convention must be followed or code must be customized to suit special naming
no support for purgatory tables at this time - meaning data that falls outside the partition scope
multi-threaded support has not been tested, meaning one instance of the management code can be run at any one time.
cleanup is not accounted for with failed operation. If any of the code dies along the way, it will leave remnants.

Test Cases:

DROP TABLE analytics.market_daily cascade;

CREATE TABLE analytics.market_daily
(
  exchange character varying(8) NOT NULL,
  symbol character varying(8) NOT NULL,
  trade_date date NOT NULL,
  open_amt numeric(10,4),
  high_amt numeric(10,4),
  low_amt numeric(10,4),
  close_amt numeric(10,4),
  volume bigint,
  adj_close numeric(20,5),
  CONSTRAINT market_daily_pk1 PRIMARY KEY (exchange, symbol, trade_date)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE analytics.market_daily
  OWNER TO postgres;

-- Index: analytics.market_daily_trade_year_n1

-- DROP INDEX analytics.market_daily_trade_year_n1;

CREATE INDEX market_daily_trade_year_n1
  ON analytics.market_daily
  USING btree
  (date_part('year'::text, trade_date));
truncate table partman.part_config cascade;
truncate table logger.job_log cascade;

DROP TABLE analytics.market_monthly cascade;

CREATE TABLE analytics.market_monthly
(
  exchange character varying(8) NOT NULL,
  symbol character varying(8) NOT NULL,
  trade_date date NOT NULL,
  open_amt numeric(10,4),
  high_amt numeric(10,4),
  low_amt numeric(10,4),
  close_amt numeric(10,4),
  volume bigint,
  adj_close numeric(20,5),
  CONSTRAINT market_monthly_pk1 PRIMARY KEY (exchange, symbol, trade_date)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE analytics.market_monthly
  OWNER TO postgres;

-- Index: analytics.market_monthly_trade_year_n1

-- DROP INDEX analytics.market_monthly_trade_year_n1;

CREATE INDEX market_monthly_trade_year_n1
  ON analytics.market_monthly
  USING btree
  (date_part('year'::text, trade_date));

  DROP TABLE analytics.market_quarterly cascade;

CREATE TABLE analytics.market_quarterly
(
  exchange character varying(8) NOT NULL,
  symbol character varying(8) NOT NULL,
  trade_date date NOT NULL,
  open_amt numeric(10,4),
  high_amt numeric(10,4),
  low_amt numeric(10,4),
  close_amt numeric(10,4),
  volume bigint,
  adj_close numeric(20,5),
  CONSTRAINT market_quarterly_pk1 PRIMARY KEY (exchange, symbol, trade_date)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE analytics.market_quarterly
  OWNER TO postgres;

-- Index: analytics.market_quarterly_trade_year_n1

-- DROP INDEX analytics.market_quarterly_trade_year_n1;

CREATE INDEX market_quarterly_trade_year_n1
  ON analytics.market_quarterly
  USING btree
  (date_part('year'::text, trade_date));

 DROP TABLE analytics.market_weekly cascade;

CREATE TABLE analytics.market_weekly
(
  exchange character varying(8) NOT NULL,
  symbol character varying(8) NOT NULL,
  trade_date date NOT NULL,
  open_amt numeric(10,4),
  high_amt numeric(10,4),
  low_amt numeric(10,4),
  close_amt numeric(10,4),
  volume bigint,
  adj_close numeric(20,5),
  CONSTRAINT market_weekly_pk1 PRIMARY KEY (exchange, symbol, trade_date)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE analytics.market_weekly
  OWNER TO postgres;

-- Index: analytics.market_weekly_trade_year_n1

-- DROP INDEX analytics.market_weekly_trade_year_n1;

CREATE INDEX market_weekly_trade_year_n1
  ON analytics.market_weekly
  USING btree
  (date_part('year'::text, trade_date)); 
Blog  Support  Plans & pricing  Documentation  API  Site status  Version info  Terms of service  Privacy policy
JIRA Software  Confluence  Bamboo  SourceTree  HipChat
Atlassian


select partman.create_register_parent('analytics.market_daily','trade_date','time','daily','archive',3);
select partman.create_register_parent('analytics.market_weekly','trade_date','time','weekly','archive',2);
select partman.create_register_parent('analytics.market_monthly','trade_date','time','monthly','archive',4);
select partman.create_register_parent('analytics.market_quarterly','trade_date','time','quarterly','archive',1);

select partman.run_archiver(true);